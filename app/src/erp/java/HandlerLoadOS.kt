import android.arch.lifecycle.MutableLiveData
import android.util.Log
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.CatalogOSRepository
import ru.smartms.smallsmarty.data.source.CatalogOperationObjectRepository
import ru.smartms.smallsmarty.data.source.ErrorMessageRepository
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObjects
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import ru.smartms.smallsmarty.utils.lat2cyr

suspend fun loadOSFromREST(catalogOperationObjectRepository: CatalogOperationObjectRepository, isLoadingProgressBar: MutableLiveData<Boolean>, errorMessageRepository: ErrorMessageRepository) {
    catalogOperationObjectRepository.deleteAll()
    var catalogOperationObjects: CatalogOperationObjects
    var i = 0
    try {
        isLoadingProgressBar.postValue(true)
        do {
            i++
            catalogOperationObjects = catalogOperationObjectRepository.loadFromRest(
                    BuildConfig.QTY_REQ * i,
                    BuildConfig.QTY_REQ * (i - 1),
                    null,
                    null,
                    null,
                    ORDER_BY_CATALOG).await()
            catalogOperationObjectRepository.insertAll(catalogOperationObjects.value)
        } while (catalogOperationObjects.value.isNotEmpty())
    } catch (e: Exception) {
        Log.e("loadOSFromREST", e.localizedMessage)
        isLoadingProgressBar.postValue(false)
        errorMessageRepository.insertMessage(e.localizedMessage)
    }
}

fun handleOS(barcode: String, catalogOperationObjectRepository: CatalogOperationObjectRepository): CatalogOperationObject? = catalogOperationObjectRepository.getCatalogOperationObject(lat2cyr(barcode))

fun sendOSKey(refKey: String, refKeyOS: MutableLiveData<String>, isLoadingProgressBar: MutableLiveData<Boolean>) {
    refKeyOS.postValue(refKey)
    isLoadingProgressBar.postValue(false)
}