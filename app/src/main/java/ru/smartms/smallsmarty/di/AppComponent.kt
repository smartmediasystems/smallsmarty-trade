package ru.smartms.smallsmarty.di

import dagger.Component
import ru.smartms.smallsmarty.StartActivity
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.dao.ARGoodsInStoreDao
import ru.smartms.smallsmarty.db.dao.CatalogCharacteristicDao
import ru.smartms.smallsmarty.db.dao.CatalogSeriesDao
import ru.smartms.smallsmarty.db.dao.LicenseInfoDao
import ru.smartms.smallsmarty.lifecycle.BluetoothScannerLifecycle
import ru.smartms.smallsmarty.lifecycle.RFIDScannerLifecycle
import ru.smartms.smallsmarty.ui.fragments.*
import ru.smartms.smallsmarty.ui.fragments.prefs.PrefsMainFragment
import ru.smartms.smallsmarty.ui.viewmodel.*
import javax.inject.Singleton

@Component(modules = [(AppModule::class), (NetModule::class), (RepositoryModule::class), (ContextModule::class)])
@Singleton
interface AppComponent {
    fun inject(startActivityViewModel: StartActivityViewModel)
    fun inject(catalogNomenclaturesFragment: CatalogNomenclaturesFragment)
    fun inject(catalogNomenclaturesViewModel: CatalogNomenclaturesViewModel)
    fun inject(catalogNomenclatureRepository: CatalogNomenclatureRepository)
    fun inject(docRecountGoodsFragment: DocRecountGoodsFragment)
    fun inject(docRecountGoodsViewModel: DocRecountGoodsViewModel)
    fun inject(tabDocRecountGoodsViewModel: TabDocRecountGoodsViewModel)
    fun inject(tabDocRecountGoodsFragment: TabDocRecountGoodsFragment)
    fun inject(tabDocRecountGoodsEditViewModel: TabDocRecountGoodsEditViewModel)
    fun inject(tabDocRecountGoodsEditFragment: TabDocRecountGoodsEditFragment)
    fun inject(docInventoryOSViewModel: DocInventoryOSViewModel)
    fun inject(docInventoryOSFragment: DocInventoryOSFragment)
    fun inject(tabDocInventoryOSViewModel: TabDocInventoryOSViewModel)
    fun inject(tabDocInventoryOSFragment: TabDocInventoryOSFragment)
    fun inject(catalogOSViewModel: CatalogOSViewModel)
    fun inject(catalogOSFragment: CatalogOSFragment)
    fun inject(tabDocInventoryOSEditViewModel: TabDocInventoryOSEditViewModel)
    fun inject(tabDocInventoryOSEditFragment: TabDocInventoryOSEditFragment)
    fun inject(docInventoryOSSelectDocViewModel: DocInventoryOSSelectDocViewModel)
    fun inject(docInventoryOSSelectDocFragment: DocInventoryOSSelectDocFragment)
    fun inject(bluetoothScannerLifecycle: BluetoothScannerLifecycle)
    fun inject(docMovingGoodsViewModel: DocMovingGoodsViewModel)
    fun inject(tabDocMovingGoodsViewModel: TabDocMovingGoodsViewModel)
    fun inject(docMovingGoodsFragment: DocMovingGoodsFragment)
    fun inject(tabDocMovingGoodsFragment: TabDocMovingGoodsFragment)
    fun inject(tabDocMovingGoodsEditViewModel: TabDocMovingGoodsEditViewModel)
    fun inject(tabDocMovingGoodsEditFragment: TabDocMovingGoodsEditFragment)
    fun inject(prefsMainViewModel: PrefsMainViewModel)
    fun inject(prefsMainFragment: PrefsMainFragment)
    fun inject(rfidScannerLifecycle: RFIDScannerLifecycle)
    fun inject(catalogCharacteristicRepository: CatalogCharacteristicRepository)
    fun inject(catalogCharacteristicDao: CatalogCharacteristicDao)
    fun inject(catalogSeriesRepository: CatalogSeriesRepository)
    fun inject(catalogSeriesDao: CatalogSeriesDao)
    fun inject(licenseInfoDao: LicenseInfoDao)
    fun inject(licenseInfoRepository: LicenseInfoRepository)
    fun inject(licenseInfoViewModel: LicenseInfoViewModel)
    fun inject(licenseInfoFragment: LicenseInfoFragment)
    fun inject(purchasesFragment: PurchasesFragment)
    fun inject(purchasesViewModel: PurchasesViewModel)
    fun inject(firebaseRepository: FirebaseRepository)
    fun inject(startActivity: StartActivity)
    fun inject(arGoodsInStoreRepository: ARGoodsInStoreRepository)
    fun inject(arGoodsInStoreDao: ARGoodsInStoreDao)
    fun inject(catalogOperationObjectFragment: CatalogOperationObjectFragment)
    fun inject(catalogOperationObjectViewModel: CatalogOperationObjectViewModel)
    fun inject(docInventoryOperationObjectFragment: DocInventoryOperationObjectFragment)
    fun inject(docInventoryOperationObjectViewModel: DocInventoryOperationObjectViewModel)
    fun inject(tabDocInventoryOperationObjectViewModel: TabDocInventoryOperationObjectViewModel)
    fun inject(tabDocInventoryOperationObjectEditViewModel: TabDocInventoryOperationObjectEditViewModel)
    fun inject(tabDocInventoryOperationObjectEditFragment: TabDocInventoryOperationObjectEditFragment)
    fun inject(tabDocInventoryOperationObjectFragment: TabDocInventoryOperationObjectFragment)
    fun inject(docInventoryOperationObjectSelectDocViewModel: DocInventoryOperationObjectSelectDocViewModel)
}