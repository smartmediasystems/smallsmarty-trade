package ru.smartms.smallsmarty.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.dao.*
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application):
            SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Provides
    @Singleton
    fun providesFirebaseDatabase(): FirebaseDatabase {
        val db = FirebaseDatabase.getInstance()
        db.setPersistenceEnabled(true)
        return db
    }

    @Provides
    @Singleton
    fun providesFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    //Repository
    @Provides
    @Singleton
    fun provideFirebaseRepository(db: FirebaseDatabase): FirebaseRepository {
        db.setPersistenceEnabled(true)
        return FirebaseRepository(db)
    }

    @Provides
    @Singleton
    fun provideSharedPrefRepository(sharedPreferences: SharedPreferences): SharedPrefRepository {
        return SharedPrefRepository(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideCatalogNomenclaturesRepository(webservice: RestService, catalogNomenclatureDao: CatalogNomenclatureDao): CatalogNomenclatureRepository {
        return CatalogNomenclatureRepository(webservice, catalogNomenclatureDao)
    }

    @Provides
    @Singleton
    fun provideInfRegBarcodeRepository(webservice: RestService, infRegBarcodeNomenclatureDao: InfRegBarcodeNomenclatureDao): InfRegBarcodeRepository {
        return InfRegBarcodeRepository(webservice, infRegBarcodeNomenclatureDao)
    }

    @Provides
    @Singleton
    fun provideScanDataRepository(scanDataDao: ScanDataDao): ScanDataRepository {
        return ScanDataRepository(scanDataDao)
    }

    @Provides
    @Singleton
    fun provideDocRecountGoodsRepository(webservice: RestService, docRecountGoodsDao: DocRecountGoodsDao, tabDocRecountGoodsDao: TabDocRecountGoodsDao): DocRecountGoodsRepository {
        return DocRecountGoodsRepository(webservice, docRecountGoodsDao, tabDocRecountGoodsDao)
    }

    @Provides
    @Singleton
    fun provideTabDocRecountGoodsRepository(tabDocRecountGoodsDao: TabDocRecountGoodsDao, catalogNomenclatureAllTabDocRecountGoodsDao: CatalogNomenclatureAllTabDocRecountGoodsDao): TabDocRecountGoodsRepository {
        return TabDocRecountGoodsRepository(tabDocRecountGoodsDao, catalogNomenclatureAllTabDocRecountGoodsDao)
    }

    @Provides
    @Singleton
    fun provideErrorMessageRepository(errorMessageDao: ErrorMessageDao): ErrorMessageRepository {
        return ErrorMessageRepository(errorMessageDao)
    }

    @Provides
    @Singleton
    fun provideARGoodsInStoreRepository(restService: RestService, arGoodsInStoreDao: ARGoodsInStoreDao): ARGoodsInStoreRepository {
        return ARGoodsInStoreRepository(restService, arGoodsInStoreDao)
    }

    //Dao
    @Provides
    @Singleton
    fun provideCatalogNomenclatureDao(): CatalogNomenclatureDao {
        return App.db?.catalogNomenclatureDao()!!
    }

    @Provides
    @Singleton
    fun provideInfRegBarcodeDao(): InfRegBarcodeNomenclatureDao {
        return App.db?.infRegBarcodeNomenclatureDao()!!
    }

    @Provides
    @Singleton
    fun provideScanDataDao(): ScanDataDao {
        return App.db?.scanDataDao()!!
    }

    @Provides
    @Singleton
    fun provideDocRecountGoodsDao(): DocRecountGoodsDao {
        return App.db?.docRecountGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideTabDocRecountGoodsDao(): TabDocRecountGoodsDao {
        return App.db?.tabDocRecountGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogNomenclatureAllTabDocRecountGoodsDao(): CatalogNomenclatureAllTabDocRecountGoodsDao {
        return App.db?.catalogNomenclatureAllTabDocRecountGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogOSDao(): CatalogOSDao {
        return App.db?.catalogOSDao()!!
    }

    @Provides
    @Singleton
    fun provideDocInventoryOSDao(): DocInventoryOSDao {
        return App.db?.docInventoryOSDao()!!
    }

    @Provides
    @Singleton
    fun provideTabDocInventoryOSDao(): TabDocInventoryOSDao {
        return App.db?.tabDocInventoryOSDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogNomenclatureAllTabDocInventoryOSDao(): CatalogOSAllTabDocInventoryOSDao {
        return App.db?.catalogOSAllTabDocInventoryOSDao()!!
    }

    @Provides
    @Singleton
    fun provideDocMovingGoodsDao(): DocMovingGoodsDao {
        return App.db?.docMovingGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideTabDocMovingGoodsDao(): TabDocMovingGoodsDao {
        return App.db?.tabDocMovingGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogNomenclatureAllTabDocMovingGoodsDao(): CatalogNomenclatureAllTabDocMovingGoodsDao {
        return App.db?.catalogNomenclatureAllTabDocMovingGoodsDao()!!
    }

    @Provides
    @Singleton
    fun provideErrorMessageDao(): ErrorMessageDao {
        return App.db?.errorMessageDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogCharacteristicDao(): CatalogCharacteristicDao {
        return App.db?.catalogCharacteristicDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogSeriesDao(): CatalogSeriesDao {
        return App.db?.catalogSeriesDao()!!
    }

    @Provides
    @Singleton
    fun provideLicenseInfoDao(): LicenseInfoDao {
        return App.db?.licenseInfoDao()!!
    }

    @Provides
    @Singleton
    fun provideARGoodsInStoreDao(): ARGoodsInStoreDao {
        return App.db?.aRGoodsInStoreDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogOperationObjectDao(): CatalogOperationObjectDao {
        return App.db?.catalogOperationObjectDao()!!
    }

    @Provides
    @Singleton
    fun provideCatalogOperationObjectAllTabDocInventoryOperationObjectDao(): CatalogOperationObjectAllTabDocInventoryOperationObjectDao {
        return App.db?.catalogOperationObjectAllTabDocInventoryOperationObjectDao()!!
    }

    @Provides
    @Singleton
    fun provideDocInventoryOperationObjectDao(): DocInventoryOperationObjectDao {
        return App.db?.docInventoryOperationObjectDao()!!
    }

    @Provides
    @Singleton
    fun provideTabDocInventoryOperationObjectDao(): TabDocInventoryOperationObjectDao {
        return App.db?.tabDocInventoryOperationObjectDao()!!
    }
}