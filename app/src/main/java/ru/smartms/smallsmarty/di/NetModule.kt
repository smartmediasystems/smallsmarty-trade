package ru.smartms.smallsmarty.di

import com.google.gson.Gson
import dagger.Provides
import javax.inject.Singleton
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.smartms.smallsmarty.BuildConfig
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Named
import android.app.Application
import android.content.SharedPreferences
import dagger.Module
import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.data.source.SharedPrefRepository
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.utils.DocMovingGoodsSerializer
import ru.smartms.smallsmarty.utils.HostSelectionInterceptor


const val BASE_URL: String = "BASE_URL"
const val BASE_NAME: String = "BASE_NAME"

@Module
class NetModule {

    @Provides
    @Named(BASE_URL)
    fun provideBaseUrl(sharedPreferencesRepository: SharedPrefRepository): String {
        return sharedPreferencesRepository.getBaseUrl()
    }

    @Provides
    @Named(BASE_NAME)
    fun provideBaseName(sharedPrefRepository: SharedPrefRepository): String {
        return sharedPrefRepository.getDbName()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(DocMovingGoods::class.java, DocMovingGoodsSerializer())
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache, hostSelectionInterceptor: HostSelectionInterceptor): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(logging)
                .addInterceptor(hostSelectionInterceptor)
                .connectTimeout(BuildConfig.TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(BuildConfig.TIMEOUT, TimeUnit.MILLISECONDS)
        return client.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(@Named(BASE_URL) baseUrl: String, @Named(BASE_NAME) baseName: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("http://$baseUrl/$baseName/")
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun provideRestService(retrofit: Retrofit): RestService {
        return retrofit.create(RestService::class.java)
    }

    @Provides
    @Singleton
    fun provideHostSelectionInterceptor(sharedPreferences: SharedPreferences, @Named(BASE_URL) baseUrl: String, @Named(BASE_NAME) baseName: String): HostSelectionInterceptor {
        val hostSelectionInterceptor = HostSelectionInterceptor()
        hostSelectionInterceptor.userName = sharedPreferences.getString("login", "")
        hostSelectionInterceptor.password = sharedPreferences.getString("password", "")
        hostSelectionInterceptor.url = baseUrl
        hostSelectionInterceptor.dbName = baseName
        hostSelectionInterceptor.scheme = "http"
        return hostSelectionInterceptor
    }
}