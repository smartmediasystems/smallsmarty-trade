package ru.smartms.smallsmarty

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.generalscan.bluetooth.BluetoothConnect
import android.widget.Toast
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.SwitchCompat
import android.view.MenuItem
import android.widget.ProgressBar
import android.widget.TextView
import com.crashlytics.android.Crashlytics
import com.google.firebase.auth.FirebaseAuth
import io.fabric.sdk.android.Fabric
import ru.smartms.smallsmarty.lifecycle.BluetoothScannerLifecycle
import ru.smartms.smallsmarty.lifecycle.RFIDScannerLifecycle
import ru.smartms.smallsmarty.ui.viewmodel.StartActivityViewModel
import javax.inject.Inject

class StartActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navController: NavController
    private lateinit var viewModel: StartActivityViewModel
    private lateinit var navigationView: NavigationView
    private lateinit var progressBar: ProgressBar
    private lateinit var tvTypeVersion: TextView
    var fab: FloatingActionButton? = null
    private lateinit var menuItemUHF: MenuItem
    private lateinit var rfidScannerLifecycle: RFIDScannerLifecycle
    @Inject
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component.inject(this)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.start_activity)
        progressBar = findViewById(R.id.progress_bar)
        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)
        viewModel = ViewModelProviders.of(this).get(StartActivityViewModel::class.java)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }
        fab = findViewById(R.id.fab)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(navigationView, navController)
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        BluetoothConnect.SetOnConnectedListener { Toast.makeText(applicationContext, getString(R.string.bluetooth_connect), Toast.LENGTH_SHORT).show() }
        BluetoothConnect.SetOnDisconnectListener { Toast.makeText(applicationContext, getString(R.string.bluetooth_disconnect), Toast.LENGTH_SHORT).show() }
        lifecycle.addObserver(BluetoothScannerLifecycle())
        rfidScannerLifecycle = RFIDScannerLifecycle()
        lifecycle.addObserver(rfidScannerLifecycle)
        menuItemUHF = navigationView.menu.findItem(R.id.nav_action_uhf_on_off)
        val switchOnOffRFID = menuItemUHF.actionView.findViewById<SwitchCompat>(R.id.drawer_switch)
        switchOnOffRFID.setOnCheckedChangeListener { _, isChecked ->
            rfidScannerLifecycle.onOffRFID(isChecked)
        }
        viewModel.getErrorMessage()?.observe(this, Observer { errorMessage ->
            if (errorMessage != null) {
                if (!errorMessage.isEmpty()) {
                    Toast.makeText(applicationContext, errorMessage[0].message, Toast.LENGTH_SHORT).show()
                    viewModel.deleteErrorMessage()
                }
            }
        })
        tvTypeVersion = navigationView.getHeaderView(0).findViewById(R.id.tv_type_version)
        viewModel.isLicenseActiveLiveData().observe(this, Observer { isLicenseActiveLiveData ->
            if (isLicenseActiveLiveData == true) {
                tvTypeVersion.setText(R.string.version_pro)
            } else {
                tvTypeVersion.setText(R.string.version_free)
            }
        })
//        rfidScannerLifecycle.runFlag.observe(this, Observer { runFlag ->
//            if (runFlag == null) {
//                switchOnOffRFID.isChecked = false
//            }
//        })
    }
}
