package ru.smartms.smallsmarty.billingmodule

import android.app.Activity
import android.content.Context
import android.util.Log
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponse
import java.io.IOException
import java.util.ArrayList
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.SkuDetailsParams
import ru.smartms.smallsmarty.extensions.consumeAsyncAwait
import ru.smartms.smallsmarty.extensions.queryPurchaseHistoryAsyncAwait
import ru.smartms.smallsmarty.extensions.querySkuDetailsAsyncAwait
import ru.smartms.smallsmarty.extensions.startConnectionAwait
import ru.smartms.smallsmarty.utils.BillingResponseCodeException


class BillingManager(val context: Context, private val billingUpdatesListener: BillingUpdatesListener) : PurchasesUpdatedListener {

    companion object {
        const val BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtqOuJAXy6a/z935/Dq0BifzMafQb7Ha3m0CoUs2+t83UGwAvlaDeNUcxSKZ0tIYPtHDIkMhw6tEvrCNeWNKBYp91gxV3T44WsjwCYW+VgDTeAIIyx83cPAOPe9MAmPP5qA5Yxj5eqKhLbxyBKlxE9H0hUO8M9TQqZMx1rN8Lz9yEjSO/vqhl3WvJtuEnRLhoD3GcB4d7DEDuR8b4FlmJQX1I60hwZlOr4rT99Rle7ZYS2fbExeyFXHxh/aw8MMBhv09Vsbr7kV0XniTz2vTyB1rZTX0R2RTuaGsG5FNvPS+bm7JJjWUtQ/b3dpyh7l6LEV1nh1fAc/Y/HtnTBUohfwIDAQAB"
        const val BILLING_MANAGER_NOT_INITIALIZED = -1
        const val TAG = "BillingManager"
    }

    private var billingClientResponseCode: Int = BILLING_MANAGER_NOT_INITIALIZED

    val billingClient: BillingClient by lazy {
        BillingClient.newBuilder(context).setListener(this).build()
    }

    private val purchases: ArrayList<Purchase> by lazy {
        ArrayList<Purchase>()
    }

    interface BillingUpdatesListener {
        fun onBillingClientSetupFinished()
        fun onPurchasesUpdated(purchases: List<Purchase>)
    }

    private var isServiceConnected: Boolean = false


    suspend fun startServiceConnectionAwait(): Boolean {
            isServiceConnected = try {
                billingClient.startConnectionAwait()
            } catch (e: BillingResponseCodeException) {
                false
            }
        return isServiceConnected
    }

    suspend fun consumeAsyncAwait(purchaseToken: String): Int {
        return try {
            billingClient.consumeAsyncAwait(purchaseToken)
        } catch (e: BillingResponseCodeException) {
            -10
        }
    }

    suspend fun querySkuDetailsAsyncAwait(skuDetailsParams: SkuDetailsParams): MutableList<SkuDetails> {
        return try {
            billingClient.querySkuDetailsAsyncAwait(skuDetailsParams)
        } catch (e: BillingResponseCodeException) {
            ArrayList()
        }
    }

    suspend fun getPurchasesAsync(): List<Purchase> {
        val purchasesResult = billingClient.queryPurchaseHistoryAsyncAwait()
        purchasesResult.forEach { purchase ->
            handlePurchase(purchase)
        }
        return purchasesResult
    }

    override fun onPurchasesUpdated(resultCode: Int, purchases: List<Purchase>?) {
        when (resultCode) {
            BillingResponse.OK -> {
                purchases?.forEach { purchase ->
                    handlePurchase(purchase)
                }
                purchases?.let { billingUpdatesListener.onPurchasesUpdated(it) }
            }
            BillingResponse.USER_CANCELED -> Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping")
            else -> Log.w(TAG, "onPurchasesUpdated() got unknown resultCode: $resultCode")
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
            Log.i(TAG, "Got a purchase: $purchase; but signature is bad. Skipping...")
            return
        }
        Log.d(TAG, "Got a verified purchase: $purchase")
        //purchases.add(purchase)
    }

    private fun onQueryPurchasesFinished(result: Purchase.PurchasesResult) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (result.responseCode != BillingResponse.OK) {
            return
        }
        purchases.clear()
        onPurchasesUpdated(BillingResponse.OK, result.purchasesList)
    }

    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
        // Some sanity checks to see if the developer (that's you!) really followed the
        // instructions to run this sample (don't put these checks on your app!)
        if (BASE_64_ENCODED_PUBLIC_KEY.contains("CONSTRUCT_YOUR")) {
            throw RuntimeException("Please update your app's public key at: " + "BASE_64_ENCODED_PUBLIC_KEY")
        }

        return try {
            Security.verifyPurchase(BASE_64_ENCODED_PUBLIC_KEY, signedData, signature)
        } catch (e: IOException) {
            Log.e(TAG, "Got an exception trying to validate a purchase: $e")
            false
        }
    }

    fun initiatePurchaseFlow(activity: Activity) {
        val skuList = ArrayList<String>()
        skuList.add("android.test.purchased")
        val skuDetailsParams = SkuDetailsParams.newBuilder()
                .setSkusList(skuList).setType(BillingClient.SkuType.INAPP).build()
        billingClient.querySkuDetailsAsync(skuDetailsParams) { _, skuDetailsList ->
            val flowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetailsList[0])
                    .build()
            val billingResponseCode = billingClient.launchBillingFlow(activity, flowParams)
            if (billingResponseCode == BillingResponse.ITEM_ALREADY_OWNED) {
            }
        }
    }

    fun getPurchases(): List<Purchase> {
        val purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList
        purchasesResult.forEach { purchase ->
            handlePurchase(purchase)
        }
        return purchasesResult
    }
}