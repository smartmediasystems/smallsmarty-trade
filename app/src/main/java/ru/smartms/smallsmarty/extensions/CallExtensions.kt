package ru.smartms.smallsmarty.extensions

import com.android.billingclient.api.*
import kotlinx.coroutines.NonCancellable.cancel
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import ru.smartms.smallsmarty.db.entity.BaseEntity
import ru.smartms.smallsmarty.utils.BillingResponseCodeException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

/**
 * Suspend extension that allows suspend [Call] inside coroutine.
 *
 * @return Result of request or throw exception
 */
suspend fun <T : Any> Call<T>.await(): T {
    return suspendCancellableCoroutine { continuation ->
        enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>?, response: Response<T?>) {
                val body = response.body()
                if (response.isSuccessful) {
                    if (body == null) {
                        continuation.resumeWithException(
                                NullPointerException("Response body is null: $response")
                        )
                    } else {
                        continuation.resume(body)
                    }
                } else {
                    if (body != null) {
                        try {
                            val error = (body as BaseEntity).odataError?.message.toString()
                            continuation.resumeWithException(Exception(error))
                        } catch (t: Throwable) {
                            continuation.resumeWithException(HttpException(response))
                        }
                    } else {
                        continuation.resumeWithException(HttpException(response))
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                // Don't bother with resuming the continuation if it is already cancelled.
                if (continuation.isCancelled) return
                continuation.resumeWithException(t)
            }
        })
        continuation.invokeOnCancellation {
            if (continuation.isCancelled)
                try {
                    cancel()
                } catch (ex: Throwable) {
                    //Ignore cancel exception
                }
        }
    }
}

suspend fun BillingClient.querySkuDetailsAsyncAwait(skuDetailsParams: SkuDetailsParams): MutableList<SkuDetails> {
    return suspendCancellableCoroutine { continuation ->
        querySkuDetailsAsync(skuDetailsParams) { responseCode, skuDetailsList ->
            if (responseCode == BillingClient.BillingResponse.OK) {
                continuation.resume(skuDetailsList)
            } else {
                continuation.resumeWithException(BillingResponseCodeException(responseCode.toString()))
            }
        }
        continuation.invokeOnCancellation {
            if (continuation.isCancelled)
                try {
                    cancel()
                } catch (ex: Throwable) {
                    //Ignore cancel exception
                }
        }
    }
}

suspend fun BillingClient.startConnectionAwait(): Boolean {
    return suspendCancellableCoroutine { continuation ->
        startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    continuation.resume(true)
                } else {
                    continuation.resumeWithException(BillingResponseCodeException(billingResponseCode.toString()))
                }
            }

            override fun onBillingServiceDisconnected() {
                continuation.resumeWithException(Throwable())
            }
        })
        continuation.invokeOnCancellation {
            if (continuation.isCancelled)
                try {
                    cancel()
                } catch (ex: Throwable) {
                    //Ignore cancel exception
                }
        }
    }
}

suspend fun BillingClient.consumeAsyncAwait(purchaseToken: String): Int {
    return suspendCancellableCoroutine { continuation ->
        consumeAsync(purchaseToken) { responseCode, _ ->
            if (responseCode == BillingClient.BillingResponse.OK) {
                continuation.resume(responseCode)
            } else {
                continuation.resumeWithException(BillingResponseCodeException(responseCode.toString()))
            }
        }
        continuation.invokeOnCancellation {
            if (continuation.isCancelled)
                try {
                    cancel()
                } catch (ex: Throwable) {
                    //Ignore cancel exception
                }
        }
    }
}

suspend fun BillingClient.queryPurchaseHistoryAsyncAwait(): List<Purchase> {
    return suspendCancellableCoroutine { continuation ->
        queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP) { responseCode, skuDetailsList ->
            if (responseCode == BillingClient.BillingResponse.OK) {
                continuation.resume(skuDetailsList)
            } else {
                continuation.resumeWithException(BillingResponseCodeException(responseCode.toString()))
            }
        }
        continuation.invokeOnCancellation {
            if (continuation.isCancelled)
                try {
                    cancel()
                } catch (ex: Throwable) {
                    //Ignore cancel exception
                }
        }
    }
}