package ru.smartms.smallsmarty.utils

import okhttp3.Interceptor
import okhttp3.Response
import android.util.Base64
import okhttp3.HttpUrl

class HostSelectionInterceptor : Interceptor {

    @Volatile
    var url: String? = null
    @Volatile
    var dbName: String? = null
    @Volatile
    var scheme: String? = null
    @Volatile
    var port: Int = 0
    @Volatile
    var userName: String? = null
    @Volatile
    var password: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val credentials = "$userName:$password"
        val basicParams = Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        val basic = "Basic $basicParams"
        var request = chain.request()
        if (url.isNullOrBlank()) {
            url = LOCALHOST_IP
        }
        val host = HttpUrl.parse("http://$url/$dbName")
        val scheme = this.scheme
        if (host != null && scheme != null) {
            val newUrl = request.url().newBuilder()
                    .scheme(scheme)
                    .host(host.url().toURI().host)
                    .setPathSegment(0, dbName ?: "")
                    .build()
            val builder = request.newBuilder()
            builder.url(newUrl)
            if (!userName.isNullOrBlank()) {
                builder.header("Authorization", basic)
            }
            builder.header("Accept", "application/json")
            builder.method(request.method(), request.body())
            request = builder.build()
        }
        return chain.proceed(request)
    }
}