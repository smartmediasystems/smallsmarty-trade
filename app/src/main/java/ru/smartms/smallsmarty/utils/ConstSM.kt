package ru.smartms.smallsmarty.utils

const val REQUEST_READ_STORAGE = 0
const val REQUEST_WRITE_STORAGE = 1
const val IS_FOLDER_FILTER = "IsFolder eq 'false'"
const val NULL_REF = "00000000-0000-0000-0000-000000000000"
const val ORDER_BY_CATALOG = "Description asc, Code asc"
const val EXPAND_NOMENCLATURE = "Номенклатура"
const val EXPAND_NOMENCLATURE_CHARACTERISTIC = "Номенклатура, Характеристика"
const val PRODUCT_ID = "smallsmarty_erp"
const val URL_LICENSE_SYSTEM = "smallsmarty.ru"
const val REQUEST_KEY_ACTIVATE = "activation"
const val REQUEST_KEY_CHECK= "check"
const val WC_API = "software-api"
const val LOCALHOST_IP = "127.0.0.1"
