package ru.smartms.smallsmarty.utils

import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries
import java.text.SimpleDateFormat
import java.util.*

fun getCurrentDateTime(): String {
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR).toString()
    val month = setNull(c.get(Calendar.MONTH) + 1)
    val day = setNull(c.get(Calendar.DAY_OF_MONTH))
    val hour = setNull(c.get(Calendar.HOUR_OF_DAY))
    val minute = setNull(c.get(Calendar.MINUTE))
    val second = setNull(c.get(Calendar.SECOND))
    return "$year-$month-${day}T$hour:$minute:$second"
}

fun convertDateTime(dateTime: String): String {
    return try {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale("ru")).parse(dateTime)
        SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale("ru")).format(sdf)
    } catch (t: Throwable) {
        dateTime
    }
}

fun setNull(i: Int): String = if (i < 10) "0$i" else i.toString()

fun isValidEmail(target: CharSequence?): Boolean {
    return if (target == null) false else android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
}

fun generateBarcodeForSeries(catalogSeries: CatalogSeries): CatalogSeries {
    val re = Regex("[-T:]")
    if (!catalogSeries.number.isNullOrBlank() && catalogSeries.validUntil.isNullOrBlank()) {
        catalogSeries.barcode = "${catalogSeries.number}${re.replace(catalogSeries.validUntil.toString(), "").drop(2).dropLast(2)}"
    } else if (!catalogSeries.number.isNullOrBlank()) {
        catalogSeries.barcode = catalogSeries.number
    } else if (!catalogSeries.validUntil.isNullOrBlank()) {
        catalogSeries.barcode = re.replace(catalogSeries.validUntil.toString(), "").drop(2).dropLast(2)
    }
    return catalogSeries
}

fun revertBarcodeAndGetSeries(barcode: String): CatalogSeries {
    val catalogSeries = CatalogSeries()
    if (barcode.length > 7) {
        val dateTime = barcode.drop(barcode.length - 8)
        catalogSeries.validUntilBegin = "20${dateTime.substring(4..5)}-${dateTime.substring(2..3)}-${dateTime.substring(0..1)}T${dateTime.substring(6..7)}:00:00"
        catalogSeries.validUntilEnd = "20${dateTime.substring(4..5)}-${dateTime.substring(2..3)}-${dateTime.substring(0..1)}T${dateTime.substring(6..7)}:00:59"
        catalogSeries.numberReq = barcode.dropLast(8)
    } else {
        catalogSeries.numberReq = barcode
    }
    return catalogSeries
}