package ru.smartms.smallsmarty.utils

class BillingResponseCodeException(message: String?) : Exception(message)