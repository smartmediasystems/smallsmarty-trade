@file:Suppress("ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE")

package ru.smartms.smallsmarty.utils

import android.annotation.SuppressLint
import com.google.gson.GsonBuilder
import android.os.Environment
import android.util.Log
import com.google.gson.stream.JsonReader
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature
import java.io.*
import java.io.File.separator
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
@Throws(IOException::class)
fun saveJSONToFile(doc: Any?, appName: String, partFileName: String): File {
    val directory = File("${Environment.getExternalStorageDirectory()}$separator$appName")
    directory.mkdirs()
    val path = directory.path
    val sdf = SimpleDateFormat("yyyy_MM_dd_HHmmss")
    val currentDateAndTime = sdf.format(Date())
    val fileName = "$path/${currentDateAndTime}_${partFileName.substring(0, if (partFileName.length >= 50) 50 else partFileName.length)}_${doc!!::class.simpleName}.json"
    val writer = FileWriter(fileName)
    val gson = GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(DocMovingGoods::class.java, DocMovingGoodsSerializer())
            .create()
//    val js = gson.toJson(doc)
    gson.toJson(doc, writer)
    writer.close()
    return File(fileName)
}

fun loadDataFromJSONFile(path: String) {
    try {
        val reader: JsonReader
        val gson = GsonBuilder().create()
        reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
        reader.beginObject()
        while (reader.hasNext()) {
            val name = reader.nextName()
            when (name) {
                "Номенклатура" -> {
                    var catalogNomenclature: CatalogNomenclature?
                    try {
                        reader.beginArray()
                        while (reader.hasNext()) {
                            catalogNomenclature = gson.fromJson(reader, CatalogNomenclature::class.java)

                        }
                        reader.endArray()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                "ОсновныеСредства" -> {
                    try {
                        reader.beginArray()
                        var tabDocInventoryOS: TabDocInventoryOS?
                        while (reader.hasNext()) {
                            tabDocInventoryOS = gson.fromJson(reader, TabDocInventoryOS::class.java)
                            if (tabDocInventoryOS != null) {

                            }
                        }
                        reader.endArray()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                "ИнвентаризацияОС" -> {
                    try {
                        reader.beginArray()
                        var docInventoryOS: DocInventoryOS?
                        while (reader.hasNext()) {
                            docInventoryOS = gson.fromJson(reader, DocInventoryOS::class.java)
                        }
                        reader.endArray()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }


                }
                "ИнвентаризацияТоваровНаСкладе" -> {
                    try {
                        reader.beginArray()
                        var docRecountGoods: DocRecountGoods?
                        while (reader.hasNext()) {
                            docRecountGoods = gson.fromJson(reader, DocRecountGoods::class.java)
                        }
                        reader.endArray()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                "Штрихкоды" -> {
                    try {
                        reader.beginArray()
                        var infRegBarcodeNomenclature: InfRegBarcodeNomenclature?
                        while (reader.hasNext()) {
                            infRegBarcodeNomenclature = gson.fromJson(reader, InfRegBarcodeNomenclature::class.java)

                        }
                        reader.endArray()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
                else -> reader.skipValue()
            }
        }
        reader.endObject()
        reader.close()
    } catch (t: Throwable) {
        t.printStackTrace()
        Log.e("GSON", t.message)
    }
}