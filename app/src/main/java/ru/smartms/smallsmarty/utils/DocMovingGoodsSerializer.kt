package ru.smartms.smallsmarty.utils

import com.google.gson.JsonSerializer
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import com.google.gson.JsonObject
import com.google.gson.JsonArray
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.util.regex.Pattern


class DocMovingGoodsSerializer : JsonSerializer<DocMovingGoods> {

    override fun serialize(src: DocMovingGoods, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Date", src.date)
        jsonObject.addProperty("Комментарий", src.comment)
        val pattern = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f‌​]{4}-[0-9a-f]{12}$")
        if (pattern.matcher(src.refKey).matches()) {
            jsonObject.addProperty("Ref_Key", src.refKey)
            jsonObject.addProperty("Number", src.number)
        }
        var jsonArrayObject: JsonObject
        jsonObject.addProperty("Организация_Key", src.catalogOrganizationKey)
        val jsonArray = JsonArray()
        for (i in src.goods.indices) {
            jsonArrayObject = JsonObject()
            jsonArrayObject.addProperty("LineNumber", src.goods[i].lineNumber)
            jsonArrayObject.addProperty("Номенклатура_Key", src.goods[i].catalogNomenclatureKey)
            jsonArrayObject.addProperty("Характеристика_Key", src.goods[i].catalogCharacteristicKey)
            jsonArrayObject.addProperty("Серия_Key", src.goods[i].seriesKey)
            jsonArrayObject.addProperty("Количество", src.goods[i].qty)
            jsonArrayObject.addProperty("КоличествоУпаковок", src.goods[i].pkgQty)
            jsonArray.add(jsonArrayObject)
        }
        jsonObject.add("Товары", jsonArray)
        return jsonObject
    }

}