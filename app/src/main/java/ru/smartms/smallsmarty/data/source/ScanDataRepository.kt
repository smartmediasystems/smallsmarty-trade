package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.db.dao.ScanDataDao
import ru.smartms.smallsmarty.db.entity.ScanData

@Singleton
class ScanDataRepository @Inject constructor(private val scanDataDao: ScanDataDao) {

    fun getScanData(): LiveData<List<ScanData>> = scanDataDao.getAll()

    fun deleteAll() = GlobalScope.launch { scanDataDao.deleteAll() }

    fun insert(scanData: ScanData) = GlobalScope.launch { scanDataDao.insert(scanData) }
}