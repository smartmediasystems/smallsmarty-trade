package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import retrofit2.Call
import ru.smartms.smallsmarty.db.dao.DocRecountGoodsDao
import ru.smartms.smallsmarty.db.dao.TabDocRecountGoodsDao
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoodses
import ru.smartms.smallsmarty.utils.saveJSONToFile
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader


@Singleton
class DocRecountGoodsRepository @Inject constructor(private val restService: RestService, private val docRecountGoodsDao: DocRecountGoodsDao, val tabDocRecountGoodsDao: TabDocRecountGoodsDao) {

    fun getAll(): LiveData<List<DocRecountGoods>> {
        return docRecountGoodsDao.getAllLiveData()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null): Call<DocRecountGoodses> =
            restService.getDocumentRecountGoodses(
                    top,
                    skip,
                    select,
                    filter,
                    expand)

    fun insertAll(docRecountGoods: MutableList<DocRecountGoods>?) {
        if (docRecountGoods == null) {
            return
        }
        val docRecountGoodsesList = docRecountGoodsDao.getAll()
        docRecountGoods.map {
            for (docRecountGoodsDB in docRecountGoodsesList) {
                if (docRecountGoodsDB.refKey == it.refKey && docRecountGoodsDB.isEdit) {
                    it.isEdit = true
                }
            }
        }
        val docs = docRecountGoods.dropWhile { it.isEdit }
        docRecountGoodsDao.insertAll(docs)
        for (doc in docs) {
            tabDocRecountGoodsDao.delete(doc.refKey)
            for (tabDocRecountGoods in doc.goods) {
                tabDocRecountGoodsDao.insert(tabDocRecountGoods)
            }
        }
    }

    fun deleteAll() {
        val docRecountGoodses = docRecountGoodsDao.getAll()
        for (docRecountGoods in docRecountGoodses) {
            tabDocRecountGoodsDao.delete(docRecountGoods.refKey)
        }
        docRecountGoodsDao.deleteAll()
    }

    fun get(refKeyDoc: String): DocRecountGoods {
        return docRecountGoodsDao.get(refKeyDoc)
    }

    fun upload(docRecountGoods: DocRecountGoods): Call<DocRecountGoods> {
        return restService.patchDocumentRecountGoods(docRecountGoods.refKey, docRecountGoods)
    }

    fun saveToFile(docRecountGoods: DocRecountGoods, appName: String): File? {
        return saveJSONToFile(docRecountGoods, appName, if (docRecountGoods.comment != null) docRecountGoods.comment!!.replace("[^A-Za-z0-9\\p{InCyrillic}]".toRegex(), "_") else "")
    }

    fun delete(refKeyDoc: String) {
        docRecountGoodsDao.delete(refKeyDoc)
    }

    fun setIsEditDocRecountGoods(refKeyDoc: String, isEdit: Boolean) {
        docRecountGoodsDao.setIsEditDocRecountGoods(refKeyDoc, isEdit)
    }

    fun loadFromFile(path: String): List<DocRecountGoods> {
        val docRecountGoodses = ArrayList<DocRecountGoods>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ИнвентаризацияТоваровНаСкладе" -> {
                        var docRecountGoods: DocRecountGoods?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                docRecountGoods = gson.fromJson(reader, DocRecountGoods::class.java)
                                if (docRecountGoods != null) {
                                    docRecountGoodses.add(docRecountGoods)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return docRecountGoodses
    }

    fun getLastDocRecountGoodsOrderById(): DocRecountGoods? {
        return docRecountGoodsDao.getLastDocRecountGoodsOrderById()
    }

    fun insert(docRecountGoods: DocRecountGoods) {
        docRecountGoodsDao.insert(docRecountGoods)
    }

    fun uploadNew(docRecountGoods: DocRecountGoods): Call<DocRecountGoods> = restService.postDocRecountGoods(docRecountGoods)
}