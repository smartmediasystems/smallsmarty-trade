package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import ru.smartms.smallsmarty.db.dao.CatalogNomenclatureAllTabDocRecountGoodsDao
import ru.smartms.smallsmarty.db.dao.TabDocRecountGoodsDao
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.TabDocRecountGoods


@Singleton
class TabDocRecountGoodsRepository @Inject constructor(
        private val tabDocRecountGoodsDao: TabDocRecountGoodsDao,
        private val catalogNomenclatureAllTabDocRecountGoodsDao: CatalogNomenclatureAllTabDocRecountGoodsDao) {

    fun getAll(refKey: String): LiveData<List<CatalogNomenclatureAllTabDocRecountGoods>> = catalogNomenclatureAllTabDocRecountGoodsDao.getAll(refKey)
    fun getCatalogNomenclatureAllTabDocRecountGoods(id: Long): LiveData<CatalogNomenclatureAllTabDocRecountGoods> = catalogNomenclatureAllTabDocRecountGoodsDao.get(id)
    fun update(tabDocRecountGoods: TabDocRecountGoods?) {
        tabDocRecountGoodsDao.update(tabDocRecountGoods)
    }

    fun getTabDocRecountGoods(id: Long): TabDocRecountGoods {
        return tabDocRecountGoodsDao.get(id)
    }

    fun getTabDocRecountGoods(refKeyDoc: String): List<TabDocRecountGoods> {
        return tabDocRecountGoodsDao.get(refKeyDoc)
    }

    fun getTabDocRecountGoodsGetViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc: String,
                                                                   catalogNomenclatureKey: String,
                                                                   catalogCharacteristicKey: String? = null,
                                                                   catalogSeriesKey: String? = null): TabDocRecountGoods {
        return tabDocRecountGoodsDao.getViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc, catalogNomenclatureKey, catalogCharacteristicKey, catalogSeriesKey)
    }

    fun insert(tabDocRecountGoods: TabDocRecountGoods?) {
        tabDocRecountGoodsDao.insert(tabDocRecountGoods)
    }

    fun getLastTabDocRecountGoods(refKeyDoc: String?): TabDocRecountGoods? {
        return tabDocRecountGoodsDao.getLastTabDocRecountGoods(refKeyDoc)
    }

    fun delete(refKeyDoc: String) {
        tabDocRecountGoodsDao.delete(refKeyDoc)
    }

    fun delete(id: Long) {
        tabDocRecountGoodsDao.delete(id)
    }
}