package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.CatalogOperationObjectDao
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObjects
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

@Singleton
class CatalogOperationObjectRepository @Inject constructor(private val restService: RestService, private val catalogOperationObjectDao: CatalogOperationObjectDao) {

    fun getCatalogOperationObjectsLiveData(): LiveData<List<CatalogOperationObject>> {
        return catalogOperationObjectDao.getAllCatalogOperationObject()
    }

    fun getCatalogOperationObjects(): List<CatalogOperationObject> {
        return catalogOperationObjectDao.getCatalogOperationObjects()
    }

    fun getCatalogOperationObjectsLiveData(query: String): List<CatalogOperationObject> {
        return catalogOperationObjectDao.getCatalogOperationObject(query)
    }

    fun insertAll(catalogOperationObjects: List<CatalogOperationObject>) {
        catalogOperationObjectDao.insertAll(catalogOperationObjects)
    }

    fun deleteAll() {
        catalogOperationObjectDao.deleteAllCatalogOperationObject()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null, orderBy: String? = ORDER_BY_CATALOG): Call<CatalogOperationObjects> = restService.getCatalogOperationObjects(
            top,
            skip,
            select,
            filter,
            expand,
            orderBy)


    fun load() = GlobalScope.launch {
        var catalogOperationObjects: CatalogOperationObjects
        var i = 0
        try {
            do {
                i++
                catalogOperationObjects = restService.getCatalogOperationObjects(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        IS_FOLDER_FILTER,
                        null,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogOperationObjectDao.insertAll(catalogOperationObjects.value)
            } while (catalogOperationObjects.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insert(catalogOperationObject: CatalogOperationObject) {
        catalogOperationObjectDao.insert(catalogOperationObject)
    }

    fun getCatalogOperationObject(barcode: String): CatalogOperationObject? {
        return catalogOperationObjectDao.get(barcode)
    }

    fun getCatalogOperationObjectRefKey(refKey: String): CatalogOperationObject? {
        return catalogOperationObjectDao.getFromRefKey(refKey)
    }

    fun loadFromFile(path: String): List<CatalogOperationObject> {
        val catalogOperationObjects = ArrayList<CatalogOperationObject>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ОсновныеСредства" -> {
                        var catalogOperationObject: CatalogOperationObject?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                catalogOperationObject = gson.fromJson(reader, CatalogOperationObject::class.java)
                                if (catalogOperationObject != null) {
                                    catalogOperationObjects.add(catalogOperationObject)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return catalogOperationObjects
    }

    fun delete(refKey: String) {
        catalogOperationObjectDao.delete(refKey)
    }

}