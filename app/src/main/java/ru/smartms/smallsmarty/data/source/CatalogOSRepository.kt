package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.CatalogOSDao
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOSes
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

@Singleton
class CatalogOSRepository @Inject constructor(private val restService: RestService, private val catalogOSDao: CatalogOSDao) {

    fun getCatalogOSesLiveData(): LiveData<List<CatalogOS>> {
        return catalogOSDao.getAllCatalogOS()
    }

    fun getCatalogOSes(): List<CatalogOS> {
        return catalogOSDao.getCatalogOSes()
    }

    fun getCatalogOSesLiveData(query: String): List<CatalogOS> {
        return catalogOSDao.getCatalogOS(query)
    }

    fun insertAll(catalogOSes: List<CatalogOS>) {
        catalogOSDao.insertAll(catalogOSes)
    }

    fun deleteAll() {
        catalogOSDao.deleteAllCatalogOS()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null, orderBy: String? = ORDER_BY_CATALOG): Call<CatalogOSes> = restService.getCatalogOSes(
            top,
            skip,
            select,
            filter,
            expand,
            orderBy)


    fun load() = GlobalScope.launch {
        var catalogOSs: CatalogOSes
        var i = 0
        try {
            do {
                i++
                catalogOSs = restService.getCatalogOSes(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        IS_FOLDER_FILTER,
                        null,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogOSDao.insertAll(catalogOSs.value)
            } while (catalogOSs.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insert(catalogOS: CatalogOS) {
        catalogOSDao.insert(catalogOS)
    }

    fun getCatalogOS(barcode: String): CatalogOS? {
        return catalogOSDao.get(barcode)
    }

    fun getCatalogOSRefKey(refKey: String): CatalogOS? {
        return catalogOSDao.getFromRefKey(refKey)
    }

    fun loadFromFile(path: String): List<CatalogOS> {
        val catalogOSes = ArrayList<CatalogOS>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ОсновныеСредства" -> {
                        var catalogOS: CatalogOS?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                catalogOS = gson.fromJson(reader, CatalogOS::class.java)
                                if (catalogOS != null) {
                                    catalogOSes.add(catalogOS)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return catalogOSes
    }

    fun delete(refKey: String) {
        catalogOSDao.delete(refKey)
    }

}