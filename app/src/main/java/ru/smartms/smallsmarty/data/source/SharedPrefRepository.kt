package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.content.SharedPreferences
import ru.smartms.smallsmarty.utils.LOCALHOST_IP
import ru.smartms.smallsmarty.utils.SharedPreferenceLiveData
import ru.smartms.smallsmarty.utils.booleanLiveData

@Singleton
class SharedPrefRepository @Inject constructor(private val sharedPreferences: SharedPreferences) {

    companion object {
        const val PURCHASE_TOKEN = "PurchaseToken"
        const val DB_NAME = "dbName"
        const val URL = "url"
        const val SCAN_ONE_BY_ONE = "scan_one_by_one_cbx"
        const val IS_LICENSE_ACTIVE = "IsLicenseActive"
        const val EMAIL = "Email"
        const val PASSWORD = "Password"
    }

    fun getDbName(): String = sharedPreferences.getString(DB_NAME, "") ?: ""

    fun getBaseUrl(): String {
        val url = sharedPreferences.getString(URL, LOCALHOST_IP) ?: LOCALHOST_IP
        return if (url.isBlank()) {
            LOCALHOST_IP
        } else {
            url
        }
    }

    fun getScanOneByOne(): Boolean = sharedPreferences.getBoolean(SCAN_ONE_BY_ONE, true)

    fun setPurchaseToken(purchaseToken: String) {
        sharedPreferences.edit()
                .putString(PURCHASE_TOKEN, purchaseToken)
                .apply()
    }

    fun getPurchaseToken(): String = sharedPreferences.getString(PURCHASE_TOKEN, "") ?: ""

    fun getIsLicenseActive(): Boolean = sharedPreferences.getBoolean(IS_LICENSE_ACTIVE, false)

    fun setIsLicenseActive(licenseIsActive: Boolean) {
        sharedPreferences.edit()
                .putBoolean(IS_LICENSE_ACTIVE, licenseIsActive)
                .apply()
    }

    fun getEmail(): String {
        return sharedPreferences.getString(EMAIL, "") ?: ""
    }

    fun getPassword(): String {
        return sharedPreferences.getString(PASSWORD, "") ?: ""
    }

    fun setEmail(email: String) {
        sharedPreferences.edit()
                .putString(EMAIL, email)
                .apply()
    }

    fun setPassword(password: String) {
        sharedPreferences.edit()
                .putString(PASSWORD, password)
                .apply()
    }

    fun getIsLicenseActiveLiveData(): SharedPreferenceLiveData<Boolean> {
        return sharedPreferences.booleanLiveData(IS_LICENSE_ACTIVE, false)
    }

    fun getPortPath(): String = sharedPreferences.getString("portPath", "/dev/ttyS2")
            ?: "/dev/ttyS2"

    fun isAllowDataSync(): Boolean = sharedPreferences.getBoolean("allow_data_sync", false)
}