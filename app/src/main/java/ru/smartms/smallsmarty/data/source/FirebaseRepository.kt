package ru.smartms.smallsmarty.data.source

import javax.inject.Singleton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.db.entity.PurchaseInnApp
import javax.inject.Inject

@Singleton
class FirebaseRepository(val db: FirebaseDatabase) {

    companion object {
        const val PURCHASE_TOKEN = "purchaseToken"
        const val PURCHASES = "purchases"
        const val PURCHASES_HISTORY = "purchases_history"
    }

    @Inject
    lateinit var auth: FirebaseAuth

    private val currentUser: FirebaseUser? by lazy { auth.currentUser }
    private val dbRef = db.getReference("users")

    init {
        App.component.inject(this)
    }


    fun setPurchaseInApp(purchaseInnApp: PurchaseInnApp) {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES)
                    .push()
                    .setValue(purchaseInnApp)
        }
    }

    fun setPurchaseInAppHistory(purchaseInnApp: PurchaseInnApp) {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES_HISTORY)
                    .push()
                    .setValue(purchaseInnApp)
        }
    }

    fun setPurchaseValueEventListener(valueEventListener: ValueEventListener, purchaseInnApp: PurchaseInnApp) {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES)
                    .orderByChild(PURCHASE_TOKEN)
//                    .equalTo(purchaseInnApp.purchaseToken)
                    .addValueEventListener(valueEventListener)
        }
    }

    fun setPurchaseHistoryValueEventListener(valueEventListener: ValueEventListener) {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES_HISTORY)
                    .orderByChild(PURCHASE_TOKEN)
                    .addValueEventListener(valueEventListener)
        }
    }

    fun setPurchaseChildEventListener(childEventListener: ChildEventListener, purchaseInnApp: PurchaseInnApp) {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES)
                    .orderByChild(PURCHASE_TOKEN)
//                    .equalTo(purchaseInnApp.purchaseToken)
                    .addChildEventListener(childEventListener)
        }
    }

    fun deleteAll() {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES)
                    .removeValue()
        }
    }

    fun deletePurchaseHistory() {
        currentUser?.uid?.let {
            dbRef.child(it)
                    .child(BuildConfig.APP_VERSION)
                    .child(PURCHASES_HISTORY)
                    .removeValue()
        }
    }
}