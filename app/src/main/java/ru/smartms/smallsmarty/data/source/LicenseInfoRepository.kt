package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import retrofit2.Call
import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.LicenseInfoDao
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo
import ru.smartms.smallsmarty.utils.REQUEST_KEY_ACTIVATE
import ru.smartms.smallsmarty.utils.REQUEST_KEY_CHECK

@Singleton
class LicenseInfoRepository @Inject constructor(private val restService: RestService, private val licenseInfoDao: LicenseInfoDao) {

    fun get(): LiveData<List<LicenseInfo>> {
        return licenseInfoDao.getAll()
    }

    fun getActivatedLiveData(): LiveData<LicenseInfo> {
        return licenseInfoDao.getActivatedLiveData()
    }

    fun getActivated(): LicenseInfo? {
        return licenseInfoDao.getActivated()
    }

    fun deleteAll() = licenseInfoDao.deleteAll()


    fun insert(licenseInfo: LicenseInfo) = licenseInfoDao.insert(licenseInfo)


    fun activate(licenseKey: String, email: String): Call<LicenseInfo> {
        return restService.reqLicenceInfo(request = REQUEST_KEY_ACTIVATE, email = email, licenseKey = licenseKey)
    }

    fun check(licenseKey: String, email: String): Call<LicenseInfo> {
        return restService.reqLicenceInfo(request = REQUEST_KEY_CHECK, email = email, licenseKey = licenseKey)
    }
}