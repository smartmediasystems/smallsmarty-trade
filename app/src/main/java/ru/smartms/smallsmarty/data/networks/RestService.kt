package ru.smartms.smallsmarty.data.networks

import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclatures
import retrofit2.http.PATCH
import retrofit2.http.POST
import ru.smartms.smallsmarty.db.entity.OdataMetaData
import ru.smartms.smallsmarty.db.entity.ProductPurchase
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStoreOData
import ru.smartms.smallsmarty.db.entity.catalog.*
import ru.smartms.smallsmarty.db.entity.document.*
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo
import ru.smartms.smallsmarty.utils.PRODUCT_ID
import ru.smartms.smallsmarty.utils.WC_API


@Suppress("unused")
interface RestService {

    @GET("odata/standard.odata/#metadata")
    fun getMetadata(): Call<OdataMetaData>

    //Справочники
    @GET("${BuildConfig.ODATA_URL}Catalog_Номенклатура")
    fun getCatalogNomenclatures(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null,
            @Query("\$orderby") orderBy: String? = null): Call<CatalogNomenclatures>

    @GET("${BuildConfig.ODATA_URL}Catalog_ХарактеристикиНоменклатуры")
    fun getCatalogCharacteristics(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null,
            @Query("\$orderby") orderBy: String? = null): Call<CatalogCharacteristics>

    @GET("${BuildConfig.ODATA_URL}Catalog_СерииНоменклатуры")
    fun getCatalogSeries(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null,
            @Query("\$orderby") orderBy: String? = null): Call<CatalogSeriesOData>

    //Регистры сведений
    @GET("${BuildConfig.ODATA_URL}InformationRegister_ШтрихкодыНоменклатуры")
    fun getInfRegBarcodes(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null): Call<InfRegBarcodeNomenclatures>

    @GET("${BuildConfig.ODATA_URL}Catalog_ОсновныеСредства")
    fun getCatalogOSes(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null,
            @Query("\$orderby") orderBy: String? = null): Call<CatalogOSes>

    @GET("${BuildConfig.ODATA_URL}Catalog_ОбъектыЭксплуатации")
    fun getCatalogOperationObjects(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null,
            @Query("\$orderby") orderBy: String? = null): Call<CatalogOperationObjects>

    //Документы
    @GET("${BuildConfig.ODATA_URL}Document_ПересчетТоваров")
    fun getDocumentRecountGoodses(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null): Call<DocRecountGoodses>

    @PATCH("${BuildConfig.ODATA_URL}Document_ПересчетТоваров(guid'{guid}')")
    fun patchDocumentRecountGoods(@Path("guid") guid: String, @Body req: DocRecountGoods): Call<DocRecountGoods>

    @POST("${BuildConfig.ODATA_URL}Document_ПересчетТоваров")
    fun postDocRecountGoods(@Body req: DocRecountGoods, @Query("\$expand") expand: String? = null): Call<DocRecountGoods>

    @PATCH("${BuildConfig.ODATA_URL}Document_ПеремещениеТоваров(guid'{guid}')")
    fun patchDocMovingGoods(@Path("guid") guid: String, @Body req: DocMovingGoods): Call<DocMovingGoods>

    @POST("${BuildConfig.ODATA_URL}Document_ПеремещениеТоваров")
    fun postDocMovingGoods(@Body req: DocMovingGoods, @Query("\$expand") expand: String? = null): Call<DocMovingGoods>

    @GET("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС")
    fun getDocumentInventoryOSes(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null): Call<DocInventoryOSes>

    @PATCH("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС(guid'{guid}')")
    fun patchDocInventoryOS(@Path("guid") guid: String, @Body req: DocInventoryOS): Call<DocInventoryOS>

    @POST("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС")
    fun postDocInventoryOS(@Body req: DocInventoryOS, @Query("\$expand") expand: String? = null): Call<DocInventoryOS>

    @GET("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС")
    fun getDocumentInventoryOperationObjects(
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null): Call<DocInventoryOperationObjects>

    @PATCH("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС(guid'{guid}')")
    fun patchDocInventoryOperationObject(@Path("guid") guid: String, @Body req: DocInventoryOperationObject): Call<DocInventoryOperationObject>

    @POST("${BuildConfig.ODATA_URL}Document_ИнвентаризацияОС")
    fun postDocInventoryOperationObject(@Body req: DocInventoryOperationObject, @Query("\$expand") expand: String? = null): Call<DocInventoryOperationObject>

    //    LicenseInfo
    @GET("woocommerce/")
    fun reqLicenceInfo(
            @Query("wc-api") wcApi: String? = WC_API,
            @Query("request") request: String? = null,
            @Query("email") email: String? = null,
            @Query("license_key") licenseKey: String? = null,
            @Query("product_id") productId: String? = PRODUCT_ID): Call<LicenseInfo>

    //Регистры накопления
    @GET("${BuildConfig.ODATA_URL}AccumulationRegister_ТоварыНаСкладах/{func}")
    fun getARGoodsInStore(
            @Path("func") func: String? = null,
            @Query("\$top") top: Int? = null,
            @Query("\$skip") skip: Int? = null,
            @Query("\$select") select: String? = null,
            @Query("\$filter") filter: String? = null,
            @Query("\$expand") expand: String? = null): Call<ARGoodsInStoreOData>

    //Billing
    @GET("androidpublisher/v3/applications/{packageName}/purchases/products/{productId}/tokens/{token}")
    fun getProductPurchase(
            @Path("packageName") packageName: String,
            @Path("productId") productId: String,
            @Path("token") token: String): Call<ProductPurchase>
}