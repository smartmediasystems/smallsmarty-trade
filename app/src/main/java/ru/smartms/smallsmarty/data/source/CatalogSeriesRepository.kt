package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.CatalogSeriesDao
import javax.inject.Inject
import javax.inject.Singleton
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeriesOData
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import ru.smartms.smallsmarty.utils.generateBarcodeForSeries
import ru.smartms.smallsmarty.utils.revertBarcodeAndGetSeries
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

@Singleton
class CatalogSeriesRepository @Inject constructor(private val restService: RestService, private val catalogSeriesDao: CatalogSeriesDao) {

    fun insertAll(catalogSeriesList: List<CatalogSeries>?) {
        catalogSeriesDao.insertAll(catalogSeriesList)
    }

    fun deleteAll() {
        catalogSeriesDao.deleteAllCatalogSeries()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null, orderBy: String? = null): Call<CatalogSeriesOData> = restService.getCatalogSeries(
            top,
            skip,
            select,
            filter,
            expand,
            orderBy)


    fun load() = GlobalScope.launch {
        var catalogSeriesOData: CatalogSeriesOData
        var i = 0
        try {
            do {
                i++
                catalogSeriesOData = restService.getCatalogSeries(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogSeriesDao.insertAll(catalogSeriesOData.value)
            } while (catalogSeriesOData.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insert(catalogSeries: CatalogSeries) {
        catalogSeriesDao.insert(catalogSeries)
    }

    fun loadFromFile(path: String): List<CatalogSeries> {
        val catalogSeriesList = ArrayList<CatalogSeries>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "Номенклатура" -> {
                        var catalogSeries: CatalogSeries?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                catalogSeries = gson.fromJson(reader, CatalogSeries::class.java)
                                if (catalogSeries != null) {
                                    generateBarcodeForSeries(catalogSeries)
                                    catalogSeriesList.add(catalogSeries)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return catalogSeriesList
    }

    fun delete(refKey: String?) {
        catalogSeriesDao.delete(refKey)
    }

    fun getByBarcode(barcode: String): CatalogSeries? {
        var catalogSeries = catalogSeriesDao.getByBarcode(barcode)
        if (catalogSeries == null) {
            val catalogSeriesTemp = revertBarcodeAndGetSeries(barcode)
            catalogSeries = catalogSeriesDao.getByBarcodeNumberValidUntil(catalogSeriesTemp.validUntilBegin, catalogSeriesTemp.numberReq)
        }
        return catalogSeries
    }

}