package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import ru.smartms.smallsmarty.db.dao.CatalogOperationObjectAllTabDocInventoryOperationObjectDao
import ru.smartms.smallsmarty.db.dao.TabDocInventoryOperationObjectDao
import ru.smartms.smallsmarty.db.entity.CatalogOperationObjectAllTabDocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOperationObject


@Singleton
class TabDocInventoryOperationObjectRepository @Inject constructor(
        private val tabDocInventoryOperationObjectDao: TabDocInventoryOperationObjectDao,
        private val catalogOperationObjectAllTabDocInventoryOperationObjectDao: CatalogOperationObjectAllTabDocInventoryOperationObjectDao) {

    fun getAllLiveData(refKey: String): LiveData<List<CatalogOperationObjectAllTabDocInventoryOperationObject>> = catalogOperationObjectAllTabDocInventoryOperationObjectDao.getAllLiveData(refKey)
    fun getCatalogOperationObjectAllTabDocInventoryOperationObject(id: Long): LiveData<CatalogOperationObjectAllTabDocInventoryOperationObject> = catalogOperationObjectAllTabDocInventoryOperationObjectDao.get(id)
    fun update(tabDocInventoryOperationObject: TabDocInventoryOperationObject?) {
        tabDocInventoryOperationObjectDao.update(tabDocInventoryOperationObject)
    }

    fun getTabDocInventoryOperationObject(id: Long): TabDocInventoryOperationObject {
        return tabDocInventoryOperationObjectDao.get(id)
    }

    fun getTabDocInventoryOperationObject(refKeyDoc: String): List<TabDocInventoryOperationObject> {
        return tabDocInventoryOperationObjectDao.get(refKeyDoc)
    }

    fun getTabDocInventoryOperationObjectgetViaCatalogOperationObjectKeyRefKeyDoc(refKeyDoc: String, catalogOperationObjectKey: String): TabDocInventoryOperationObject {
        return tabDocInventoryOperationObjectDao.getViaCatalogOperationObjectKeyRefKeyDoc(refKeyDoc, catalogOperationObjectKey)
    }

    fun insert(tabDocInventoryOperationObject: TabDocInventoryOperationObject?) {
        tabDocInventoryOperationObjectDao.insert(tabDocInventoryOperationObject)
    }

    fun getLastTabDocInventoryOperationObject(refKeyDoc: String?): TabDocInventoryOperationObject? {
        return tabDocInventoryOperationObjectDao.getLastTabDocInventoryOperationObject(refKeyDoc)
    }

    fun delete(refKeyDoc: String) {
        tabDocInventoryOperationObjectDao.delete(refKeyDoc)
    }

    fun delete(id: Long) {
        tabDocInventoryOperationObjectDao.delete(id)
    }

    fun updateIsAct(checked: Boolean, id: Long) {
        tabDocInventoryOperationObjectDao.updateIsAct(checked, id)
    }

    fun getTabDocInventoryOperationObjectLiveData(refKeyDoc: String): LiveData<List<TabDocInventoryOperationObject>> {
        return tabDocInventoryOperationObjectDao.getAllLiveData(refKeyDoc)
    }
}