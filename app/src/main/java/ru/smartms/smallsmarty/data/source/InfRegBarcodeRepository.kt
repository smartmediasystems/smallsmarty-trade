package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.db.dao.InfRegBarcodeNomenclatureDao
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclatures
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.EXPAND_NOMENCLATURE
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader


@Singleton
class InfRegBarcodeRepository @Inject constructor(private val restService: RestService, private val infRegBarcodeNomenclatureDao: InfRegBarcodeNomenclatureDao) {

    fun getAll(): LiveData<List<InfRegBarcodeNomenclature>> {
        return infRegBarcodeNomenclatureDao.getAll()
    }

    fun deleteAll() = infRegBarcodeNomenclatureDao.deleteAll()

    fun getAllFromREST(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null): Call<InfRegBarcodeNomenclatures> =
            restService.getInfRegBarcodes(
                    top,
                    skip,
                    select,
                    filter,
                    EXPAND_NOMENCLATURE
            )

    suspend fun load() {
        deleteAll()
        var infRegBarcodeNomenclatures: InfRegBarcodeNomenclatures
        var i = 0
        try {
            do {
                i++
                infRegBarcodeNomenclatures = getAllFromREST(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        null
                ).await()
                infRegBarcodeNomenclatureDao.insertAll(infRegBarcodeNomenclatures.value)
            } while (infRegBarcodeNomenclatures.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insertAll(infRegBarcodeNomenclatures: List<InfRegBarcodeNomenclature>) {
        infRegBarcodeNomenclatureDao.insertAll(infRegBarcodeNomenclatures)
    }

    fun get(barcode: String): InfRegBarcodeNomenclature? {
        return infRegBarcodeNomenclatureDao.get(barcode)
    }

    fun getFromREST(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = EXPAND_NOMENCLATURE): Call<InfRegBarcodeNomenclatures> =
            restService.getInfRegBarcodes(
                    top,
                    skip,
                    select,
                    filter,
                    expand
            )

    fun insert(infRegBarcodeNomenclature: InfRegBarcodeNomenclature) {
        infRegBarcodeNomenclatureDao.insert(infRegBarcodeNomenclature)
    }

    fun loadFromFile(path: String): List<InfRegBarcodeNomenclature> {
        val infRegBarcodeNomenclatures = ArrayList<InfRegBarcodeNomenclature>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "Штрихкоды" -> {
                        var infRegBarcodeNomenclature: InfRegBarcodeNomenclature?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                infRegBarcodeNomenclature = gson.fromJson(reader, InfRegBarcodeNomenclature::class.java)
                                if (infRegBarcodeNomenclature != null) {
                                    infRegBarcodeNomenclatures.add(infRegBarcodeNomenclature)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return infRegBarcodeNomenclatures
    }
}