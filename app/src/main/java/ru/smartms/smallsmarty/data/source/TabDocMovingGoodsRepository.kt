package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import ru.smartms.smallsmarty.db.dao.CatalogNomenclatureAllTabDocMovingGoodsDao
import ru.smartms.smallsmarty.db.dao.TabDocMovingGoodsDao
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods
import ru.smartms.smallsmarty.db.entity.document.TabDocMovingGoods


@Singleton
class TabDocMovingGoodsRepository @Inject constructor(
        private val tabDocMovingGoodsDao: TabDocMovingGoodsDao,
        private val catalogNomenclatureAllTabDocMovingGoodsDao: CatalogNomenclatureAllTabDocMovingGoodsDao) {

    fun getAll(refKey: String): LiveData<List<CatalogNomenclatureAllTabDocMovingGoods>> = catalogNomenclatureAllTabDocMovingGoodsDao.getAll(refKey)
    fun getCatalogNomenclatureAllTabDocMovingGoods(id: Long): LiveData<CatalogNomenclatureAllTabDocMovingGoods> = catalogNomenclatureAllTabDocMovingGoodsDao.get(id)
    fun update(tabDocMovingGoods: TabDocMovingGoods?) {
        tabDocMovingGoodsDao.update(tabDocMovingGoods)
    }

    fun getTabDocMovingGoods(id: Long): TabDocMovingGoods {
        return tabDocMovingGoodsDao.get(id)
    }

    fun getTabDocMovingGoods(refKeyDoc: String): List<TabDocMovingGoods> {
        return tabDocMovingGoodsDao.get(refKeyDoc)
    }

    fun getTabDocMovingGoodsgetViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc: String, catalogNomenclatureKey: String): TabDocMovingGoods {
        return tabDocMovingGoodsDao.getViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc, catalogNomenclatureKey)
    }

    fun insert(tabDocMovingGoods: TabDocMovingGoods?) {
        tabDocMovingGoodsDao.insert(tabDocMovingGoods)
    }

    fun getLastTabDocMovingGoods(refKeyDoc: String?): TabDocMovingGoods? {
        return tabDocMovingGoodsDao.getLastTabDocMovingGoods(refKeyDoc)
    }

    fun delete(refKeyDoc: String) {
        tabDocMovingGoodsDao.delete(refKeyDoc)
    }

    fun delete(id: Long) {
        tabDocMovingGoodsDao.delete(id)
    }
}