package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.CatalogNomenclatureDao
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclatures
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

@Singleton
class CatalogNomenclatureRepository @Inject constructor(private val restService: RestService, private val catalogNomenclatureDao: CatalogNomenclatureDao) {

companion object {
    const val SELECT = "Ref_Key,DataVersion,DeletionMark,Parent_Key,Code,Description"
}

    fun getCatalogNomenclaturesLiveData(): LiveData<List<CatalogNomenclature>> {
        return catalogNomenclatureDao.getAllCatalogNomenclature()
    }

    fun getCatalogNomenclatures(): List<CatalogNomenclature> {
        return catalogNomenclatureDao.getCatalogNomenclatures()
    }

    fun getCatalogNomenclaturesLiveData(query: String): List<CatalogNomenclature> {
        return catalogNomenclatureDao.getCatalogNomenclature(query)
    }

    fun insertAll(catalogNomenclatures: List<CatalogNomenclature>?) {
        catalogNomenclatureDao.insertAll(catalogNomenclatures)
    }

    fun deleteAll() {
        catalogNomenclatureDao.deleteAllCatalogNomenclature()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = SELECT, filter: String? = null, expand: String? = null, orderBy: String? = ORDER_BY_CATALOG): Call<CatalogNomenclatures> = restService.getCatalogNomenclatures(
            top,
            skip,
            select,
            filter,
            expand,
            orderBy)


    fun load() = GlobalScope.launch {
        var catalogNomenclatures: CatalogNomenclatures
        var i = 0
        try {
            do {
                i++
                catalogNomenclatures = restService.getCatalogNomenclatures(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogNomenclatureDao.insertAll(catalogNomenclatures.value)
            } while (catalogNomenclatures.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insert(catalogNomenclature: CatalogNomenclature) {
        catalogNomenclatureDao.insert(catalogNomenclature)
    }

    fun loadFromFile(path: String): List<CatalogNomenclature> {
        val catalogNomenclatures = ArrayList<CatalogNomenclature>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "Номенклатура" -> {
                        var catalogNomenclature: CatalogNomenclature?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                catalogNomenclature = gson.fromJson(reader, CatalogNomenclature::class.java)
                                if (catalogNomenclature != null) {
                                    catalogNomenclatures.add(catalogNomenclature)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return catalogNomenclatures
    }

    fun delete(refKey: String?) {
        catalogNomenclatureDao.delete(refKey)
    }

    fun getCatalogNomenclature(catalogNomenclatureKey: String): CatalogNomenclature? = catalogNomenclatureDao.get(catalogNomenclatureKey)
}