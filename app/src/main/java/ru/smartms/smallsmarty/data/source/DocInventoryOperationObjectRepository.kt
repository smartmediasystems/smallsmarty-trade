package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import retrofit2.Call
import ru.smartms.smallsmarty.db.dao.DocInventoryOperationObjectDao
import ru.smartms.smallsmarty.db.dao.TabDocInventoryOperationObjectDao
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObjects
import ru.smartms.smallsmarty.utils.saveJSONToFile
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader


@Singleton
class DocInventoryOperationObjectRepository @Inject constructor(private val restService: RestService, private val docInventoryOperationObjectDao: DocInventoryOperationObjectDao, private val tabDocInventoryOperationObjectDao: TabDocInventoryOperationObjectDao) {

    fun getAll(): LiveData<List<DocInventoryOperationObject>> {
        return docInventoryOperationObjectDao.getAllLiveData()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null): Call<DocInventoryOperationObjects> =
            restService.getDocumentInventoryOperationObjects(
                    top,
                    skip,
                    select,
                    filter,
                    expand)

    fun insertAll(docInventoryOperationObjects: MutableList<DocInventoryOperationObject>) {
        val docInventoryOperationObjectsList = docInventoryOperationObjectDao.getAll()
        docInventoryOperationObjects.map {
            for (docInventoryGoodsDB in docInventoryOperationObjectsList) {
                if (docInventoryGoodsDB.refKey == it.refKey && docInventoryGoodsDB.isEdit) {
                    it.isEdit = true
                }
            }
        }
        val docs = docInventoryOperationObjects.dropWhile { it.isEdit }
        docInventoryOperationObjectDao.insertAll(docs)
        for (docInventoryOperationObject in docs) {
            tabDocInventoryOperationObjectDao.delete(docInventoryOperationObject.refKey)
            for (tabDocInventoryOperationObject in docInventoryOperationObject.os) {
                tabDocInventoryOperationObjectDao.insert(tabDocInventoryOperationObject)
            }
        }
    }

    fun deleteAll() {
        val docInventoryOperationObjects = docInventoryOperationObjectDao.getAll()
        for (docInventoryOperationObject in docInventoryOperationObjects) {
            tabDocInventoryOperationObjectDao.delete(docInventoryOperationObject.refKey)
        }
        docInventoryOperationObjectDao.deleteAll()
    }

    fun get(refKeyDoc: String): DocInventoryOperationObject {
        return docInventoryOperationObjectDao.get(refKeyDoc)
    }

    fun upload(docInventoryOperationObject: DocInventoryOperationObject): Call<DocInventoryOperationObject>{
        return restService.patchDocInventoryOperationObject(docInventoryOperationObject.refKey, docInventoryOperationObject)
    }

    fun saveToFile(docInventoryOperationObject: DocInventoryOperationObject, appName: String): File? {
        return saveJSONToFile(docInventoryOperationObject, appName, if (docInventoryOperationObject.comment != null) docInventoryOperationObject.comment!!.replace("[^A-Za-z0-9\\p{InCyrillic}]".toRegex(), "_") else "")
    }

    fun delete(refKeyDoc: String) {
        docInventoryOperationObjectDao.delete(refKeyDoc)
    }

    fun setIsEditDocInventoryOperationObject(refKeyDoc: String, isEdit: Boolean) {
        docInventoryOperationObjectDao.setIsEditDocInventoryOperationObject(refKeyDoc, isEdit)
    }

    fun loadFromFile(path: String): List<DocInventoryOperationObject> {
        val docInventoryOperationObjects = ArrayList<DocInventoryOperationObject>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ИнвентаризацияОС" -> {
                        var docInventoryOperationObject: DocInventoryOperationObject?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                docInventoryOperationObject = gson.fromJson(reader, DocInventoryOperationObject::class.java)
                                if (docInventoryOperationObject != null) {
                                    docInventoryOperationObjects.add(docInventoryOperationObject)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return docInventoryOperationObjects
    }

    fun getLastDocInventoryOperationObjectOrderById(): DocInventoryOperationObject? {
        return docInventoryOperationObjectDao.getLastDocInventoryOperationObjectOrderById()
    }

    fun insert(docInventoryOperationObject: DocInventoryOperationObject) {
        docInventoryOperationObjectDao.insert(docInventoryOperationObject)
    }
}