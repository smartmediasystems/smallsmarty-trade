package ru.smartms.smallsmarty.data

import android.support.v7.util.DiffUtil
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods

class TabDocRecountGoodsDiffUtil(private val oldList: List<CatalogNomenclatureAllTabDocRecountGoods>, private val newList: List<CatalogNomenclatureAllTabDocRecountGoods>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val catalogNomenclatureAllTabDocInventoryGoodsOld = oldList[oldItemPosition]
        val catalogNomenclatureAllTabDocInventoryGoodsNew = newList[newItemPosition]
        return catalogNomenclatureAllTabDocInventoryGoodsOld.equals(catalogNomenclatureAllTabDocInventoryGoodsNew)
    }
}