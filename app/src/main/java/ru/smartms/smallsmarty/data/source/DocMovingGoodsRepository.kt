package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import retrofit2.Call
import ru.smartms.smallsmarty.db.dao.DocMovingGoodsDao
import ru.smartms.smallsmarty.db.dao.TabDocMovingGoodsDao
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.utils.saveJSONToFile
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader


@Singleton
class DocMovingGoodsRepository @Inject constructor(private val restService: RestService, private val docMovingGoodsDao: DocMovingGoodsDao, private val tabDocMovingGoodsDao: TabDocMovingGoodsDao) {

    fun getAll(): LiveData<List<DocMovingGoods>> {
        return docMovingGoodsDao.getAllLiveData()
    }

    fun insertAll(docMovingMovingGoodses: MutableList<DocMovingGoods>?) {
        if (docMovingMovingGoodses == null) {
            return
        }
        val docMovingGoodsesList = docMovingGoodsDao.getAll()
        for (docMovingGoodsDB in docMovingGoodsesList) {
            for (docMovingGoods in docMovingMovingGoodses) {
                if (docMovingGoodsDB.refKey == docMovingGoods.refKey && docMovingGoodsDB.isEdit) {
                    docMovingGoods.isEdit = true
                }
            }
        }

        for (doc in docMovingMovingGoodses) {
            if (doc.isEdit) {
                docMovingMovingGoodses.remove(doc)
            }
        }
        docMovingGoodsDao.insertAll(docMovingMovingGoodses)
        for (docMovingGoods in docMovingMovingGoodses) {
            insertTabDocMovingGoodses(docMovingGoods)
        }
    }

    fun insertDocMovingGoods(docMovingGoods: DocMovingGoods) {
        docMovingGoodsDao.insert(docMovingGoods)
        insertTabDocMovingGoodses(docMovingGoods)
    }

    fun insertTabDocMovingGoodses(docMovingGoods: DocMovingGoods) {
        tabDocMovingGoodsDao.delete(docMovingGoods.refKey)
        for (tabDocMovingGoods in docMovingGoods.goods) {
            tabDocMovingGoods.refKeyDoc = docMovingGoods.refKey
            tabDocMovingGoodsDao.insert(tabDocMovingGoods)
        }
    }

    fun deleteAll() {
        val docMovingGoodses = docMovingGoodsDao.getAll()
        for (docMovingGoods in docMovingGoodses) {
            tabDocMovingGoodsDao.delete(docMovingGoods.refKey)
        }
        docMovingGoodsDao.deleteAll()
    }

    fun get(refKeyDoc: String): DocMovingGoods {
        return docMovingGoodsDao.get(refKeyDoc)
    }

    fun saveToFile(docMovingGoods: DocMovingGoods, appName: String): File? {
        return saveJSONToFile(docMovingGoods, appName, if (docMovingGoods.comment != null) docMovingGoods.comment!!.replace("[^A-Za-z0-9\\p{InCyrillic}]".toRegex(), "_") else "")
    }

    fun delete(refKeyDoc: String) {
        docMovingGoodsDao.delete(refKeyDoc)
    }

    fun setIsEditDocMovingGoods(refKeyDoc: String, isEdit: Boolean) {
        docMovingGoodsDao.setIsEditDocMovingGoods(refKeyDoc, isEdit)
    }

    fun loadFromFile(path: String): List<DocMovingGoods> {
        val docMovingGoodses = ArrayList<DocMovingGoods>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ИнвентаризацияТоваровНаСкладе" -> {
                        var docMovingGoods: DocMovingGoods?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                docMovingGoods = gson.fromJson(reader, DocMovingGoods::class.java)
                                if (docMovingGoods != null) {
                                    docMovingGoodses.add(docMovingGoods)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return docMovingGoodses
    }

    fun getLastDocMovingGoodsOrderById(): DocMovingGoods? {
        return docMovingGoodsDao.getLastDocMovingGoodsOrderById()
    }


    fun insert(docMovingGoods: DocMovingGoods) {
        docMovingGoodsDao.insert(docMovingGoods)
    }

    fun upload(docMovingGoods: DocMovingGoods): Call<DocMovingGoods> {
        return restService.patchDocMovingGoods(docMovingGoods.refKey, docMovingGoods)
    }

    fun uploadNew(docMovingGoods: DocMovingGoods): Call<DocMovingGoods> {
        return restService.postDocMovingGoods(docMovingGoods)
    }
}