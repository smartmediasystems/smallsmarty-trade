package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.dao.CatalogCharacteristicDao
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristics
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader

@Singleton
class CatalogCharacteristicRepository @Inject constructor(private val restService: RestService, private val catalogCharacteristicDao: CatalogCharacteristicDao) {

    fun getCatalogCharacteristicsLiveData(query: String, typeCatalogNomenclatureKey: String?, catalogNomenclatureKey: String?): List<CatalogCharacteristic> {
        return catalogCharacteristicDao.getCatalogCharacteristic(query, typeCatalogNomenclatureKey, catalogNomenclatureKey)
    }

    fun insertAll(catalogCharacteristics: List<CatalogCharacteristic>?) {
        catalogCharacteristicDao.insertAll(catalogCharacteristics)
    }

    fun deleteAll() {
        catalogCharacteristicDao.deleteAllCatalogCharacteristic()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null, orderBy: String? = null): Call<CatalogCharacteristics> = restService.getCatalogCharacteristics(
            top,
            skip,
            select,
            filter,
            expand,
            orderBy)


    fun load() = GlobalScope.launch {
        var catalogCharacteristics: CatalogCharacteristics
        var i = 0
        try {
            do {
                i++
                catalogCharacteristics = restService.getCatalogCharacteristics(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogCharacteristicDao.insertAll(catalogCharacteristics.value)
            } while (catalogCharacteristics.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
        }
    }

    fun insert(catalogCharacteristic: CatalogCharacteristic) {
        catalogCharacteristicDao.insert(catalogCharacteristic)
    }

    fun loadFromFile(path: String): List<CatalogCharacteristic> {
        val catalogCharacteristics = ArrayList<CatalogCharacteristic>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "Характеристики" -> {
                        var catalogCharacteristic: CatalogCharacteristic?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                catalogCharacteristic = gson.fromJson(reader, CatalogCharacteristic::class.java)
                                if (catalogCharacteristic != null) {
                                    catalogCharacteristics.add(catalogCharacteristic)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
        }
        return catalogCharacteristics
    }

    fun delete(refKey: String?) {
        catalogCharacteristicDao.delete(refKey)
    }

    fun getCatalogCharacteristic(catalogCharacteristicKey: String): CatalogCharacteristic? = catalogCharacteristicDao.get(catalogCharacteristicKey)
}