package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.entity.ProductPurchase
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.HostSelectionInterceptor
import javax.inject.Inject

class GoogleAPIsRepository @Inject constructor(private val restService: RestService, private val hostSelectionInterceptor: HostSelectionInterceptor) {


    suspend fun getPurchaseProduct(packageName: String, productId: String, token: String) : ProductPurchase? {
        setParamHostSelectionInterceptor()
        return  try {
            restService.getProductPurchase(packageName, productId, token).await()
        } catch (e: Exception) {
            null
        }
    }

    private fun setParamHostSelectionInterceptor() {
        hostSelectionInterceptor.url = "www.googleapis.com"
        hostSelectionInterceptor.scheme = "https"
        hostSelectionInterceptor.dbName = ""
        hostSelectionInterceptor.userName = ""
        hostSelectionInterceptor.password = ""
    }
}