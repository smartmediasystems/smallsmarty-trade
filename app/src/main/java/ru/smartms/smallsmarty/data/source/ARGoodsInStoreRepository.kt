package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.db.dao.ARGoodsInStoreDao
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStore
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStoreOData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries
import ru.smartms.smallsmarty.extensions.await

@Singleton
class ARGoodsInStoreRepository @Inject constructor(private val restService: RestService, private val arGoodsInStoreDao: ARGoodsInStoreDao) {

    companion object {
        const val BALANCE_AND_TURNOVERS = "BalanceAndTurnovers"
        const val TAG = "ARGoodsInStoreLoadREST"
    }

    fun insertAll(arGoodsInStoreList: List<ARGoodsInStore>?) {
        arGoodsInStoreDao.insertAll(arGoodsInStoreList)
    }

    fun deleteAll() = arGoodsInStoreDao.deleteAll()

    fun load() = GlobalScope.launch {
        var arGoodsInStoreOData: ARGoodsInStoreOData
        var i = 0
        try {
            do {
                i++
                arGoodsInStoreOData = restService.getARGoodsInStore(
                        top = BuildConfig.QTY_REQ * i,
                        skip = BuildConfig.QTY_REQ * (i - 1),
                        func = BALANCE_AND_TURNOVERS).await()
                arGoodsInStoreDao.insertAll(arGoodsInStoreOData.value)
            } while (arGoodsInStoreOData.value.isNotEmpty())

        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    suspend fun loadFromRestBalanceAndTurnovers(count: Int): ARGoodsInStoreOData = restService.getARGoodsInStore(
            func = BALANCE_AND_TURNOVERS,
            top = BuildConfig.QTY_REQ * count,
            skip = BuildConfig.QTY_REQ * (count - 1))
            .await()


    fun insert(arGoodsInStore: ARGoodsInStore) = arGoodsInStoreDao.insert(arGoodsInStore)

    fun getRecordWithQty(catalogSeries: CatalogSeries): ARGoodsInStore? = arGoodsInStoreDao.getRecordWithQty(catalogSeries.refKey)

}