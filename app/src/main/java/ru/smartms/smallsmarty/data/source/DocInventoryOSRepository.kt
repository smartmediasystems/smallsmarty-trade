package ru.smartms.smallsmarty.data.source

import ru.smartms.smallsmarty.data.networks.RestService
import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import retrofit2.Call
import ru.smartms.smallsmarty.db.dao.DocInventoryOSDao
import ru.smartms.smallsmarty.db.dao.TabDocInventoryOSDao
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOSes
import ru.smartms.smallsmarty.utils.saveJSONToFile
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader


@Singleton
class DocInventoryOSRepository @Inject constructor(private val restService: RestService, private val docInventoryOSDao: DocInventoryOSDao, private val tabDocInventoryOSDao: TabDocInventoryOSDao) {

    fun getAll(): LiveData<List<DocInventoryOS>> {
        return docInventoryOSDao.getAllLiveData()
    }

    fun loadFromRest(top: Int? = null, skip: Int? = null, select: String? = null, filter: String? = null, expand: String? = null): Call<DocInventoryOSes> =
            restService.getDocumentInventoryOSes(
                    top,
                    skip,
                    select,
                    filter,
                    expand)

    fun insertAll(docInventoryOSes: MutableList<DocInventoryOS>) {
        val docInventoryOSesList = docInventoryOSDao.getAll()
        docInventoryOSes.map {
            for (docInventoryGoodsDB in docInventoryOSesList) {
                if (docInventoryGoodsDB.refKey == it.refKey && docInventoryGoodsDB.isEdit) {
                    it.isEdit = true
                }
            }
        }
        val docs = docInventoryOSes.dropWhile { it.isEdit }
        docInventoryOSDao.insertAll(docs)
        for (docInventoryOS in docs) {
            tabDocInventoryOSDao.delete(docInventoryOS.refKey)
            for (tabDocInventoryOS in docInventoryOS.os) {
                tabDocInventoryOSDao.insert(tabDocInventoryOS)
            }
        }
    }

    fun deleteAll() {
        val docInventoryOSes = docInventoryOSDao.getAll()
        for (docInventoryOS in docInventoryOSes) {
            tabDocInventoryOSDao.delete(docInventoryOS.refKey)
        }
        docInventoryOSDao.deleteAll()
    }

    fun get(refKeyDoc: String): DocInventoryOS {
        return docInventoryOSDao.get(refKeyDoc)
    }

    fun upload(docInventoryOS: DocInventoryOS): Call<DocInventoryOS>{
        return restService.patchDocInventoryOS(docInventoryOS.refKey, docInventoryOS)
    }

    fun saveToFile(docInventoryOS: DocInventoryOS, appName: String): File? {
        return saveJSONToFile(docInventoryOS, appName, if (docInventoryOS.comment != null) docInventoryOS.comment!!.replace("[^A-Za-z0-9\\p{InCyrillic}]".toRegex(), "_") else "")
    }

    fun delete(refKeyDoc: String) {
        docInventoryOSDao.delete(refKeyDoc)
    }

    fun setIsEditDocInventoryOS(refKeyDoc: String, isEdit: Boolean) {
        docInventoryOSDao.setIsEditDocInventoryOS(refKeyDoc, isEdit)
    }

    fun loadFromFile(path: String): List<DocInventoryOS> {
        val docInventoryOSes = ArrayList<DocInventoryOS>()
        try {
            val reader: JsonReader
            val gson = GsonBuilder().create()
            reader = JsonReader(InputStreamReader(FileInputStream(path), "UTF-8"))
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "ИнвентаризацияОС" -> {
                        var docInventoryOS: DocInventoryOS?
                        try {
                            reader.beginArray()
                            while (reader.hasNext()) {
                                docInventoryOS = gson.fromJson(reader, DocInventoryOS::class.java)
                                if (docInventoryOS != null) {
                                    docInventoryOSes.add(docInventoryOS)
                                }
                            }
                            reader.endArray()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()
            reader.close()
        } catch (t: Throwable) {
            t.printStackTrace()
            Log.e("GSON", t.message)
        }
        return docInventoryOSes
    }

    fun getLastDocInventoryOSOrderById(): DocInventoryOS? {
        return docInventoryOSDao.getLastDocInventoryOSOrderById()
    }

    fun insert(docInventoryOS: DocInventoryOS) {
        docInventoryOSDao.insert(docInventoryOS)
    }
}