package ru.smartms.smallsmarty.data.source

import retrofit2.Call
import ru.smartms.smallsmarty.data.networks.RestService
import ru.smartms.smallsmarty.db.entity.OdataMetaData
import javax.inject.Inject

class OdataMetaDataRepository @Inject constructor(private val restService: RestService) {

    fun getMetaData(): Call<OdataMetaData> {
        return restService.getMetadata()
    }
}