package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import ru.smartms.smallsmarty.db.dao.CatalogOSAllTabDocInventoryOSDao
import ru.smartms.smallsmarty.db.dao.TabDocInventoryOSDao
import ru.smartms.smallsmarty.db.entity.CatalogOSAllTabDocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS


@Singleton
class TabDocInventoryOSRepository @Inject constructor(
        private val tabDocInventoryOSDao: TabDocInventoryOSDao,
        private val catalogOSAllTabDocInventoryOSDao: CatalogOSAllTabDocInventoryOSDao) {

    fun getAllLiveData(refKey: String): LiveData<List<CatalogOSAllTabDocInventoryOS>> = catalogOSAllTabDocInventoryOSDao.getAllLiveData(refKey)
    fun getCatalogOSAllTabDocInventoryOS(id: Long): LiveData<CatalogOSAllTabDocInventoryOS> = catalogOSAllTabDocInventoryOSDao.get(id)
    fun update(tabDocInventoryOS: TabDocInventoryOS?) {
        tabDocInventoryOSDao.update(tabDocInventoryOS)
    }

    fun getTabDocInventoryOS(id: Long): TabDocInventoryOS {
        return tabDocInventoryOSDao.get(id)
    }

    fun getTabDocInventoryOS(refKeyDoc: String): List<TabDocInventoryOS> {
        return tabDocInventoryOSDao.get(refKeyDoc)
    }

    fun getTabDocInventoryOSgetViaCatalogOSKeyRefKeyDoc(refKeyDoc: String, catalogOSKey: String): TabDocInventoryOS {
        return tabDocInventoryOSDao.getViaCatalogOSKeyRefKeyDoc(refKeyDoc, catalogOSKey)
    }

    fun insert(tabDocInventoryOS: TabDocInventoryOS?) {
        tabDocInventoryOSDao.insert(tabDocInventoryOS)
    }

    fun getLastTabDocInventoryOS(refKeyDoc: String?): TabDocInventoryOS? {
        return tabDocInventoryOSDao.getLastTabDocInventoryOS(refKeyDoc)
    }

    fun delete(refKeyDoc: String) {
        tabDocInventoryOSDao.delete(refKeyDoc)
    }

    fun delete(id: Long) {
        tabDocInventoryOSDao.delete(id)
    }

    fun updateIsAct(checked: Boolean, id: Long) {
        tabDocInventoryOSDao.updateIsAct(checked, id)
    }

    fun getTabDocInventoryOSLiveData(refKeyDoc: String): LiveData<List<TabDocInventoryOS>> {
        return tabDocInventoryOSDao.getAllLiveData(refKeyDoc)
    }
}