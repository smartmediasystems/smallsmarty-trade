package ru.smartms.smallsmarty.data.source

import javax.inject.Inject
import javax.inject.Singleton
import android.arch.lifecycle.LiveData
import ru.smartms.smallsmarty.db.dao.ErrorMessageDao
import ru.smartms.smallsmarty.db.entity.ErrorMessage
import ru.smartms.smallsmarty.utils.getCurrentDateTime

@Singleton
class ErrorMessageRepository @Inject constructor(private val errorMessageDao: ErrorMessageDao) {

    fun getErrorMessage(): LiveData<List<ErrorMessage>> {
        return errorMessageDao.getAll()
    }

    fun deleteAll() = errorMessageDao.deleteAll()


    fun insert(errorMessage: ErrorMessage) = errorMessageDao.insert(errorMessage)


    fun insertMessage(message: String) {
        val errorMessage = ErrorMessage(0, message, getCurrentDateTime())
        insert(errorMessage)
    }
}