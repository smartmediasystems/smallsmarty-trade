package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.rv_fragment.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.ui.adapters.BaseRecyclerViewAdapter
import ru.smartms.smallsmarty.ui.view.RecyclerViewEmptySupport
import ru.smartms.smallsmarty.StartActivity
import android.support.v4.app.ActivityCompat
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.EditText
import android.widget.ImageButton
import ru.smartms.smallsmarty.utils.REQUEST_READ_STORAGE
import ru.smartms.smallsmarty.utils.REQUEST_WRITE_STORAGE
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.zxing.integration.android.IntentIntegrator


open class BaseFragment : Fragment() {
    lateinit var recyclerView: RecyclerViewEmptySupport
    lateinit var progressBar: ProgressBar
    lateinit var drawerLayout: DrawerLayout
    lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var constraintLayout: ConstraintLayout
    lateinit var etSearch: EditText
    lateinit var iBtnSearchBarcode: ImageButton
    var fab: FloatingActionButton? = null

    fun showAlertDialog(clickListenerPositiveButton: DialogInterface.OnClickListener,
                        clickListenerNegativeButton: DialogInterface.OnClickListener? = null,
                        message: String,
                        position: Int = -1) {
        val alertDialog = AlertDialog.Builder(context!!)
        alertDialog.setTitle(getString(R.string.are_you_sure))
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(getString(android.R.string.ok), clickListenerPositiveButton)
        alertDialog.setNegativeButton(getString(android.R.string.cancel), clickListenerNegativeButton)
        if (position != -1) {
            alertDialog.setOnCancelListener { recyclerView.adapter?.notifyItemChanged(position) }
        }
        alertDialog.setCancelable(true)
        alertDialog.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        fab = (activity as StartActivity).fab
    }

    fun setupRecyclerView(adapter: BaseRecyclerViewAdapter) {
        constraintLayout = search_ll
        etSearch = et_barcode
        iBtnSearchBarcode = ibtn_enter_barcode
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.toString().contains("\n")) {
                    Log.d("Штрихкод", editable.toString())
                    var searchQuery = editable.toString()
                    searchQuery = searchQuery.replace("\n", "")
                    etSearch.setText("")
                    handleScan(searchQuery)
                }
            }
        })
        recyclerView = recycler_view
        recyclerView.setEmptyView(empty_view)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(activity?.applicationContext)
        recyclerView.layoutManager = LinearLayoutManager(activity?.applicationContext)
        recyclerView.setHasFixedSize(true)
        val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                layoutManager.orientation)
        recyclerView.addItemDecoration(dividerItemDecoration)
        val simpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                onSwipe(viewHolder, direction)
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (fab == null) {
                    fab = (activity as StartActivity).fab
                }
                if (dy > 0) {
                    fab?.hide()
                } else {
                    fab?.show()
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    fun initView() {
        activity?.let { progressBar = it.findViewById(R.id.progress_bar) }
        activity?.let { drawerLayout = it.findViewById(R.id.drawer_layout) }
        activity?.let { coordinatorLayout = it.findViewById(R.id.coordinator_layout_main) }
//        progressBar = activity!!.findViewById(R.id.progress_bar)
//        drawerLayout = activity!!.findViewById(R.id.drawer_layout)
//        coordinatorLayout = activity!!.findViewById(R.id.coordinator_layout_main) as CoordinatorLayout
    }

    open fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        //Implementation in inherited classes
    }

    fun requestReadStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Snackbar.make(coordinatorLayout, R.string.permission_read_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok) {
                        ActivityCompat.requestPermissions(activity!!,
                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                REQUEST_READ_STORAGE)
                    }
                    .show()
        } else {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_READ_STORAGE)
        }
    }

    fun requestWriteStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(coordinatorLayout, R.string.permission_write_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok) { ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_STORAGE) }
                    .show()
        } else {
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_STORAGE)
        }
    }

    open fun handleScan(query: String) {
        //Implementation in inherited classes
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Snackbar.make(coordinatorLayout, getString(android.R.string.cancel), Snackbar.LENGTH_LONG).show()
            } else {
                Snackbar.make(coordinatorLayout, "${getString(R.string.scanned)} ${result.contents}", Snackbar.LENGTH_LONG).show()
                handleScan(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    open fun initScanBarcode() {
        context?.let {
            if (ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED) {
                try {
                    val camera0 = Camera.open(0)
                    camera0.release()
                } catch (e: Exception) {
                    Log.e("initScanBarcode0", e.localizedMessage)
                }
                try {
                    val camera1 = Camera.open(1)
                    camera1.release()
                } catch (e: Exception) {
                    Log.e("initScanBarcode1", e.localizedMessage)
                }
            }
        }
        val integrator = IntentIntegrator.forSupportFragment(this)
        //integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES)
        integrator.setPrompt(getString(R.string.scan_barcode))
        integrator.setCameraId(0)  // Use a specific camera of the device
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(true)
        integrator.setOrientationLocked(false)
        integrator.initiateScan()
    }

    open fun showHideProgressBar(isLoadingProgressBar: Boolean?) {
        if (isLoadingProgressBar!!) {
            progressBar.visibility = View.VISIBLE
            activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            progressBar.visibility = View.GONE
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }
    open fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showSnackbarAndNavigateToPref(actionId: Int) {
        Snackbar.make(activity?.findViewById(android.R.id.content) as View,
                getString(R.string.data_synchronization_is_disabled_in_the_settings), Snackbar.LENGTH_LONG)
                .setAction(R.string.go_to) {
                    findNavController().navigate(actionId)
                }
                .show()
    }
}