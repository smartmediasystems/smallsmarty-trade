package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.document.TabDocMovingGoods
import ru.smartms.smallsmarty.utils.NULL_REF
import javax.inject.Inject

class TabDocMovingGoodsEditViewModel : ViewModel() {

    private var id: Long = -1L
    lateinit var tabDocMovingGoods: TabDocMovingGoods
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var refKeyDoc: String

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var tabDocMovingGoodsRepository: TabDocMovingGoodsRepository

    @Inject
    lateinit var catalogCharacteristicRepository: CatalogCharacteristicRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogNomenclatureLiveData(query: String): List<CatalogNomenclature> {
        return catalogNomenclatureRepository.getCatalogNomenclaturesLiveData(query)
    }

    fun getCatalogNomenclatures(): List<CatalogNomenclature>? {
        return catalogNomenclatureRepository.getCatalogNomenclatures()
    }

    fun getCatalogNomenclatureAllTabDocMovingGoods(id: Long): LiveData<CatalogNomenclatureAllTabDocMovingGoods> {
        return tabDocMovingGoodsRepository.getCatalogNomenclatureAllTabDocMovingGoods(id)
    }

    fun setTabDocMovingGoods(id: Long) {
        this.id = id
        GlobalScope.launch {
            tabDocMovingGoods = tabDocMovingGoodsRepository.getTabDocMovingGoods(id)
            tabDocMovingGoods.catalogNomenclature = catalogNomenclatureRepository.getCatalogNomenclature(tabDocMovingGoods.catalogNomenclatureKey!!)!!
            val catalogCharacteristicKey = tabDocMovingGoods.catalogCharacteristicKey
            if (catalogCharacteristicKey != null && catalogCharacteristicKey.isNullOrEmpty() && catalogCharacteristicKey != NULL_REF) {
                tabDocMovingGoods.catalogCharacteristic = catalogCharacteristicRepository.getCatalogCharacteristic(tabDocMovingGoods.catalogCharacteristicKey!!)!!
            }
        }
    }

    fun setTabDocMovingGoods() {
        isLoadingProgressBar.postValue(true)
        if (id >= 0) {
            tabDocMovingGoodsRepository.update(tabDocMovingGoods)
        } else {
            tabDocMovingGoodsRepository.insert(tabDocMovingGoods)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun setCatalogNomenclature(catalogNomenclatureKey: String?) {
        GlobalScope.launch {
            tabDocMovingGoods.catalogNomenclatureKey = catalogNomenclatureKey
            tabDocMovingGoods.catalogNomenclature = catalogNomenclatureRepository.getCatalogNomenclature(catalogNomenclatureKey!!)!!
        }
    }

    fun setCatalogCharacteristic(catalogCharacteristicKey: String?) {
        if (catalogCharacteristicKey == null) {
            tabDocMovingGoods.catalogCharacteristicKey = null
            tabDocMovingGoods.catalogCharacteristic = null
        } else {
            GlobalScope.launch {
                tabDocMovingGoods.catalogCharacteristicKey = catalogCharacteristicKey
                tabDocMovingGoods.catalogCharacteristic = catalogCharacteristicRepository.getCatalogCharacteristic(catalogCharacteristicKey)
            }
        }
    }

    fun setQty(qty: Double) {
        tabDocMovingGoods.qty = qty
        tabDocMovingGoods.pkgQty = qty
    }

    fun createNewTabMovingGoods() {
        tabDocMovingGoods = TabDocMovingGoods()
        tabDocMovingGoods.refKeyDoc = refKeyDoc
        tabDocMovingGoods.lineNumber = if (tabDocMovingGoodsRepository.getLastTabDocMovingGoods(refKeyDoc)?.lineNumber != null) tabDocMovingGoodsRepository.getLastTabDocMovingGoods(refKeyDoc)?.lineNumber!! + 1 else 1
    }

    fun getCatalogCharacteristicLiveData(query: String, typeCatalogNomenclatureKey: String?, catalogNomenclatureKey: String?): List<CatalogCharacteristic> {
        return catalogCharacteristicRepository.getCatalogCharacteristicsLiveData(query, typeCatalogNomenclatureKey, catalogNomenclatureKey)
    }
}
