package ru.smartms.smallsmarty.ui

interface IHideProgress {
    fun hideProgressBar()
}