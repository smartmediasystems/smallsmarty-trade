package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import loadOSFromREST
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStoreOData
import ru.smartms.smallsmarty.db.entity.catalog.*
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclatures
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import ru.smartms.smallsmarty.utils.generateBarcodeForSeries
import javax.inject.Inject

class PrefsMainViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    val showCheckConnectionOk: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var docRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var docMovingGoodsRepository: DocMovingGoodsRepository

    @Inject
    lateinit var odataMetaDataRepository: OdataMetaDataRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var catalogCharacteristicRepository: CatalogCharacteristicRepository

    @Inject
    lateinit var catalogSeriesRepository: CatalogSeriesRepository

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    @Inject
    lateinit var aRGoodsInStoreRepository: ARGoodsInStoreRepository

    fun loadAllCatalogAndInfRegFromREST() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        loadOSFromREST(catalogOperationObjectRepository, isLoadingProgressBar, errorMessageRepository)
        catalogNomenclatureRepository.deleteAll()
        loadCatalogNomenclatureFromRest()
        catalogCharacteristicRepository.deleteAll()
        loadCatalogCharacteristicsFromRest()
        infRegBarcodeRepository.deleteAll()
        loadInfRegBarcodeFromRest()
        catalogSeriesRepository.deleteAll()
        loadCatalogSeriesFromRest()
        aRGoodsInStoreRepository.deleteAll()
        loadARGoodsInStoreFromRest()
        isLoadingProgressBar.postValue(false)

    }

    private suspend fun loadARGoodsInStoreFromRest() {
        var arGoodsInStoreOData: ARGoodsInStoreOData
        var i = 0
        try {
            do {
                i++
                arGoodsInStoreOData = aRGoodsInStoreRepository.loadFromRestBalanceAndTurnovers(i)
                aRGoodsInStoreRepository.insertAll(arGoodsInStoreOData.value)
            } while (arGoodsInStoreOData.value.isNotEmpty())
        } catch (e: Exception) {
            setErrorMessage(e, ARGoodsInStoreRepository.TAG)
        }
    }

    private suspend fun loadCatalogSeriesFromRest() {
        var catalogSeriesOData: CatalogSeriesOData
        var i = 0
        try {
            do {
                i++
                catalogSeriesOData = catalogSeriesRepository.loadFromRest(
                        top = BuildConfig.QTY_REQ * i,
                        skip = BuildConfig.QTY_REQ * (i - 1),
                        orderBy = "Description"
                ).await()
                catalogSeriesOData.value.forEach {
                    generateBarcodeForSeries(it)
                }
                catalogSeriesRepository.insertAll(catalogSeriesOData.value)
            } while (catalogSeriesOData.value.isNotEmpty())
        } catch (e: Exception) {
            setErrorMessage(e, "SeriesRest")
        }
    }

    private suspend fun loadInfRegBarcodeFromRest() {
        var infRegBarcodeNomenclatures: InfRegBarcodeNomenclatures
        var i = 0
        try {
            do {
                i++
                infRegBarcodeNomenclatures = infRegBarcodeRepository.getAllFromREST(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        null
                ).await()
                infRegBarcodeRepository.insertAll(infRegBarcodeNomenclatures.value)
            } while (infRegBarcodeNomenclatures.value.isNotEmpty())
        } catch (e: Exception) {
            setErrorMessage(e, "loadInfRegBarcodeFromRest")
        }
    }

    private suspend fun loadCatalogCharacteristicsFromRest() {
        var catalogCharacteristics: CatalogCharacteristics
        var i = 0
        try {
            do {
                i++
                catalogCharacteristics = catalogCharacteristicRepository.loadFromRest(
                        top = BuildConfig.QTY_REQ * i,
                        skip = BuildConfig.QTY_REQ * (i - 1),
                        orderBy = "Description"
                ).await()
                catalogCharacteristicRepository.insertAll(catalogCharacteristics.value)
            } while (catalogCharacteristics.value.isNotEmpty())
        } catch (e: Exception) {
            setErrorMessage(e, "loadCatalogCharacteristicsFromRest")
        }
    }

    private suspend fun loadCatalogNomenclatureFromRest() {
        var catalogNomenclatures: CatalogNomenclatures
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                catalogNomenclatures = catalogNomenclatureRepository.loadFromRest(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogNomenclatureRepository.insertAll(catalogNomenclatures.value)
            } while (catalogNomenclatures.value.isNotEmpty())
        } catch (e: Exception) {
            setErrorMessage(e, "loadCatalogNomenclatureFromRest")
        }
    }

    private fun setErrorMessage(e: Exception, tag: String) {
        Log.e(tag, e.localizedMessage)
        isLoadingProgressBar.postValue(false)
        errorMessageRepository.insertMessage(e.localizedMessage)
    }

    fun deleteAllData() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            catalogNomenclatureRepository.deleteAll()
            catalogCharacteristicRepository.deleteAll()
            catalogSeriesRepository.deleteAll()
            catalogOperationObjectRepository.deleteAll()
            infRegBarcodeRepository.deleteAll()
            docRecountGoodsRepository.deleteAll()
            docInventoryOSRepository.deleteAll()
            docMovingGoodsRepository.deleteAll()
            isLoadingProgressBar.postValue(false)
        }
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            docRecountGoodsRepository.insertAll(docRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            catalogCharacteristicRepository.insertAll(catalogCharacteristicRepository.loadFromFile(path))
            catalogSeriesRepository.insertAll(catalogSeriesRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOperationObjectRepository.insertAll(catalogOperationObjectRepository.loadFromFile(path))
            docInventoryOSRepository.insertAll(docInventoryOSRepository.loadFromFile(path) as MutableList<DocInventoryOS>)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun checkConnection() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            try {
                val odataMetaData = odataMetaDataRepository.getMetaData().await()
                if (odataMetaData.odataError != null) {
                    errorMessage.postValue(odataMetaData.odataError?.message?.value)
                } else {
                    showCheckConnectionOk.postValue(true)
                }
            } catch (e: Throwable) {
                isLoadingProgressBar.postValue(false)
                errorMessage.postValue(e.localizedMessage)
                errorMessageRepository.insertMessage(e.localizedMessage)
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun isAllowDataSync(): Boolean  = sharedPreferencesRepository.isAllowDataSync()
}
