package ru.smartms.smallsmarty.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.license_info.*
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo
import ru.smartms.smallsmarty.ui.viewmodel.LicenseInfoViewModel
import ru.smartms.smallsmarty.utils.isValidEmail


class LicenseInfoFragment : BaseFragment(), View.OnClickListener {

    private lateinit var viewModel: LicenseInfoViewModel
    private lateinit var etLicenseKey: EditText
    private lateinit var etEmail: EditText
    private lateinit var tvLicenseInfo: TextView
    private lateinit var btnCheck: Button
    private lateinit var btnActivate: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LicenseInfoViewModel::class.java)
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
        viewModel.getActivatedLicense().observe(this, Observer {licenseInfo ->
            setLicenseInfo(licenseInfo)
        })
        viewModel.licenseInfoLiveData.observe(this, Observer {licenseInfo ->
            setLicenseInfo(licenseInfo)
        })
    }

    private fun setLicenseInfo(licenseInfo: LicenseInfo?) {
        if (licenseInfo != null) {
            if (licenseInfo.activated) {
                val message = "${getString(R.string.license_is_active)}, ${licenseInfo.message}"
                tvLicenseInfo.text = message
                tvLicenseInfo.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_green_dark))
            }
            if (!licenseInfo.activated && licenseInfo.message.isNullOrEmpty()) {
                val message = "${getString(R.string.license_is_not_active)}, ${licenseInfo.message}"
                tvLicenseInfo.text = message
                tvLicenseInfo.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_red_dark))
            }
            if (!licenseInfo.activated && !licenseInfo.error.isNullOrEmpty()) {
                val message = "${getString(R.string.license_is_not_active)}, ${licenseInfo.error}"
                tvLicenseInfo.text = message
                tvLicenseInfo.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_red_dark))
            }
            if (licenseInfo.success) {
                val countActivation = licenseInfo.licenseInfoActivations.size + licenseInfo.remaining
                val message = "${getString(R.string.license_key_was_previously_activated)} ${licenseInfo.licenseInfoActivations.size} ${getString(R.string.of)} $countActivation"
                tvLicenseInfo.text = message
                tvLicenseInfo.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_red_dark))
            }
        } else {
            tvLicenseInfo.text = getString(R.string.license_is_not_active)
            tvLicenseInfo.setTextColor(ContextCompat.getColor(context!!, android.R.color.holo_red_dark))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.license_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        etLicenseKey = et_license_key
        etEmail = et_email_license_info
        tvLicenseInfo = tv_license_info
        btnCheck = btn_check_license_key
        btnActivate = btn_activate
        btnActivate.setOnClickListener(this)
        btnCheck.setOnClickListener(this)
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onClick(v: View?) {
       when (v?.id) {
           android.R.id.home -> findNavController().popBackStack()
           R.id.btn_activate -> {
               if (!isValidEmail(etEmail.text)) {
                   etEmail.error = getString(R.string.incorrect_email)
               }
               if (etLicenseKey.text.isNullOrEmpty()) {
                   etLicenseKey.error = getString(R.string.enter_license_key)
               }
               if (isValidEmail(etEmail.text) && !etLicenseKey.text.isNullOrEmpty()) {
                   val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
                       viewModel.activateLicense(etLicenseKey.text.toString(), etEmail.text.toString())
                   }
                   val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> }
                   val alertDialog = AlertDialog.Builder(context!!)
                   alertDialog.setTitle(getString(R.string.are_you_sure))
                   alertDialog.setMessage(R.string.license_will_be_activated)
                   alertDialog.setPositiveButton(getString(android.R.string.ok), clickListenerPositiveButton)
                   alertDialog.setNegativeButton(getString(android.R.string.cancel), clickListenerNegativeButton)
                   alertDialog.setCancelable(true)
                   alertDialog.show()
               }
           }
           R.id.btn_check_license_key -> {
               if (!isValidEmail(etEmail.text)) {
                   etEmail.error = getString(R.string.incorrect_email)
               }
               if (etLicenseKey.text.isNullOrEmpty()) {
                   etLicenseKey.error = getString(R.string.enter_license_key)
               }
               if (isValidEmail(etEmail.text) && !etLicenseKey.text.isNullOrEmpty()) {
                   viewModel.checkLicense(etLicenseKey.text.toString(), etEmail.text.toString())
               }
           }
       }
    }
}
