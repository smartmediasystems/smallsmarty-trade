package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.media.MediaScannerConnection
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogOSAllTabDocInventoryOS
import ru.smartms.smallsmarty.ui.adapters.TabDocInventoryOSRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.TabDocInventoryOSViewModel
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.content.Intent
import ru.smartms.smallsmarty.utils.REQUEST_WRITE_STORAGE
import android.content.DialogInterface
import android.support.v7.widget.SearchView
import android.widget.CheckBox
import android.support.v7.app.AlertDialog
import android.widget.LinearLayout
import android.widget.EditText
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS


class TabDocInventoryOSFragment : BaseFragment(), View.OnClickListener, SearchView.OnQueryTextListener {

    var data: ArrayList<CatalogOSAllTabDocInventoryOS> = ArrayList()

    private lateinit var viewModel: TabDocInventoryOSViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TabDocInventoryOSViewModel::class.java)
        val refKeyDoc: String = arguments?.getString("refKeyDoc", "")!!
        viewModel.refKeyDoc = refKeyDoc
        viewModel.getCatalogOSAllTabDocInventoryOS(refKeyDoc)?.observe(this, Observer { catalogOSAllTabDocInventoryOSes ->
            data.clear()
            data.addAll(catalogOSAllTabDocInventoryOSes!!)
            viewModel.setIsEditDocInventoryOS(true)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.handleBarcode(scanData[0].barcode)
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.paths.observe(this, Observer { paths ->
            MediaScannerConnection.scanFile(context, paths, null) { path, _ ->
                Log.d("SCAN File", path)
                Toast.makeText(context, "${getString(R.string.document_saved)} $path", Toast.LENGTH_LONG).show()
            }
            Toast.makeText(context, "${getString(R.string.document_saved)} ${paths?.get(0)}", Toast.LENGTH_LONG).show()
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(TabDocInventoryOSRVAdapter(data, this))
        fab?.setOnClickListener(this)
        iBtnSearchBarcode.setOnClickListener(this)
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_tab_doc_inventory_os_fragment, menu)
        val menuItemSearch = menu?.findItem(R.id.action_search)
        val searchView = menuItemSearch?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigateUp()
            R.id.upload_doc_inventory_os -> viewModel.uploadDocInventoryOS()
            R.id.delete_doc_inventory_os -> viewModel.deleteDocInventoryOS()
            R.id.action_save_to_file -> {
                if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestWriteStoragePermission()
                } else {
                    GlobalScope.launch { viewModel.saveToFile(getString(R.string.app_name)) }
                }
            }
            R.id.action_enter_barcode -> {
                item.isChecked = !item.isChecked
                if (constraintLayout.visibility == View.GONE) constraintLayout.visibility = View.VISIBLE else constraintLayout.visibility = View.GONE
            }
            R.id.action_scan_barcode_from_camera -> initScanBarcode()
            R.id.action_add_comment -> {
                GlobalScope.launch(Dispatchers.Main) {
                    val input = EditText(context)
                    var docInventoryOS: DocInventoryOS? = null
                    withContext(Dispatchers.Default) {
                        docInventoryOS = viewModel.getDocInventoryOS()
                    }
                    input.setText(docInventoryOS?.comment)
                    val lp = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT)
                    val alertDialog = AlertDialog.Builder(context!!)
                            .setTitle(R.string.comment)
                            .setPositiveButton(android.R.string.ok) { _, _ ->
                                docInventoryOS?.comment = input.text.toString()
                                launch {
                                    docInventoryOS?.let { viewModel.insert(it) }
                                }
                            }
                            .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    input.layoutParams = lp
                    alertDialog.setView(input)
                    alertDialog.show()
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_catalog_os -> {
                val bundle = Bundle()
                bundle.putLong("id", data[recyclerView.getChildAdapterPosition(v.parent as View)].id)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocInventoryOSFragment_to_tabDocInventoryOSEditFragment, bundle)
            }
            R.id.cb_is_act -> viewModel.setAct((v as CheckBox).isChecked, data[recyclerView.getChildAdapterPosition(v.parent as View)].id)
            R.id.fab -> {
                val bundle = Bundle()
                bundle.putLong("id", -1L)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocInventoryOSFragment_to_tabDocInventoryOSEditFragment, bundle)
            }
            R.id.ibtn_enter_barcode -> {
                val query = etSearch.text.toString()
                etSearch.setText("")
                if (query.isNotEmpty()) {
                    viewModel.handleBarcode(query)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_WRITE_STORAGE) {
            viewModel.saveToFile(getString(R.string.app_name))
        }
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteTabDocInventoryOS(data[position].id)
            Toast.makeText(context, getString(R.string.document_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.doc_will_be_remove), position)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        if (!query.isEmpty()) {
            viewModel.handleBarcode(query)
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null && newText.contains("\n")) {
            viewModel.handleBarcode(newText.replace("\n", ""))
        }
        return true
    }

    override fun handleScan(query: String) {
        viewModel.handleBarcode(query)
    }
}
