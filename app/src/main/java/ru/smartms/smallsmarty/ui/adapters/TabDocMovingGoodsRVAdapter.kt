package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.tab_doc_moving_goods_item.view.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods


class TabDocMovingGoodsRVAdapter(private val dataModels: ArrayList<CatalogNomenclatureAllTabDocMovingGoods>, private val listener: View.OnClickListener) : BaseRecyclerViewAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tab_doc_moving_goods_item, parent, false)
        view.setOnClickListener(listener)
        return TabDocMovingGoodsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as TabDocMovingGoodsViewHolder
        viewHolder.tvCatalogNomenclature.text = if (!dataModels[position].catalogNomenclatureDescription.isNullOrEmpty()) dataModels[position].catalogNomenclatureDescription else viewHolder.tvCatalogNomenclature.context.getText(R.string.no_description)
        if (!dataModels[position].catalogCharacteristicDescription.isNullOrEmpty()) {
            viewHolder.tvCatalogCharacteristic.text = dataModels[position].catalogCharacteristicDescription
            viewHolder.tvCatalogCharacteristic.visibility = View.VISIBLE
        } else {
            viewHolder.tvCatalogCharacteristic.visibility = View.GONE
        }
        viewHolder.tvQty.text = dataModels[position].qty.toString()
    }

    override fun getItemCount(): Int {
        return dataModels.size
    }

    inner class TabDocMovingGoodsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCatalogNomenclature: TextView = itemView.tv_moving_goods_catalog_nomenclature
        var tvCatalogCharacteristic: TextView = itemView.tv_moving_goods_catalog_characteristic
        var tvQty: TextView = itemView.tv_moving_goods_qty
    }
}
