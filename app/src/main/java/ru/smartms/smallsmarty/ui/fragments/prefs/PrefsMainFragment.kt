package ru.smartms.smallsmarty.ui.fragments.prefs

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import ru.smartms.smallsmarty.R
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothAdapter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Environment
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.preference.*
import android.view.*
import android.widget.ProgressBar
import android.widget.Toast
import com.obsez.android.lib.filechooser.ChooserDialog

import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.StartActivity
import ru.smartms.smallsmarty.utils.REQUEST_READ_STORAGE
import ru.smartms.smallsmarty.ui.viewmodel.PrefsMainViewModel
import ru.smartms.smallsmarty.utils.HostSelectionInterceptor
import javax.inject.Inject


class PrefsMainFragment : PreferenceFragmentCompat(), PreferenceFragmentCompat.OnPreferenceStartScreenCallback, SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {
    private lateinit var drawerLayout: DrawerLayout

    private lateinit var fab: FloatingActionButton
    private lateinit var viewModel: PrefsMainViewModel
    private lateinit var coordinatorLayout: CoordinatorLayout
    lateinit var progressBar: ProgressBar

    @Inject
    lateinit var hostSelectionInterceptor: HostSelectionInterceptor

    private val bindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
        if (preference is EditTextPreference) {
            // For all other preferences, set the summary to the value's
            // simple string representation.
            if (preference.key != "password") {
                preference.summary = value.toString()
            }
        }
        true
    }

    override fun onSharedPreferenceChanged(preferences: SharedPreferences?, key: String?) {
        when (key) {
            "password" -> hostSelectionInterceptor.password = preferences?.getString(key, "")
            "login" -> hostSelectionInterceptor.userName = preferences?.getString(key, "")
            "url" -> hostSelectionInterceptor.url = preferences?.getString(key, "127.0.0.1")
            "dbName" -> hostSelectionInterceptor.dbName = preferences?.getString(key, "")
        }
    }

    override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
        when (preference?.key) {
            "password" -> hostSelectionInterceptor.password = newValue as String
            "login" -> hostSelectionInterceptor.userName = newValue as String
            "url" -> hostSelectionInterceptor.url = newValue as String
            "dbName" -> hostSelectionInterceptor.dbName = newValue as String
        }
        if (preference?.key != "password") {
            preference?.summary = newValue.toString()
        }
        return true
    }

    private fun bindPreferenceSummaryToValue(preference: Preference) {
        // Set the listener to watch for value changes.
        preference.onPreferenceChangeListener = bindPreferenceSummaryToValueListener
        // Trigger the listener immediately with the preference's
        // current value.
        if (preference.key == "enabled_odata" || preference.key == "scan_one_by_one_cbx") {
            bindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getBoolean(preference.key, false))
        } else {
            bindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }
        preference.onPreferenceChangeListener = this
    }

    override fun onPreferenceStartScreen(caller: PreferenceFragmentCompat?, pref: PreferenceScreen?): Boolean {
        caller?.preferenceScreen = preferenceScreen
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { viewModel = ViewModelProviders.of(it).get(PrefsMainViewModel::class.java) }
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null && errorMessage.isNotEmpty()) {
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.showCheckConnectionOk.observe(this, Observer { showCheckConnectionOk ->
            if (showCheckConnectionOk == true) {
                Toast.makeText(context, R.string.connection_successful, Toast.LENGTH_LONG).show()
                viewModel.showCheckConnectionOk.postValue(false)
            }
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar == true) {
                progressBar.visibility = View.VISIBLE
                activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            } else {
                progressBar.visibility = View.GONE
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        App.component.inject(this)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun getCallbackFragment(): Fragment {
        return this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            drawerLayout = it.findViewById(R.id.drawer_layout)
            coordinatorLayout = it.findViewById(R.id.coordinator_layout_main) as CoordinatorLayout
            progressBar = it.findViewById(R.id.progress_bar)
            fab = it.fab
            fab.setOnClickListener(null)
            fab.hide()
        }
        setHasOptionsMenu(true)
    }


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.pref)
        val listPreferenceBluetoothDevice: ListPreference = findPreference("bluetooth_device_list") as ListPreference
        setBluetoothDevice(listPreferenceBluetoothDevice)
        listPreferenceBluetoothDevice.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _, newValue ->
            val sharedPrefs: SharedPreferences.Editor? = context?.getSharedPreferences("SelectBluetoothDevice", 0)?.edit()
            sharedPrefs?.putString("SelectBluetoothDeviceBluetoothDevice", newValue as String?)
            sharedPrefs?.apply()
            true
        }
        listPreferenceBluetoothDevice.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            setBluetoothDevice(listPreferenceBluetoothDevice)
            true
        }
        val pref = findPreference("check_connection")
        pref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            viewModel.checkConnection()
            false
        }
        bindPreferenceSummaryToValue(findPreference("login"))
        bindPreferenceSummaryToValue(findPreference("password"))
        bindPreferenceSummaryToValue(findPreference("url"))
        bindPreferenceSummaryToValue(findPreference("dbName"))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_prefs_main_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.action_load_all_data -> {
                if (viewModel.isAllowDataSync()) {
                    GlobalScope.launch(Dispatchers.Default) {
                        viewModel.loadAllCatalogAndInfRegFromREST()
                    }
                } else {
                    Snackbar.make(activity?.findViewById(android.R.id.content) as View,
                            getString(R.string.data_synchronization_is_disabled_in_the_settings), Snackbar.LENGTH_LONG).show();
                }
            }
            R.id.action_delete_all_data -> viewModel.deleteAllData()
            R.id.action_load_from_file_all_data -> context?.let { itContext ->
                if (ActivityCompat.checkSelfPermission(itContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    activity?.let { itActivity ->
                        if (ActivityCompat.shouldShowRequestPermissionRationale(itActivity,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Snackbar.make(coordinatorLayout, R.string.permission_read_storage_rationale,
                                    Snackbar.LENGTH_INDEFINITE)
                                    .setAction(android.R.string.ok) {
                                        ActivityCompat.requestPermissions(itActivity,
                                                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                                                REQUEST_READ_STORAGE)
                                    }
                                    .show()
                        } else {
                            ActivityCompat.requestPermissions(itActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_READ_STORAGE)
                        }
                    }
                } else {
                    ChooserDialog(context)
                            .withFilter(false, false, "json", "txt")
                            .withStartFile("${Environment.getExternalStorageDirectory()}")
                            .withChosenListener { path, _ -> viewModel.loadFromFile(path) }
                            .build()
                            .show()
                }
            }
        }
        return true
    }

    private fun setBluetoothDevice(listPreferenceBluetoothDevice: ListPreference) {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        var bluetoothDevice: BluetoothDevice
        if (bluetoothAdapter != null) {
            val bondedDevices = bluetoothAdapter.bondedDevices
            if (bondedDevices.size > 0) {
                val listPreferenceValue = arrayOfNulls<CharSequence>(bondedDevices.size)
                val listPreferenceEntities = arrayOfNulls<CharSequence>(bondedDevices.size)
                val iterator = bondedDevices.iterator()
                var i = 0
                while (iterator.hasNext()) {
                    bluetoothDevice = iterator.next()
                    listPreferenceValue[i] = bluetoothDevice.address
                    listPreferenceEntities[i] = bluetoothDevice.name
                    i++
                }
                listPreferenceBluetoothDevice.entryValues = listPreferenceValue
                listPreferenceBluetoothDevice.entries = listPreferenceEntities
            } else {
                listPreferenceBluetoothDevice.entryValues = arrayOf<CharSequence>(getString(R.string.no_bluetooth_device))
                listPreferenceBluetoothDevice.entries = arrayOf<CharSequence>(getString(R.string.no_bluetooth_device))
            }
        } else {
            listPreferenceBluetoothDevice.entryValues = arrayOf<CharSequence>(getString(R.string.no_bluetooth_adapter))
            listPreferenceBluetoothDevice.entries = arrayOf<CharSequence>(getString(R.string.no_bluetooth_adapter))
        }
    }
}