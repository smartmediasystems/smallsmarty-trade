package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import com.obsez.android.lib.filechooser.ChooserDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS
import ru.smartms.smallsmarty.ui.adapters.CatalogOSesRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.CatalogOSViewModel


class CatalogOSFragment : BaseFragment() {

    var data: ArrayList<CatalogOS> = ArrayList()

    private lateinit var viewModel: CatalogOSViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(CatalogOSViewModel::class.java)
        viewModel.getCatalogOSes()?.observe(this, Observer { catalogOSes ->
            catalogOSes?.let {
                data.clear()
                data.addAll(it)
                recyclerView.adapter?.notifyDataSetChanged()
            }
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(CatalogOSesRVAdapter(data))
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_catalog_os_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.load_catalog_os -> viewModel.loadCatalogOSesFromREST()
            R.id.delete_catalog_os -> viewModel.deleteAllCatalogOSes()
            R.id.load_from_file_catalog_os -> {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestReadStoragePermission()
                } else {
                    ChooserDialog(context)
                            .withFilter(false, false, "json", "txt")
                            .withStartFile("${Environment.getExternalStorageDirectory()}")
                            .withChosenListener { path, _ -> viewModel.loadFromFile(path) }
                            .build()
                            .show()
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.Default) {
                    viewModel.deleteCatalogOS(data[position].refKey)
                }
                Toast.makeText(context, getString(R.string.item_was_deleted_from_device),
                        Toast.LENGTH_LONG).show()
            }
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.item_will_be_remove), position)
    }
}
