package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView

abstract class BaseRecyclerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()