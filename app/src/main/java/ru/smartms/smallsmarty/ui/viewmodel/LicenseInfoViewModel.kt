package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.HostSelectionInterceptor
import ru.smartms.smallsmarty.utils.URL_LICENSE_SYSTEM
import java.lang.Exception
import javax.inject.Inject

class LicenseInfoViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    val licenseInfoLiveData: MutableLiveData<LicenseInfo> = MutableLiveData()

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository
    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var hostSelectionInterceptor: HostSelectionInterceptor

    fun getActivatedLicense(): LiveData<LicenseInfo> {
        return licenseInfoRepository.getActivatedLiveData()
    }

    fun activateLicense(licenseKey: String, email: String) {
        GlobalScope.launch {
            try {
                isLoadingProgressBar.postValue(true)
                setParamHostSelectionInterceptor()
                val licenseInfo = licenseInfoRepository.activate(licenseKey, email).await()
                if (licenseInfo.activated) {
                    licenseInfoRepository.insert(licenseInfo)
                }
                licenseInfoLiveData.postValue(licenseInfo)
                isLoadingProgressBar.postValue(false)
            } catch (e: Exception) {
                isLoadingProgressBar.postValue(false)
                errorMessageRepository.insertMessage(e.localizedMessage)
            }
        }
    }

    fun checkLicense(licenseKey: String, email: String) {
        GlobalScope.launch {
            try {
                isLoadingProgressBar.postValue(true)
                setParamHostSelectionInterceptor()
                val licenseInfo = licenseInfoRepository.check(licenseKey, email).await()
                licenseInfoLiveData.postValue(licenseInfo)
                isLoadingProgressBar.postValue(false)
            } catch (e: Exception) {
                isLoadingProgressBar.postValue(false)
                errorMessageRepository.insertMessage(e.localizedMessage)
            }
        }
    }

    private fun setParamHostSelectionInterceptor() {
        hostSelectionInterceptor.url = URL_LICENSE_SYSTEM
        hostSelectionInterceptor.dbName = ""
        hostSelectionInterceptor.userName = ""
        hostSelectionInterceptor.password = ""
    }
}
