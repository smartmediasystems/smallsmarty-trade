package ru.smartms.smallsmarty.ui.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.tab_doc_inventory_os_item_edit.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS
import ru.smartms.smallsmarty.ui.viewmodel.TabDocInventoryOSEditViewModel

class TabDocInventoryOSEditFragment : BaseFragment(), View.OnClickListener {

    private var job: Job = Job()
    private lateinit var viewModel: TabDocInventoryOSEditViewModel
    private lateinit var catalogOSAutoCompleteTextView: AutoCompleteTextView
    private lateinit var accCheckbox: CheckBox
    private lateinit var actCheckbox: CheckBox
    private lateinit var ivBtnClear: ImageView
    private lateinit var btnOk: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TabDocInventoryOSEditViewModel::class.java)
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
        val refKeyDocTemp = arguments?.getString("refKeyDoc", "")
        if (refKeyDocTemp != null) {
            viewModel.refKeyDoc = refKeyDocTemp
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        val view = inflater.inflate(R.layout.tab_doc_inventory_os_item_edit, container, false)
        catalogOSAutoCompleteTextView = view.autocomplete_tv_catalog_os
        accCheckbox = view.cb_acc
        actCheckbox = view.cb_act
        btnOk = view.btn_ok
        ivBtnClear = view.iv_clear_btn_catalog_nomenclature
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        catalogOSAutoCompleteTextView.setAdapter(CatalogOSAutoCompleteAdapter(ArrayList()))
        val id = arguments?.getLong("id", -1L)
        if (id!! >= 0) {
            viewModel.getCatalogOSAllTabDocInventoryOS(id).observe(this, Observer { catalogOSAllTabDocInventoryOS ->
                if (catalogOSAllTabDocInventoryOS != null) {
                    catalogOSAutoCompleteTextView.setText(catalogOSAllTabDocInventoryOS.catalogOSDescription)
                    accCheckbox.isChecked = catalogOSAllTabDocInventoryOS.isAvailabilityAcc
                    actCheckbox.isChecked = catalogOSAllTabDocInventoryOS.isAvailabilityAct
                }
            })
        } else {
            GlobalScope.launch {
                viewModel.createNewTabInventoryOS()
            }
        }
        catalogOSAutoCompleteTextView.setOnTouchListener { _, _ ->
            catalogOSAutoCompleteTextView.showDropDown()
            false
        }
        catalogOSAutoCompleteTextView.setOnItemClickListener { adapterView, _, _, idActv ->
            val catalogOS = adapterView.adapter.getItem(idActv.toInt()) as CatalogOS
            viewModel.setCatalogOS(catalogOS.refKey)
        }
        actCheckbox.requestFocus()
        btnOk.setOnClickListener(this)
        ivBtnClear.setOnClickListener(this)
        fab?.hide()
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_doc_inventory_os_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_ok -> {
                if (catalogOSAutoCompleteTextView.text.isEmpty()) {
                    Toast.makeText(context, R.string.select_os, Toast.LENGTH_LONG).show()
                    return
                }
                GlobalScope.launch(Dispatchers.Main) {
                    withContext(Dispatchers.Default) {
                        viewModel.setAct(actCheckbox.isChecked)
                        viewModel.setTabDocInventoryOS()
                    }
                    findNavController().popBackStack()
                }
            }
            R.id.iv_clear_btn_catalog_nomenclature -> catalogOSAutoCompleteTextView.setText("")
        }
    }

    inner class CatalogOSAutoCompleteAdapter(val result: ArrayList<CatalogOS>) : BaseAdapter(), Filterable {

        override fun getCount(): Int {
            return result.size
        }

        override fun getItem(index: Int): CatalogOS {
            return result[index]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, cV: View?, parent: ViewGroup): View {
            var convertView = cV
            if (convertView == null) {
                val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.custom_catalog_os_autocomplete_adapter_item, parent, false)
            }
            (convertView!!.findViewById(R.id.tv_item) as TextView).text = getItem(position).description
            return convertView
        }

        override fun getFilter(): Filter? {
            return object : Filter() {
                override fun convertResultToString(resultValue: Any): CharSequence? {
                    return (resultValue as CatalogOS).description
                }

                override fun performFiltering(constraint: CharSequence): FilterResults? {
                    return null
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    if (constraint != null) {
                        job.cancel()
                        job = Job()
                        GlobalScope.launch(Dispatchers.Main + job) {
                            progressBar.visibility = View.VISIBLE
                            withContext(Dispatchers.Default) {
                                result.clear()
                                result.addAll(if (!constraint.isEmpty()) viewModel.getCatalogOSLiveData("%$constraint%") else viewModel.getCatalogOSes()!!)
                            }
                            notifyDataSetChanged()
                            progressBar.visibility = View.GONE
                        }
                    } else {
                        notifyDataSetInvalidated()
                    }
                }
            }
        }
    }
}
