package ru.smartms.smallsmarty.ui.fragments

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.android.billingclient.api.Purchase
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.billing_layout.*
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.billingmodule.NUMBER_OF_ACTIVATIONS
import ru.smartms.smallsmarty.db.entity.PurchaseInnApp
import ru.smartms.smallsmarty.ui.viewmodel.PurchasesViewModel
import ru.smartms.smallsmarty.utils.isValidEmail
import javax.inject.Inject


class PurchasesFragment : BaseFragment(), View.OnClickListener {

    var data: ArrayList<Purchase> = ArrayList()
    private var job: Job = Job()

    @Inject
    lateinit var auth: FirebaseAuth

    private lateinit var viewModel: PurchasesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PurchasesViewModel::class.java)
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.billing_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        btn_check_license.setOnClickListener(this)
        btn_activate.setOnClickListener(this)
        btn_buy_activate_license.setOnClickListener(this)
        btn_consume_license.setOnClickListener(this)
        btn_sign_in.setOnClickListener(this)
        btn_sign_up.setOnClickListener(this)
        btn_sign_out.setOnClickListener(this)
        btn_send_email_verification.setOnClickListener(this)
        tv_forgot_pass.setOnClickListener(this)
        viewModel.message.observe(this, Observer { message ->
            message?.let {
                showToast(message)
            }
        })
        viewModel.isDisableRegistration.observe(this, Observer { isDisableRegistration ->
            if (isDisableRegistration == true) {
                btn_sign_in.visibility = View.GONE
                btn_sign_up.visibility = View.GONE
                btn_sign_out.visibility = View.VISIBLE
                tv_forgot_pass.visibility = View.GONE
            } else {
                btn_sign_in.visibility = View.VISIBLE
                btn_sign_up.visibility = View.VISIBLE
                btn_sign_out.visibility = View.GONE
                tv_forgot_pass.visibility = View.VISIBLE
            }
            tiet_email.isEnabled = isDisableRegistration == false
            tiet_password.isEnabled = isDisableRegistration == false
        })
        viewModel.isEmailVerifySend.observe(this, Observer { isEmailVerifySend ->
            if (isEmailVerifySend == true) {
                btn_send_email_verification.visibility = View.VISIBLE
                updateUIEmailVerify()
            }
        })
        viewModel.isEmailNotVerified.observe(this, Observer { isEmailNotVerified ->
            if (isEmailNotVerified == true) {
                btn_send_email_verification.visibility = View.VISIBLE
                tv_email_not_verified.visibility = View.VISIBLE
                viewModel.checkEmailVerify()
                updateUIEmailVerify()
            } else {
                btn_send_email_verification.visibility = View.GONE
                tv_email_not_verified.visibility = View.GONE
                job.cancel()
                setViewDefaultText()
            }
        })
        viewModel.purchasesInnAppLiveData.observe(this, Observer { purchasesInnAppFB ->
            if (purchasesInnAppFB != null && viewModel.purchaseInnApp != null) {
                tv_count_activate_license.text = (NUMBER_OF_ACTIVATIONS - purchasesInnAppFB.size).toString()
                context?.let { tv_count_activate_license.setTextColor(ContextCompat.getColor(it, android.R.color.holo_green_dark)) }
            } else {
                tv_count_activate_license.setText(R.string.license_unavailable)
                context?.let { tv_count_activate_license.setTextColor(ContextCompat.getColor(it, android.R.color.holo_red_dark)) }
            }
        })
        viewModel.isLicenseActiveLiveData().observe(this, Observer { isLicenseActiveLiveData ->
            btn_buy_activate_license.isEnabled = isLicenseActiveLiveData == false
            btn_activate.isEnabled = isLicenseActiveLiveData == false
            setIsLicActiveUI(isLicenseActiveLiveData, viewModel.purchaseInnApp)
        })
        viewModel.isForgotPassSend.observe(this, Observer { isForgotPassSend ->
            tv_forgot_pass.isEnabled = isForgotPassSend == true
            if (isForgotPassSend == true) {
                tv_forgot_pass.isEnabled = true
                Toast.makeText(context, R.string.password_reset_link_sent, Toast.LENGTH_SHORT).show()
            }
        })
        tiet_email.setText(viewModel.getEmail())
        tiet_password.setText(viewModel.getPassword())
        viewModel.signIn(tiet_email.text.toString(), tiet_password.text.toString())
    }

    private fun setIsLicActiveUI(isLicenseActive: Boolean?, purchaseInnApp: PurchaseInnApp?) {
        if (isLicenseActive == true) {
            context?.let { tv_is_license_active.setTextColor(ContextCompat.getColor(it, android.R.color.holo_green_dark)) }
            tv_is_license_active.setText(R.string.license_is_active)
        } else if (isLicenseActive == false && purchaseInnApp != null) {
            context?.let { tv_is_license_active.setTextColor(ContextCompat.getColor(it, android.R.color.holo_red_dark)) }
            tv_is_license_active.setText(R.string.license_is_not_active_but_purchased)
        } else {
            context?.let { tv_is_license_active.setTextColor(ContextCompat.getColor(it, android.R.color.holo_red_dark)) }
            tv_is_license_active.setText(R.string.license_is_not_active)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
            R.id.action_delete_license -> {
                val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
                    viewModel.consumePurchase()
                }
                showAlertDialog(clickListenerPositiveButton, message = getString(R.string.license_will_be_deleted))
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_check_license -> GlobalScope.launch(Dispatchers.Main) { viewModel.checkPurchase() }
            R.id.btn_buy_activate_license ->
                if (!viewModel.isLicenseActive()) {
                    val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
                        viewModel.purchaseLicense(activity as Activity)
                    }
                    showAlertDialog(clickListenerPositiveButton, message = getString(R.string.license_will_be_activated))
                }
            R.id.btn_consume_license -> viewModel.consumePurchase()
            R.id.btn_sign_in -> {
                if (!isValidEmail(tiet_email.text)) {
                    tiet_email.error = getString(R.string.incorrect_email)
                }
                if (tiet_password.text.isNullOrBlank()) {
                    tiet_password.error = getString(R.string.password_is_blank)
                }
                if (isValidEmail(tiet_email.text) && !tiet_password.text.isNullOrEmpty()) {
                    viewModel.signIn(tiet_email.text.toString(), tiet_password.text.toString())
                }
            }
            R.id.btn_sign_up -> {
                if (!isValidEmail(tiet_email.text)) {
                    tiet_email.error = getString(R.string.incorrect_email)
                }
                if (tiet_password.text.isNullOrBlank()) {
                    tiet_password.error = getString(R.string.password_is_blank)
                }
                if (isValidEmail(tiet_email.text) && !tiet_password.text.isNullOrEmpty()) {
                    viewModel.signUp(tiet_email.text.toString(), tiet_password.text.toString())
                }
            }
            R.id.btn_sign_out -> {
                viewModel.signOut()
                job.cancel()
                setViewDefaultText()
                btn_send_email_verification.visibility = View.GONE
            }
            R.id.btn_send_email_verification -> {
                viewModel.sendEmailVerification()
                viewModel.isEmailVerifySend.postValue(true)
                updateUIEmailVerify()
            }
            R.id.btn_activate ->
                if (!viewModel.isLicenseActive()) {
                    val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
                        viewModel.activatePurchase()
                    }
                    showAlertDialog(clickListenerPositiveButton, message = getString(R.string.license_will_be_activated))
                }
            R.id.tv_forgot_pass -> {
                if (!isValidEmail(tiet_email.text)) {
                    tiet_email.error = getString(R.string.incorrect_email)
                } else {
                    tv_forgot_pass.isEnabled = false
                    viewModel.forgotPass(tiet_email.text.toString())
                }
            }
        }
    }

    private fun updateUIEmailVerify() {
        job.cancel()
        job = Job()
        GlobalScope.launch(Dispatchers.Main + job) {
            val tvText = getText(R.string.email_not_verified)
            val btnText = getText(R.string.verify_email)
            btn_send_email_verification.visibility = View.VISIBLE
            tv_email_not_verified.visibility = View.VISIBLE
            btn_send_email_verification.isEnabled = false
            var tvEmailIsNotVerifiedText: String
            var btnEmailVerifyText: String
            var i = 60
            while (viewModel.isEmailNotVerified.value == true) {
                delay(1000)
                tvEmailIsNotVerifiedText = "$tvText($i)"
                tv_email_not_verified.text = tvEmailIsNotVerifiedText
                if (viewModel.isEmailVerifySend.value == true) {
                    btn_send_email_verification.text = tvEmailIsNotVerifiedText
                    btnEmailVerifyText = "$btnText $i"
                    btn_send_email_verification.text = btnEmailVerifyText
                } else {
                    btn_send_email_verification.isEnabled = true
                }
                i--
                if (i == 0) {
                    i = 60
                    btn_send_email_verification.text = btnText
                    btn_send_email_verification.isEnabled = true
                    viewModel.isEmailVerifySend.postValue(false)
                }

            }
            btn_send_email_verification.visibility = View.GONE
            tv_email_not_verified.visibility = View.GONE
            setViewDefaultText()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    private fun setViewDefaultText() {
        btn_send_email_verification.setText(R.string.verify_email)
        tv_email_not_verified.setText(R.string.email_not_verified)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_purchase_fragment, menu)
    }
}
