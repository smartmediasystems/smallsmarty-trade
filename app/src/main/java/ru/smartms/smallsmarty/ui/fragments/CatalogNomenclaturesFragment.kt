package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.obsez.android.lib.filechooser.ChooserDialog
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.ui.adapters.CatalogNomenclaturesRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.CatalogNomenclaturesViewModel


class CatalogNomenclaturesFragment : BaseFragment() {

    var data: ArrayList<CatalogNomenclature> = ArrayList()

    private lateinit var viewModel: CatalogNomenclaturesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CatalogNomenclaturesViewModel::class.java)
        viewModel.getCatalogNomenclatures()?.observe(this, Observer { catalogNomenclatures ->
            data.clear()
            data.addAll(catalogNomenclatures!!)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.isNavigateToPref.observe(this, Observer { isNavigateToPref ->
            if (isNavigateToPref == true) {
                showSnackbarAndNavigateToPref()
                viewModel.isNavigateToPref.postValue(false)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(CatalogNomenclaturesRVAdapter(data))
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_catalog_nomenclature_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.load_catalog_nomenclatures -> {
                if (viewModel.isAllowDataSync()) {
                    viewModel.loadCatalogNomenclaturesFromREST()
                } else {
                    showSnackbarAndNavigateToPref()
                }
            }
            R.id.delete_catalog_nomenclatures -> GlobalScope.launch(Dispatchers.Default) {
                viewModel.deleteAllCatalogNomenclatures()
            }
            R.id.action_delete_data -> viewModel.deleteAllData()
            R.id.action_load_catalog_nomenclatures_and_barcodes -> viewModel.loadAllData()
            R.id.action_load_data_from_file -> if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestReadStoragePermission()
            } else {
                ChooserDialog(context)
                        .withFilter(false, false, "json", "txt")
                        .withStartFile("${Environment.getExternalStorageDirectory()}")
                        .withChosenListener { path, _ -> viewModel.loadFromFile(path) }
                        .build()
                        .show()
            }
        }
        return true
    }

    private fun showSnackbarAndNavigateToPref() {
        Snackbar.make(activity?.findViewById(android.R.id.content) as View,
                getString(R.string.data_synchronization_is_disabled_in_the_settings), Snackbar.LENGTH_LONG)
                .setAction(R.string.go_to) {
                    findNavController().navigate(R.id.action_catalogNomenclaturesFragment_to_prefsMainFragment)
                }
                .show()
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            GlobalScope.launch(Dispatchers.Main) {
                withContext(Dispatchers.Default) {
                    viewModel.deleteCatalogNomenclature(data[position].refKey)
                }
                Toast.makeText(context, getString(R.string.item_was_deleted_from_device),
                        Toast.LENGTH_LONG).show()
            }
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.item_will_be_remove), position)
    }
}
