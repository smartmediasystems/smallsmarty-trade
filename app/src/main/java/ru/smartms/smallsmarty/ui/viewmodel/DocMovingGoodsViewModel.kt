package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.utils.getCurrentDateTime
import javax.inject.Inject

class DocMovingGoodsViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docMovingGoodsRepository: DocMovingGoodsRepository

    @Inject
    lateinit var tabDocMovingGoodsRepository: TabDocMovingGoodsRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocMovingGoods(): LiveData<List<DocMovingGoods>>? {
        return docMovingGoodsRepository.getAll()
    }

    fun deleteAllDocMovingGoods() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docMovingGoodsRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocMovingGoods(refKey: String) {
        GlobalScope.launch {
            docMovingGoodsRepository.delete(refKey)
            tabDocMovingGoodsRepository.delete(refKey)
        }
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            docMovingGoodsRepository.insertAll(docMovingGoodsRepository.loadFromFile(path) as MutableList<DocMovingGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            isLoadingProgressBar.postValue(false)
        }
    }

    fun createDocMovingGoods() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val id = if (docMovingGoodsRepository.getLastDocMovingGoodsOrderById()?.id is Long) docMovingGoodsRepository.getLastDocMovingGoodsOrderById()?.id!! + 1L else 1L
            val docMovingGoods = DocMovingGoods()
            docMovingGoods.id = id
            docMovingGoods.refKey = id.toString()
            docMovingGoods.number = id.toString()
            docMovingGoods.date = getCurrentDateTime()
            docMovingGoodsRepository.insert(docMovingGoods)
            isLoadingProgressBar.postValue(false)
        }
    }
}
