package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogOSAllTabDocInventoryOS
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOSes
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import ru.smartms.smallsmarty.utils.lat2cyr
import java.io.File
import javax.inject.Inject

class TabDocInventoryOSViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val paths: MutableLiveData<Array<String?>> = MutableLiveData()
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var refKeyDoc: String

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var tabDocInventoryOSRepository: TabDocInventoryOSRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOSAllTabDocInventoryOS(refKey: String): LiveData<List<CatalogOSAllTabDocInventoryOS>>? {
        refKeyDoc = refKey
        return tabDocInventoryOSRepository.getAllLiveData(refKey)
    }

    fun handleBarcode(barcode: String) {
        GlobalScope.launch {
            val code = lat2cyr(barcode)
            var catalogOSes = CatalogOSes()
            catalogOSRepository.getCatalogOS(code)?.let { (catalogOSes.value as MutableList).add(it) }
            if (catalogOSes.value.isEmpty()) {
                try {
                    catalogOSes = catalogOSRepository.loadFromRest(null, null, null, "Code eq '$code' and $IS_FOLDER_FILTER", null, ORDER_BY_CATALOG).await()
                } catch (e: Throwable) {
                    Log.e("Load from REST OS", e.localizedMessage)
                    errorMessageRepository.insertMessage(e.localizedMessage)
                }
            }
            if (!catalogOSes.value.isEmpty()) {
                catalogOSRepository.insertAll(catalogOSes.value)
                var tabDocInventoryOS: TabDocInventoryOS? = tabDocInventoryOSRepository.getTabDocInventoryOSgetViaCatalogOSKeyRefKeyDoc(refKeyDoc, catalogOSes.value[0].refKey)
                if (tabDocInventoryOS != null) {
                    tabDocInventoryOS.isAvailabilityAct = true
                } else {
                    tabDocInventoryOS = TabDocInventoryOS(catalogOSKey = catalogOSes.value[0].refKey)
                    tabDocInventoryOS.refKeyDoc = refKeyDoc
                    tabDocInventoryOS.lineNumber = if (tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber != null) tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber!! + 1 else 1
                }
                tabDocInventoryOSRepository.insert(tabDocInventoryOS)
            }
        }
    }

    fun uploadDocInventoryOS() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val docInventoryOS: DocInventoryOS = getDocInventoryOS()
            try {
                docInventoryOSRepository.upload(docInventoryOS).await()
                isLoadingProgressBar.postValue(false)
            } catch (e: Throwable) {
                isLoadingProgressBar.postValue(false)
                Log.e("ERROR", e.localizedMessage)
                errorMessageRepository.insertMessage(e.localizedMessage)
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun getDocInventoryOS(): DocInventoryOS {
        val docInventoryOS: DocInventoryOS = docInventoryOSRepository.get(refKeyDoc)
        val tabDocInventoryOSes: List<TabDocInventoryOS> = tabDocInventoryOSRepository.getTabDocInventoryOS(refKeyDoc)
        docInventoryOS.os = tabDocInventoryOSes
        return docInventoryOS
    }

    fun saveToFile(appName: String) {
        isLoadingProgressBar.postValue(true)
        val docInventoryOS: DocInventoryOS = getDocInventoryOS()
        var file: File? = null
        try {
            file = docInventoryOSRepository.saveToFile(docInventoryOS, appName)
            isLoadingProgressBar.postValue(false)
        } catch (e: Throwable) {
            isLoadingProgressBar.postValue(false)
            Log.e("ERROR", e.localizedMessage)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        if (file != null) {
            paths.postValue(arrayOf(file.absolutePath))
        }
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOS() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOSRepository.delete(refKeyDoc)
            docInventoryOSRepository.delete(refKeyDoc)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun deleteTabDocInventoryOS(id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOSRepository.delete(id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun setIsEditDocInventoryOS(isEdit: Boolean) {
        GlobalScope.launch {
            docInventoryOSRepository.setIsEditDocInventoryOS(refKeyDoc, isEdit)
        }
    }

    fun setAct(checked: Boolean, id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOSRepository.updateIsAct(checked, id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun insert(docInventoryOS: DocInventoryOS) {
        docInventoryOSRepository.insert(docInventoryOS)
    }
}
