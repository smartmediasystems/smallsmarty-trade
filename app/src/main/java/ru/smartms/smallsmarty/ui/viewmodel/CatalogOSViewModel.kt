package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOSes
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import javax.inject.Inject

class CatalogOSViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOSes(): LiveData<List<CatalogOS>>? {
        return catalogOSRepository.getCatalogOSesLiveData()
    }

    fun loadCatalogOSesFromREST() = GlobalScope.launch {
        var catalogOSes: CatalogOSes
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                catalogOSes = catalogOSRepository.loadFromRest(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        null,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogOSRepository.insertAll(catalogOSes.value)
            } while (catalogOSes.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllCatalogOSes()  = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        catalogOSRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteCatalogOS(refKey: String) {
        catalogOSRepository.delete(refKey)
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            mDocRecountGoodsRepository.insertAll(mDocRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOSRepository.insertAll(catalogOSRepository.loadFromFile(path))
            docInventoryOSRepository.insertAll(docInventoryOSRepository.loadFromFile(path) as MutableList<DocInventoryOS>)
            isLoadingProgressBar.postValue(false)
        }
    }
}
