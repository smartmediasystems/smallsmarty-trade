package ru.smartms.smallsmarty.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.ui.adapters.DocMovingGoodsRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.DocMovingGoodsViewModel
import android.util.Log
import androidx.navigation.fragment.findNavController

class DocMovingGoodsFragment : BaseFragment(), View.OnClickListener {

    var data: ArrayList<DocMovingGoods> = ArrayList()

    private lateinit var viewModel: DocMovingGoodsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(activity!!).get(DocMovingGoodsViewModel::class.java)
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.getDocMovingGoods()?.observe(this, Observer { docMovingGoodses ->
            data.clear()
            data.addAll(docMovingGoodses!!)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        Log.d("VIEWMODEL", viewModel.toString())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(DocMovingGoodsRVAdapter(data, this))
        fab?.setOnClickListener(this)
        view
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteDocMovingGoods(data[position].refKey)
            Toast.makeText(context, getString(R.string.document_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.doc_will_be_remove), position)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_doc_moving_goods_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.delete_all_doc_moving_goods -> viewModel.deleteAllDocMovingGoods()
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.doc_moving_goods_item -> {
                val bundle = Bundle()
                bundle.putString("refKeyDoc", data[recyclerView.getChildAdapterPosition(v)].refKey)
                findNavController().navigate(R.id.action_docMovingGoodsFragment_to_tabDocMovingGoodsFragment, bundle)
            }
            R.id.fab -> viewModel.createDocMovingGoods()
        }
    }

    override fun onResume() {
        super.onResume()
        fab?.show()
    }
}
