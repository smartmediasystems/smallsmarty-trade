package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoodses
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.getCurrentDateTime
import javax.inject.Inject

class DocRecountGoodsViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var mTabDocRecountGoodsRepository: TabDocRecountGoodsRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    fun isLicenseActive(): Boolean {
        return sharedPreferencesRepository.getIsLicenseActive()
    }

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocRecountGoods(): LiveData<List<DocRecountGoods>>? {
        return docRecountGoodsRepository.getAll()
    }

    fun loadDocRecountGoodsFromRest() =
            GlobalScope.launch {
                var docRecountGoodses: DocRecountGoodses
                var i = 0
                try {
                    isLoadingProgressBar.postValue(true)
                    do {
                        i++
                        docRecountGoodses = docRecountGoodsRepository.loadFromRest(
//                        "Ref_Key, Date, Number, DeletionMark, Организация_Key, Комментарий",
                                BuildConfig.DOC_QTY_REQ * i,
                                BuildConfig.DOC_QTY_REQ * (i - 1),
                                null,
                                null).await()
                        for (doc in docRecountGoodses.value) {
                            doc.isDownloaded = true
                        }
                        docRecountGoodsRepository.insertAll(docRecountGoodses.value as MutableList<DocRecountGoods>)
                    } while (docRecountGoodses.value.isNotEmpty())
                    isLoadingProgressBar.postValue(false)
                } catch (e: Throwable) {
                    isLoadingProgressBar.postValue(false)
                    errorMessageRepository.insertMessage(e.localizedMessage)
                }
            }

    fun deleteAllDocRecountGoods() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docRecountGoodsRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocRecountGoods(refKey: String) {
        GlobalScope.launch {
            docRecountGoodsRepository.delete(refKey)
            mTabDocRecountGoodsRepository.delete(refKey)
        }
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            docRecountGoodsRepository.insertAll(docRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            isLoadingProgressBar.postValue(false)
        }
    }

    fun createDocRecountGoods() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val id = if (docRecountGoodsRepository.getLastDocRecountGoodsOrderById()?.id is Long) docRecountGoodsRepository.getLastDocRecountGoodsOrderById()?.id!! + 1L else 1L
            val docRecountGoods = DocRecountGoods()
            docRecountGoods.id = id
            docRecountGoods.refKey = id.toString()
            docRecountGoods.number = id.toString()
            docRecountGoods.date = getCurrentDateTime()
            docRecountGoodsRepository.insert(docRecountGoods)
            isLoadingProgressBar.postValue(false)
        }
    }
}
