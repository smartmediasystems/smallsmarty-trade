package ru.smartms.smallsmarty.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.ui.adapters.DocInventoryOSRVAdapter
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.smartms.smallsmarty.ui.viewmodel.DocInventoryOSSelectDocViewModel


class DocInventoryOSSelectDocFragment : BaseFragment(), View.OnClickListener {

    var data: ArrayList<DocInventoryOS> = ArrayList()

    private lateinit var viewModel: DocInventoryOSSelectDocViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DocInventoryOSSelectDocViewModel::class.java)
        viewModel.getDocInventoryOS()?.observe(this, Observer { docInventoryOSes ->
            data.clear()
            data.addAll(docInventoryOSes!!)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(DocInventoryOSRVAdapter(data, this))
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }
//
//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
//        inflater.inflate(R.menu.menu_doc_inventory_os_fragment, menu)
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return false
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.doc_inventory_os_item -> {
                GlobalScope.launch(Dispatchers.Main) {
                    progressBar.visibility = View.VISIBLE
                    withContext(Dispatchers.Default) {
                        val refKeyOS: String = arguments?.getString("refKeyOS", "")!!
                        val refKeyDoc = data[recyclerView.getChildAdapterPosition(v)].refKey
                        viewModel.handleRefKey(refKeyOS, refKeyDoc)
                    }
                    progressBar.visibility = View.GONE
                    findNavController().popBackStack()
                }
            }
        }
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteDocInventoryOS(data[position].refKey)
            Toast.makeText(context, getString(R.string.document_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.doc_will_be_remove), position)
    }
}
