package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import kotlinx.android.synthetic.main.tab_doc_inventory_os_item.view.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogOperationObjectAllTabDocInventoryOperationObject


class TabDocInventoryOperationObjectRVAdapter(private val dataModels: ArrayList<CatalogOperationObjectAllTabDocInventoryOperationObject>,
                                              private val onClickListener: View.OnClickListener) : BaseRecyclerViewAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tab_doc_inventory_os_item, parent, false)
        return TabDocInventoryGoodsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as TabDocInventoryGoodsViewHolder
        viewHolder.tvCatalogOperationObject.text = if (!dataModels[position].catalogOperationObjectDescription.isEmpty()) dataModels[position].catalogOperationObjectDescription else viewHolder.tvCatalogOperationObject.context.getText(R.string.no_description)
        viewHolder.cbAcc.isChecked = dataModels[position].isAvailabilityAcc
        viewHolder.cbAct.isChecked= dataModels[position].isAvailabilityAct
        viewHolder.tvCatalogOperationObject.setOnClickListener(onClickListener)
        viewHolder.cbAct.setOnClickListener(onClickListener)
    }

    override fun getItemCount(): Int {
        return dataModels.size
    }

    inner class TabDocInventoryGoodsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCatalogOperationObject: TextView = itemView.tv_catalog_os
        var cbAcc: CheckBox = itemView.cb_is_acc
        var cbAct: CheckBox = itemView.cb_is_act
    }
}
