package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObjects
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOperationObject
import ru.smartms.smallsmarty.extensions.await
import javax.inject.Inject

class DocInventoryOperationObjectSelectDocViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOperationObjectRepository: DocInventoryOperationObjectRepository

    @Inject
    lateinit var tabDocInventoryOperationObjectRepository: TabDocInventoryOperationObjectRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocInventoryOperationObject(): LiveData<List<DocInventoryOperationObject>>? {
        return docInventoryOperationObjectRepository.getAll()
    }

    fun loadDocInventoryOperationObjectFromRest() = GlobalScope.launch {
        var docInventoryOperationObjects: DocInventoryOperationObjects
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                docInventoryOperationObjects = docInventoryOperationObjectRepository.loadFromRest(
                        BuildConfig.DOC_QTY_REQ * i,
                        BuildConfig.DOC_QTY_REQ * (i - 1),
                        null,
                        null).await()
                docInventoryOperationObjectRepository.insertAll(docInventoryOperationObjects.value as MutableList<DocInventoryOperationObject>)
            } while (docInventoryOperationObjects.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllDocInventoryOperationObject() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docInventoryOperationObjectRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOperationObject(refKey: String) {
        GlobalScope.launch {
            docInventoryOperationObjectRepository.delete(refKey)
            tabDocInventoryOperationObjectRepository.delete(refKey)
        }
    }

    fun handleRefKey(refKeyOperationObject: String, refKeyDoc: String) {
        val catalogOperationObject = catalogOperationObjectRepository.getCatalogOperationObjectRefKey(refKeyOperationObject)
        if (catalogOperationObject != null) {
            var tabDocInventoryOperationObject: TabDocInventoryOperationObject? = tabDocInventoryOperationObjectRepository.getTabDocInventoryOperationObjectgetViaCatalogOperationObjectKeyRefKeyDoc(refKeyDoc, catalogOperationObject.refKey)
            if (tabDocInventoryOperationObject != null) {
                tabDocInventoryOperationObject.isAvailabilityAct = true
            } else {
                tabDocInventoryOperationObject = TabDocInventoryOperationObject(catalogOperationObjectKey = catalogOperationObject.refKey)
                tabDocInventoryOperationObject.refKeyDoc = refKeyDoc
                tabDocInventoryOperationObject.lineNumber = if (tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber != null) tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber!! + 1 else 1
            }
            tabDocInventoryOperationObjectRepository.insert(tabDocInventoryOperationObject)
        }
    }
}
