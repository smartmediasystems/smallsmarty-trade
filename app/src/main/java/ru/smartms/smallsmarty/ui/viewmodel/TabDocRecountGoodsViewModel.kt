package ru.smartms.smallsmarty.ui.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import handleOS
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeriesOData
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.TabDocRecountGoods
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.NULL_REF
import ru.smartms.smallsmarty.utils.revertBarcodeAndGetSeries
import sendOSKey
import java.io.File
import javax.inject.Inject

class TabDocRecountGoodsViewModel(application: Application) : AndroidViewModel(application) {

    init {
        App.component.inject(this)
    }

    val paths: MutableLiveData<Array<String?>> = MutableLiveData()
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    val refKeyOS: MutableLiveData<String> = MutableLiveData()
    val infRegBarcodeNomenclatureLiveData: MutableLiveData<InfRegBarcodeNomenclature> = MutableLiveData()
    val catalogSeriesLiveData: MutableLiveData<CatalogSeries> = MutableLiveData()
    val isNavigateToPref: MutableLiveData<Boolean> = MutableLiveData()

    lateinit var refKeyDoc: String

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var tabDocRecountGoodsRepository: TabDocRecountGoodsRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository

    @Inject
    lateinit var catalogSeriesRepository: CatalogSeriesRepository

    @Inject
    lateinit var arGoodsInStoreRepository: ARGoodsInStoreRepository

    fun isLicenseActive(): Boolean = sharedPreferencesRepository.getIsLicenseActive()

    fun getScanData(): LiveData<List<ScanData>>? = scanDataRepository.getScanData()

    fun deleteScanData() = scanDataRepository.deleteAll()

    fun getCatalogNomenclatureAllTabDocRecountGoods(refKey: String): LiveData<List<CatalogNomenclatureAllTabDocRecountGoods>>? {
        refKeyDoc = refKey
        return tabDocRecountGoodsRepository.getAll(refKey)
    }

    fun handleBarcode(barcode: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val infRegBarcodeNomenclature = infRegBarcodeRepository.get(barcode)
            if (infRegBarcodeNomenclature != null) {
                if (sharedPreferencesRepository.getScanOneByOne()) {
                    insertTabDocRecountGoods(infRegBarcodeNomenclature, 1.0)
                } else {
                    infRegBarcodeNomenclatureLiveData.postValue(infRegBarcodeNomenclature)
                }
                isLoadingProgressBar.postValue(false)
                return@launch
            }
            val catalogSeries = catalogSeriesRepository.getByBarcode(barcode)
            if (catalogSeries != null) {
                catalogSeries.arGoodsInStore = arGoodsInStoreRepository.getRecordWithQty(catalogSeries)
                if (catalogSeries.arGoodsInStore != null) {
                    if (sharedPreferencesRepository.getScanOneByOne()) {
                        insertTabDocRecountGoods(catalogSeries, 1.0)
                    } else {
                        catalogSeriesLiveData.postValue(catalogSeries)
                    }
                    isLoadingProgressBar.postValue(false)
                    return@launch
                }
            }
            val catalogOS = handleOS(barcode, catalogOperationObjectRepository)
            if (catalogOS != null) {
                sendOSKey(catalogOS.refKey, refKeyOS, isLoadingProgressBar)
                isLoadingProgressBar.postValue(false)
                return@launch
            }
            handleBarcodeRemote(barcode)
            isLoadingProgressBar.postValue(false)
        }
    }

    private suspend fun handleBarcodeRemote(barcode: String) {
        if (!isAllowDataSync()) {
            isNavigateToPref.postValue(true)
            return
        }
        val infRegBarcodeNomenclature = getInfRegBarcodeFromRest(barcode)
        if (infRegBarcodeNomenclature != null) {
            if (sharedPreferencesRepository.getScanOneByOne()) {
                insertTabDocRecountGoods(infRegBarcodeNomenclature, 1.0)
            } else {
                infRegBarcodeNomenclatureLiveData.postValue(infRegBarcodeNomenclature)
            }
            return
        }
        val catalogSeries = getCatalogSeriesBarcodeFromRest(barcode)
        if (catalogSeries != null) {
            catalogSeries.arGoodsInStore = arGoodsInStoreRepository.getRecordWithQty(catalogSeries)
            if (catalogSeries.arGoodsInStore != null) {
                if (sharedPreferencesRepository.getScanOneByOne()) {
                    insertTabDocRecountGoods(catalogSeries, 1.0)
                } else {
                    catalogSeriesLiveData.postValue(catalogSeries)
                }
                isLoadingProgressBar.postValue(false)
                return
            }
        }
    }

    private suspend fun getCatalogSeriesBarcodeFromRest(barcode: String): CatalogSeries? {
        var catalogSeries: CatalogSeries? = null
        val catalogSeriesTemp= revertBarcodeAndGetSeries(barcode)
        var catalogSeriesOData: CatalogSeriesOData?
        try {
            catalogSeriesOData = catalogSeriesRepository.loadFromRest(filter = "Номер eq '$barcode'").await()
            if (catalogSeriesOData.value.isEmpty()) {
                catalogSeriesOData = catalogSeriesRepository.loadFromRest(filter = "Номер eq '${catalogSeriesTemp.numberReq}' and (ГоденДо ge datetime'${catalogSeriesTemp.validUntilBegin}' and ГоденДо le datetime'${catalogSeriesTemp.validUntilEnd}')").await()
            }
            if (!catalogSeriesOData.value.isEmpty()) {
                catalogSeries = catalogSeriesOData.value[0]
            }
        } catch (e: java.lang.Exception) {
            Log.e("getInfRegBarcodeFromRest", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        return catalogSeries
    }

    private suspend fun getInfRegBarcodeFromRest(barcode: String): InfRegBarcodeNomenclature? {
        val infRegBarcodeNomenclature: InfRegBarcodeNomenclature? = null
        try {
            val infRegBarcodeNomenclatures = infRegBarcodeRepository.getFromREST(filter = "Штрихкод eq '$barcode'").await()
            if (infRegBarcodeNomenclatures.value.isNotEmpty()) {
                infRegBarcodeRepository.insert(infRegBarcodeNomenclatures.value[0])
                catalogNomenclatureRepository.insert(infRegBarcodeNomenclatures.value[0].catalogNomenclature)
            }
        } catch (e: Exception) {
            Log.e("getInfRegBarcodeFromRest", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        return infRegBarcodeNomenclature
    }

    fun insertTabDocRecountGoods(infRegBarcodeNomenclature: InfRegBarcodeNomenclature, qty: Double) {
        var tabDocRecountGoods: TabDocRecountGoods? = tabDocRecountGoodsRepository.getTabDocRecountGoodsGetViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc, infRegBarcodeNomenclature.catalogNomenclatureKey, infRegBarcodeNomenclature.catalogCharacteristicKey)
        if (tabDocRecountGoods != null) {
            tabDocRecountGoods.qty = tabDocRecountGoods.qty + qty
            tabDocRecountGoods.qtyPkg = tabDocRecountGoods.qtyPkg + qty
        } else {
            tabDocRecountGoods = TabDocRecountGoods()
            tabDocRecountGoods.refKeyDoc = refKeyDoc
            tabDocRecountGoods.catalogNomenclatureKey = infRegBarcodeNomenclature.catalogNomenclatureKey
            tabDocRecountGoods.catalogCharacteristicKey = infRegBarcodeNomenclature.catalogCharacteristicKey
            tabDocRecountGoods.qty = qty
            tabDocRecountGoods.qtyPkg = qty
            tabDocRecountGoods.lineNumber = if (tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber != null) tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber!! + 1 else 1
        }
        tabDocRecountGoodsRepository.insert(tabDocRecountGoods)
        infRegBarcodeNomenclatureLiveData.postValue(null)
    }

    fun insertTabDocRecountGoods(catalogSeries: CatalogSeries, qty: Double) {
        val arGoodsInStore = catalogSeries.arGoodsInStore
        if (arGoodsInStore != null) {
            var tabDocRecountGoods: TabDocRecountGoods? = tabDocRecountGoodsRepository.getTabDocRecountGoodsGetViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc, arGoodsInStore.catalogNomenclatureKey, arGoodsInStore.catalogCharacteristicKey, catalogSeries.refKey)
            if (tabDocRecountGoods != null) {
                tabDocRecountGoods.catalogSeriesKey = catalogSeries.refKey
                tabDocRecountGoods.qty = tabDocRecountGoods.qty + qty
                tabDocRecountGoods.qtyPkg = tabDocRecountGoods.qtyPkg + qty
            } else {
                tabDocRecountGoods = TabDocRecountGoods()
                tabDocRecountGoods.refKeyDoc = refKeyDoc
                tabDocRecountGoods.catalogNomenclatureKey = arGoodsInStore.catalogNomenclatureKey
                tabDocRecountGoods.catalogCharacteristicKey = arGoodsInStore.catalogCharacteristicKey
                tabDocRecountGoods.catalogSeriesKey = catalogSeries.refKey
                tabDocRecountGoods.qty = qty
                tabDocRecountGoods.qtyPkg = qty
                tabDocRecountGoods.lineNumber = if (tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber != null) tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber!! + 1 else 1
            }
            tabDocRecountGoodsRepository.insert(tabDocRecountGoods)
            catalogSeriesLiveData.postValue(null)
        }

    }

    fun uploadDocRecountGoods() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            var docRecountGoods: DocRecountGoods = getDocRecountGoods()
            try {
                if (docRecountGoods.isDownloaded) {
                    mDocRecountGoodsRepository.upload(docRecountGoods).await()
                } else {
                    docRecountGoods.refKey = NULL_REF
                    docRecountGoods = mDocRecountGoodsRepository.uploadNew(docRecountGoods).await()
                    docRecountGoods.isEdit = true
                    mDocRecountGoodsRepository.insert(docRecountGoods)
                    mDocRecountGoodsRepository.delete(refKeyDoc)
                    refKeyDoc = docRecountGoods.refKey
                }
                isLoadingProgressBar.postValue(false)
            } catch (e: Throwable) {
                isLoadingProgressBar.postValue(false)
                errorMessageRepository.insertMessage(e.localizedMessage)
                Log.e("ERROR", e.localizedMessage)
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun getDocRecountGoods(): DocRecountGoods {
        val docRecountGoods: DocRecountGoods = mDocRecountGoodsRepository.get(refKeyDoc)
        val tabDocRecountGoods: List<TabDocRecountGoods> = tabDocRecountGoodsRepository.getTabDocRecountGoods(refKeyDoc)
        docRecountGoods.goods = tabDocRecountGoods
        return docRecountGoods
    }

    fun saveToFile(appName: String) {
        isLoadingProgressBar.postValue(true)
        val docRecountGoods: DocRecountGoods = getDocRecountGoods()
        var file: File? = null
        try {
            file = mDocRecountGoodsRepository.saveToFile(docRecountGoods, appName)
            isLoadingProgressBar.postValue(false)
        } catch (e: Throwable) {
            isLoadingProgressBar.postValue(false)
            Log.e("ERROR", e.localizedMessage)
        }
        if (file != null) {
            paths.postValue(arrayOf(file.absolutePath))
        }
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocRecountGoods() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocRecountGoodsRepository.delete(refKeyDoc)
            mDocRecountGoodsRepository.delete(refKeyDoc)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun deleteTabDocRecountGoods(id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocRecountGoodsRepository.delete(id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun setIsEditDocRecountGoods(isEdit: Boolean) {
        GlobalScope.launch {
            mDocRecountGoodsRepository.setIsEditDocRecountGoods(refKeyDoc, isEdit)
        }
    }

    fun insert(docRecountGoods: DocRecountGoods) = mDocRecountGoodsRepository.insert(docRecountGoods)

    fun isAllowDataSync(): Boolean = sharedPreferencesRepository.isAllowDataSync()
}

