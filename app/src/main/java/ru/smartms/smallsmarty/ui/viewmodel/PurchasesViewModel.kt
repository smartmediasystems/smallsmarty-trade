package ru.smartms.smallsmarty.ui.viewmodel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.android.billingclient.api.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.billingmodule.BillingManager
import ru.smartms.smallsmarty.billingmodule.SKU_SMALLSMARTY_TRADE_PRO
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.PurchaseInnApp
import ru.smartms.smallsmarty.db.entity.getPurchaseInnApp
import ru.smartms.smallsmarty.utils.BillingResponseCodeException
import ru.smartms.smallsmarty.utils.PRODUCT_ID
import ru.smartms.smallsmarty.utils.SharedPreferenceLiveData
import java.lang.Exception
import java.util.ArrayList
import javax.inject.Inject


class PurchasesViewModel(application: Application) : AndroidViewModel(application), BillingManager.BillingUpdatesListener {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    val isDisableRegistration: MutableLiveData<Boolean> = MutableLiveData()
    val isEmailNotVerified: MutableLiveData<Boolean> = MutableLiveData()
    val isEmailVerifySend: MutableLiveData<Boolean> = MutableLiveData()
    val isForgotPassSend: MutableLiveData<Boolean> = MutableLiveData()
    val message: MutableLiveData<String> = MutableLiveData()
    val email: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()
    val purchasesInnAppLiveData: MutableLiveData<ArrayList<PurchaseInnApp>> = MutableLiveData()
    var purchaseInnApp: PurchaseInnApp? = null
    val purchasesInnApp = ArrayList<PurchaseInnApp>()

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    @Inject
    lateinit var firebaseRepository: FirebaseRepository

    @Inject
    lateinit var googleAPIsRepository: GoogleAPIsRepository

    @Inject
    lateinit var auth: FirebaseAuth

    private val billingManager: BillingManager by lazy {
        BillingManager(getApplication(), this)
    }

    fun isLicenseActiveLiveData(): SharedPreferenceLiveData<Boolean> = sharedPreferencesRepository.getIsLicenseActiveLiveData()

    fun getEmail(): String {
        return sharedPreferencesRepository.getEmail()
    }

    fun getPassword(): String {
        return sharedPreferencesRepository.getPassword()
    }

    fun purchaseLicense(activity: Activity) {
        GlobalScope.launch(Dispatchers.Main) {
            isLoadingProgressBar.postValue(true)
            if (auth.currentUser == null) {
                isLoadingProgressBar.postValue(false)
                message.postValue(getApplication<Application>().getString(R.string.login_required))
                return@launch
            }
            val isServiceConnected = try {
                billingManager.startServiceConnectionAwait()
            } catch (e: BillingResponseCodeException) {
                false
            }
            if (isServiceConnected) {
                val skuList = ArrayList<String>()
                skuList.add(SKU_SMALLSMARTY_TRADE_PRO)
                val skuDetailsParams = SkuDetailsParams.newBuilder()
                        .setSkusList(skuList).setType(BillingClient.SkuType.INAPP).build()
                val skuDetailsList = billingManager.querySkuDetailsAsyncAwait(skuDetailsParams)
                if (!skuDetailsList.isEmpty()) {
                    val flowParams = BillingFlowParams.newBuilder()
                            .setSkuDetails(skuDetailsList[0])
                            .build()
                    billingManager.billingClient.launchBillingFlow(activity, flowParams)
                }
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun consumePurchase() {
        GlobalScope.launch(Dispatchers.Main) {
            isLoadingProgressBar.postValue(true)
            val isServiceConnected = try {
                billingManager.startServiceConnectionAwait()
            } catch (e: BillingResponseCodeException) {
                false
            }
            if (isServiceConnected) {
                try {
                    val purchases = billingManager.getPurchasesAsync()
                    if (!purchases.isEmpty()) {
                        val responseCode = billingManager.consumeAsyncAwait(purchases[0].purchaseToken)
                        if (responseCode == BillingClient.BillingResponse.OK) {

                        }
                        firebaseRepository.deletePurchaseHistory()
                        firebaseRepository.deleteAll()
                        purchaseInnApp = null
                        purchasesInnAppLiveData.postValue(null)
                        sharedPreferencesRepository.setIsLicenseActive(false)
                    }
                } catch (e: Exception) {
                    errorMessageRepository.insertMessage(e.localizedMessage)
                }
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun checkPurchase() {
        if (auth.currentUser == null) {
            isLoadingProgressBar.postValue(false)
            message.postValue(getApplication<Application>().getString(R.string.login_required))
            return
        }
        isLoadingProgressBar.postValue(true)
        purchasesInnAppLiveData.postValue(null)
        setPurchaseInnAppHistoryListener()
        isLoadingProgressBar.postValue(false)
    }

    private suspend fun isConsumedPurchase(purchase: Purchase): Boolean {
        val p = googleAPIsRepository.getPurchaseProduct(getApplication<Application>().packageName, PRODUCT_ID, purchase.purchaseToken)
        return p?.let { it.consumptionState == 1 && it.purchaseState == 0 } ?: true
    }

    fun activatePurchase() {
        GlobalScope.launch(Dispatchers.Main) {
            isLoadingProgressBar.postValue(true)
            if (purchasesInnApp.size > 2) {
                isLoadingProgressBar.postValue(false)
                message.postValue(getApplication<Application>().getString(R.string.maximum_number_activations_reached))
                return@launch
            }
            if (auth.currentUser == null) {
                isLoadingProgressBar.postValue(false)
                message.postValue(getApplication<Application>().getString(R.string.login_required))
                return@launch
            }
            if (purchaseInnApp == null) {
                isLoadingProgressBar.postValue(false)
                message.postValue(getApplication<Application>().getString(R.string.no_licenses_purchased))
                return@launch
            } else {
                if (auth.currentUser != null) {
                    purchaseInnApp?.let {
                        firebaseRepository.setPurchaseInApp(it)
                        sharedPreferencesRepository.setIsLicenseActive(true)
                    }
                    checkPurchase()
                } else {
                    message.postValue(getApplication<Application>().getString(R.string.login_required))
                }
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    override fun onPurchasesUpdated(purchases: List<Purchase>) {
        if (!purchases.isEmpty()) {
            purchaseInnApp = getPurchaseInnApp(purchases[0])
            firebaseRepository.setPurchaseInAppHistory(getPurchaseInnApp(purchases[0]))
            purchasesInnAppLiveData.postValue(null)
            activatePurchase()
        }
    }

    override fun onBillingClientSetupFinished() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun signIn(email: String? = sharedPreferencesRepository.getEmail(), password: String? = sharedPreferencesRepository.getPassword()) {
        if (email != null && password != null && !email.isBlank() && !password.isBlank()) {
            isLoadingProgressBar.postValue(true)
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.authorization_successful))
                            val currentUser = auth.currentUser
                            if (currentUser != null) {
                                currentUser.reload()
                                if (!currentUser.isEmailVerified) {
                                    message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.email_not_verified))
                                    isEmailNotVerified.postValue(true)
                                } else {
                                    GlobalScope.launch(Dispatchers.Main) {
                                        checkPurchase()
                                    }
                                }
                            }
                            sharedPreferencesRepository.setEmail(email)
                            sharedPreferencesRepository.setPassword(password)
                            isDisableRegistration.postValue(true)
                        } else {
                            message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.authorization_not_successful))
                        }
                        isLoadingProgressBar.postValue(false)
                    }
                    .addOnFailureListener { e ->
                        message.postValue(e.localizedMessage)
                        isLoadingProgressBar.postValue(false)
                    }
        }
    }

    fun signUp(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        auth.currentUser?.sendEmailVerification()
                        message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.registration_successful))
                    } else {
                        message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.registration_not_successful))
                    }
                }
                .addOnFailureListener { e ->
                    message.postValue(e.localizedMessage)
                }
    }

    fun signOut() {
        auth.signOut()
        isDisableRegistration.postValue(false)
    }

    fun sendEmailVerification() {
        auth.currentUser?.sendEmailVerification()
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.email_verify_sent))
                    } else {
                        message.postValue(this@PurchasesViewModel.getApplication<Application>().getString(R.string.email_verify_not_sent))
                    }
                }
                ?.addOnFailureListener { exception ->
                    message.postValue(exception.localizedMessage)
                }
    }

    fun checkEmailVerify() {
        GlobalScope.launch {
            auth.currentUser?.let {
                while (!it.isEmailVerified) {
                    delay(10000)
                    it.reload()
                }
                isEmailNotVerified.postValue(false)
            }
        }
    }

    private fun setPurchaseInnAppListener(purchaseInn: PurchaseInnApp) {
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    purchasesInnApp.clear()
                    for (snapShot in dataSnapshot.children) {
                        snapShot.getValue(PurchaseInnApp::class.java)?.let {
                            purchasesInnApp.add(it)
                        }
                    }
                    if (purchasesInnApp.size > 0) purchasesInnAppLiveData.postValue(purchasesInnApp)
                }
            }
        }
        firebaseRepository.setPurchaseValueEventListener(valueEventListener, purchaseInn)
    }

    private fun setPurchaseInnAppHistoryListener() {
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    purchaseInnApp = dataSnapshot.children.last().getValue(PurchaseInnApp::class.java)
                    purchaseInnApp?.let {
                        setPurchaseInnAppListener(it)
                        setPurchaseInnAppChildEventListener(it)
                    }
                }
            }
        }
        firebaseRepository.setPurchaseHistoryValueEventListener(valueEventListener)
    }

    private fun setPurchaseInnAppChildEventListener(purchaseInnApp: PurchaseInnApp) {
        val childEventListener = object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d("ChildEventListener", p1)
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d("ChildEventListener", p1)
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                if (dataSnapshot.exists()) {
                    dataSnapshot.getValue(PurchaseInnApp::class.java)?.let {
                        Log.d("ChildEventListener", it.purchaseToken)
                    }
                }
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataSnapshot.getValue(PurchaseInnApp::class.java)?.let {
                        Log.d("ChildEventListener", it.purchaseToken)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("ChildEventListener", databaseError.message)
            }
        }
        firebaseRepository.setPurchaseChildEventListener(childEventListener, purchaseInnApp)
    }

    fun isLicenseActive(): Boolean {
        return sharedPreferencesRepository.getIsLicenseActive()
    }

    fun forgotPass(email: String) {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        isForgotPassSend.postValue(true)
                    }
                }
    }
}