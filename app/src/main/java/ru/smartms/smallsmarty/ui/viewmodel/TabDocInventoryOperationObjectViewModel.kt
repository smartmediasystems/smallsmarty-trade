package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogOperationObjectAllTabDocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObjects
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOperationObject
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import ru.smartms.smallsmarty.utils.lat2cyr
import java.io.File
import javax.inject.Inject

class TabDocInventoryOperationObjectViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val paths: MutableLiveData<Array<String?>> = MutableLiveData()
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var refKeyDoc: String

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOperationObjectRepository: DocInventoryOperationObjectRepository

    @Inject
    lateinit var tabDocInventoryOperationObjectRepository: TabDocInventoryOperationObjectRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOperationObjectAllTabDocInventoryOperationObject(refKey: String): LiveData<List<CatalogOperationObjectAllTabDocInventoryOperationObject>>? {
        refKeyDoc = refKey
        return tabDocInventoryOperationObjectRepository.getAllLiveData(refKey)
    }

    fun handleBarcode(barcode: String) {
        GlobalScope.launch {
            val code = lat2cyr(barcode)
            var catalogOperationObjects = CatalogOperationObjects()
            catalogOperationObjectRepository.getCatalogOperationObject(code)?.let { (catalogOperationObjects.value as MutableList).add(it) }
            if (catalogOperationObjects.value.isEmpty()) {
                try {
                    catalogOperationObjects = catalogOperationObjectRepository.loadFromRest(null, null, null, "Code eq '$code' and $IS_FOLDER_FILTER", null, ORDER_BY_CATALOG).await()
                } catch (e: Throwable) {
                    Log.e("Load from REST", e.localizedMessage)
                    errorMessageRepository.insertMessage(e.localizedMessage)
                }
            }
            if (!catalogOperationObjects.value.isEmpty()) {
                catalogOperationObjectRepository.insertAll(catalogOperationObjects.value)
                var tabDocInventoryOperationObject: TabDocInventoryOperationObject? = tabDocInventoryOperationObjectRepository.getTabDocInventoryOperationObjectgetViaCatalogOperationObjectKeyRefKeyDoc(refKeyDoc, catalogOperationObjects.value[0].refKey)
                if (tabDocInventoryOperationObject != null) {
                    tabDocInventoryOperationObject.isAvailabilityAct = true
                } else {
                    tabDocInventoryOperationObject = TabDocInventoryOperationObject(catalogOperationObjectKey = catalogOperationObjects.value[0].refKey)
                    tabDocInventoryOperationObject.refKeyDoc = refKeyDoc
                    tabDocInventoryOperationObject.lineNumber = if (tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber != null) tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber!! + 1 else 1
                }
                tabDocInventoryOperationObjectRepository.insert(tabDocInventoryOperationObject)
            }
        }
    }

    fun uploadDocInventoryOperationObject() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val docInventoryOperationObject: DocInventoryOperationObject = getDocInventoryOperationObject()
            try {
                docInventoryOperationObjectRepository.upload(docInventoryOperationObject).await()
                isLoadingProgressBar.postValue(false)
            } catch (e: Throwable) {
                isLoadingProgressBar.postValue(false)
                Log.e("ERROR", e.localizedMessage)
                errorMessageRepository.insertMessage(e.localizedMessage)
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun getDocInventoryOperationObject(): DocInventoryOperationObject {
        val docInventoryOperationObject: DocInventoryOperationObject = docInventoryOperationObjectRepository.get(refKeyDoc)
        val tabDocInventoryOperationObjects: List<TabDocInventoryOperationObject> = tabDocInventoryOperationObjectRepository.getTabDocInventoryOperationObject(refKeyDoc)
        docInventoryOperationObject.os = tabDocInventoryOperationObjects
        return docInventoryOperationObject
    }

    fun saveToFile(appName: String) {
        isLoadingProgressBar.postValue(true)
        val docInventoryOperationObject: DocInventoryOperationObject = getDocInventoryOperationObject()
        var file: File? = null
        try {
            file = docInventoryOperationObjectRepository.saveToFile(docInventoryOperationObject, appName)
            isLoadingProgressBar.postValue(false)
        } catch (e: Throwable) {
            isLoadingProgressBar.postValue(false)
            Log.e("ERROR", e.localizedMessage)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        if (file != null) {
            paths.postValue(arrayOf(file.absolutePath))
        }
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOperationObject() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOperationObjectRepository.delete(refKeyDoc)
            docInventoryOperationObjectRepository.delete(refKeyDoc)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun deleteTabDocInventoryOperationObject(id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOperationObjectRepository.delete(id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun setIsEditDocInventoryOperationObject(isEdit: Boolean) {
        GlobalScope.launch {
            docInventoryOperationObjectRepository.setIsEditDocInventoryOperationObject(refKeyDoc, isEdit)
        }
    }

    fun setAct(checked: Boolean, id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocInventoryOperationObjectRepository.updateIsAct(checked, id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun insert(docInventoryOperationObject: DocInventoryOperationObject) {
        docInventoryOperationObjectRepository.insert(docInventoryOperationObject)
    }
}
