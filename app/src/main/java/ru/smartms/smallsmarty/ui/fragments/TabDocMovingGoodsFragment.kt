package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.media.MediaScannerConnection
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods
import ru.smartms.smallsmarty.ui.adapters.TabDocMovingGoodsRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.TabDocMovingGoodsViewModel
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.content.Intent
import ru.smartms.smallsmarty.utils.REQUEST_WRITE_STORAGE
import android.content.DialogInterface
import android.hardware.Camera
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.SearchView
import android.text.InputType
import android.widget.EditText
import android.widget.LinearLayout
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import android.view.inputmethod.InputMethodManager
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class TabDocMovingGoodsFragment : BaseFragment(), View.OnClickListener, SearchView.OnQueryTextListener {

    var data: ArrayList<CatalogNomenclatureAllTabDocMovingGoods> = ArrayList()

    private lateinit var viewModel: TabDocMovingGoodsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TabDocMovingGoodsViewModel::class.java)
        val refKeyDoc: String = arguments?.getString("refKeyDoc", "")!!
        val observerList = Observer<List<CatalogNomenclatureAllTabDocMovingGoods>> { catalogNomenclatureAllTabDocMovingGoodses ->
            updateAdapter(catalogNomenclatureAllTabDocMovingGoodses)
        }
        viewModel.refKeyDocLiveData.postValue(null)
        if (refKeyDoc.isNotEmpty()) {
            viewModel.refKeyDoc = refKeyDoc
        }
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.handleBarcode(scanData[0].barcode)
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.paths.observe(this, Observer { paths ->
            MediaScannerConnection.scanFile(context, paths, null) { path, _ ->
                Log.d("SCAN File", path)
                Toast.makeText(context, "${getString(R.string.document_saved)} $path", Toast.LENGTH_LONG).show()
            }
            Toast.makeText(context, "${getString(R.string.document_saved)} ${paths?.get(0)}", Toast.LENGTH_LONG).show()
        })
        viewModel.infRegBarcodeNomenclatureLiveData.observe(this, Observer { infRegBarcodeNomenclature ->
            if (infRegBarcodeNomenclature != null) {
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle(getString(R.string.enter_qty))
                val input = EditText(context)
                input.inputType = InputType.TYPE_CLASS_NUMBER
                input.setText("1")
                input.setSelectAllOnFocus(true)
                input.selectAll()
                builder.setView(input)
                builder.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                    val qty = java.lang.Double.parseDouble(input.text.toString())
                    GlobalScope.launch {
                        viewModel.insertTabDocMovingGoods(infRegBarcodeNomenclature, qty)
                    }
                }
                builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, _ -> dialog.cancel() }
                builder.show()
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }
        })
        viewModel.getCatalogNomenclatureAllTabDocMovingGoods()?.observe(this, observerList)
        viewModel.refKeyDocLiveData.observe(this, Observer { refKeyDocLiveData ->
            if (refKeyDocLiveData != null) {
                viewModel.getCatalogNomenclatureAllTabDocMovingGoods()?.removeObserver(observerList)
                viewModel.getCatalogNomenclatureAllTabDocMovingGoods()?.observe(this, observerList)
            }
        })
    }

    private fun updateAdapter(catalogNomenclatureAllTabDocMovingGoodses: List<CatalogNomenclatureAllTabDocMovingGoods>?) {
        data.clear()
        data.addAll(catalogNomenclatureAllTabDocMovingGoodses!!)
        viewModel.setIsEditDocMovingGoods(true)
        recyclerView.adapter?.notifyDataSetChanged()
        viewModel.refKeyDocLiveData.postValue(null)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(TabDocMovingGoodsRVAdapter(data, this))
        fab?.setOnClickListener(this)
        fab?.show()
        iBtnSearchBarcode.setOnClickListener(this)
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_tab_doc_moving_goods_fragment, menu)
        val menuItemSearch = menu?.findItem(R.id.action_search)
        val searchView = menuItemSearch?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteTabDocMovingGoods(data[position].id)
            Toast.makeText(context, getString(R.string.item_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.item_will_be_remove), position)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigateUp()
            R.id.upload_doc_moving_goods -> GlobalScope.launch(Dispatchers.Main) {
                if (viewModel.isLicenseActive()) {
                    withContext(Dispatchers.Default) {
                        viewModel.uploadDocMovingGoods()
                    }
                } else {
                    findNavController().navigate(R.id.action_tabDocMovingGoodsFragment_to_purchasesFragment)
                }
            }
            R.id.delete_doc_moving_goods -> viewModel.deleteDocMovingGoods()
            R.id.action_save_to_file -> {
                if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestWriteStoragePermission()
                } else {
                    GlobalScope.launch { viewModel.saveToFile(getString(R.string.app_name)) }
                }
            }
            R.id.action_enter_barcode -> {
                item.isChecked = !item.isChecked
                if (constraintLayout.visibility == View.GONE) constraintLayout.visibility = View.VISIBLE else constraintLayout.visibility = View.GONE
            }
            R.id.action_scan_barcode_from_camera -> {
                initScanBarcode()
            }
            R.id.action_add_comment -> {
                GlobalScope.launch(Dispatchers.Main) {
                    val input = EditText(context)
                    var docMovingGoods: DocMovingGoods? = null
                    withContext(Dispatchers.Default) {
                        docMovingGoods = viewModel.getDocMovingGoods()
                    }
                    input.setText(docMovingGoods?.comment)
                    val lp = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT)
                    val alertDialog = AlertDialog.Builder(context!!)
                            .setTitle(R.string.comment)
                            .setPositiveButton(android.R.string.ok) { _, _ ->
                                docMovingGoods?.comment = input.text.toString()
                                launch {
                                    docMovingGoods?.let { viewModel.insert(it) }
                                }
                            }
                            .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    input.layoutParams = lp
                    alertDialog.setView(input)
                    alertDialog.show()
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tab_doc_moving_goods_item -> {
                val bundle = Bundle()
                bundle.putLong("id", data[recyclerView.getChildAdapterPosition(v)].id)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocMovingGoodsFragment_to_tabDocMovingGoodsEditFragment, bundle)
            }
            R.id.fab -> {
                val bundle = Bundle()
                bundle.putLong("id", -1L)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocMovingGoodsFragment_to_tabDocMovingGoodsEditFragment, bundle)
            }
            R.id.ibtn_enter_barcode -> {
                val query = etSearch.text.toString()
                etSearch.setText("")
                if (query.isNotEmpty()) {
                    viewModel.handleBarcode(query)
                }
            }
        }
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        if (!query.isEmpty()) {
            viewModel.handleBarcode(query)
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null && newText.contains("\n")) {
            viewModel.handleBarcode(newText.replace("\n", ""))
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_WRITE_STORAGE) {
            viewModel.saveToFile(getString(R.string.app_name))
        }
    }

    override fun handleScan(query: String) {
        viewModel.handleBarcode(query)
    }
}
