package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.doc_recount_goods_item.view.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.utils.convertDateTime


class DocRecountGoodsRVAdapter(private val dataModels: ArrayList<DocRecountGoods>, private val listener: View.OnClickListener) : BaseRecyclerViewAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.doc_recount_goods_item, parent, false)
        view.setOnClickListener(listener)
        return DocRecountGoodsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DocRecountGoodsViewHolder
        viewHolder.tvDocNumber.text = if (!dataModels[position].number.isEmpty()) dataModels[position].number else viewHolder.tvDocNumber.context.getText(R.string.no_description)
        viewHolder.tvDocDateTime.text = if (!dataModels[position].date.isEmpty()) convertDateTime(dataModels[position].date) else viewHolder.tvDocDateTime.context.getText(R.string.no_description)
        if (dataModels[position].comment != null && !dataModels[position].comment?.isEmpty()!!) {
            viewHolder.tvDocComment.visibility = View.VISIBLE
            viewHolder.tvDocComment.text = dataModels[position].comment
        } else {
            viewHolder.tvDocComment.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return dataModels.size
    }

    inner class DocRecountGoodsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvDocNumber: TextView = itemView.tv_doc_number
        var tvDocDateTime: TextView = itemView.tv_doc_date_time
        var tvDocComment: TextView = itemView.tv_doc_comment
    }
}
