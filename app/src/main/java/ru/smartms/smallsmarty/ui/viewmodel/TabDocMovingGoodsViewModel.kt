package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods
import ru.smartms.smallsmarty.db.entity.document.TabDocMovingGoods
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.EXPAND_NOMENCLATURE_CHARACTERISTIC
import java.io.File
import javax.inject.Inject

class TabDocMovingGoodsViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val paths: MutableLiveData<Array<String?>> = MutableLiveData()
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var refKeyDoc: String
    val refKeyDocLiveData: MutableLiveData<String> = MutableLiveData()
    val infRegBarcodeNomenclatureLiveData: MutableLiveData<InfRegBarcodeNomenclature> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docMovingGoodsRepository: DocMovingGoodsRepository

    @Inject
    lateinit var tabDocMovingGoodsRepository: TabDocMovingGoodsRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var catalogCharacteristicRepository: CatalogCharacteristicRepository

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogNomenclatureAllTabDocMovingGoods(): LiveData<List<CatalogNomenclatureAllTabDocMovingGoods>>? {
        return tabDocMovingGoodsRepository.getAll(refKeyDoc)
    }

    fun handleBarcode(barcode: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            var infRegBarcodeNomenclature = infRegBarcodeRepository.get(barcode)
            if (infRegBarcodeNomenclature == null) {
                try {
                    val infRegBarcodeNomenclatures = infRegBarcodeRepository.getFromREST(filter = "Штрихкод eq '$barcode'", expand = EXPAND_NOMENCLATURE_CHARACTERISTIC).await()
                    if (!infRegBarcodeNomenclatures.value.isEmpty()) {
                        catalogNomenclatureRepository.insert(infRegBarcodeNomenclatures.value[0].catalogNomenclature)
                        if (infRegBarcodeNomenclatures.value[0].catalogCharacteristic != null) catalogCharacteristicRepository.insert(infRegBarcodeNomenclatures.value[0].catalogCharacteristic!!)
                        infRegBarcodeRepository.insert(infRegBarcodeNomenclatures.value[0])
                        infRegBarcodeNomenclature = infRegBarcodeNomenclatures.value[0]
                    }
                } catch (e: Exception) {
                    Log.e("Load from REST", e.localizedMessage)
                    isLoadingProgressBar.postValue(false)
                    errorMessageRepository.insertMessage(e.localizedMessage)
                }
            }
            if (infRegBarcodeNomenclature != null) {
                val isScanOneByOne = sharedPreferencesRepository.getScanOneByOne()
                if (isScanOneByOne) {
                    insertTabDocMovingGoods(infRegBarcodeNomenclature, 1.0)
                } else {
                    infRegBarcodeNomenclatureLiveData.postValue(infRegBarcodeNomenclature)
                }
            }
            isLoadingProgressBar.postValue(false)
        }
    }

    fun insertTabDocMovingGoods(infRegBarcodeNomenclature: InfRegBarcodeNomenclature, qty: Double) {
        var tabDocMovingGoods: TabDocMovingGoods? = tabDocMovingGoodsRepository.getTabDocMovingGoodsgetViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc, infRegBarcodeNomenclature.catalogNomenclatureKey)
        if (tabDocMovingGoods != null) {
            tabDocMovingGoods.qty = tabDocMovingGoods.qty + qty
        } else {
            tabDocMovingGoods = TabDocMovingGoods()
            tabDocMovingGoods.catalogNomenclatureKey = infRegBarcodeNomenclature.catalogNomenclatureKey
            tabDocMovingGoods.catalogCharacteristicKey = infRegBarcodeNomenclature.catalogCharacteristicKey
            tabDocMovingGoods.refKeyDoc = refKeyDoc
            tabDocMovingGoods.lineNumber = if (tabDocMovingGoodsRepository.getLastTabDocMovingGoods(refKeyDoc)?.lineNumber != null) tabDocMovingGoodsRepository.getLastTabDocMovingGoods(refKeyDoc)?.lineNumber!! + 1 else 1
            tabDocMovingGoods.qty = qty
            tabDocMovingGoods.pkgQty = qty
        }
        tabDocMovingGoodsRepository.insert(tabDocMovingGoods)
        infRegBarcodeNomenclatureLiveData.postValue(null)
    }

    suspend fun uploadDocMovingGoods() {
        isLoadingProgressBar.postValue(true)
        var docMovingGoods: DocMovingGoods = getDocMovingGoods()
        try {
            if (docMovingGoods.isDownloaded) {
                docMovingGoodsRepository.upload(docMovingGoods).await()
            } else {
                docMovingGoods = docMovingGoodsRepository.uploadNew(docMovingGoods).await()
                docMovingGoods.isEdit = true
                docMovingGoods.isDownloaded = true
                val refKeyDocTemp = refKeyDoc
                refKeyDoc = docMovingGoods.refKey
                refKeyDocLiveData.postValue(refKeyDoc)
                docMovingGoodsRepository.insertDocMovingGoods(docMovingGoods)
                docMovingGoodsRepository.delete(refKeyDocTemp)
                tabDocMovingGoodsRepository.delete(refKeyDocTemp)
            }
            isLoadingProgressBar.postValue(false)
        } catch (e: Throwable) {
            isLoadingProgressBar.postValue(false)
            Log.e("ERROR", e.localizedMessage)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun getDocMovingGoods(): DocMovingGoods {
        val docMovingGoods: DocMovingGoods = docMovingGoodsRepository.get(refKeyDoc)
        val tabDocMovingGoodses: List<TabDocMovingGoods> = tabDocMovingGoodsRepository.getTabDocMovingGoods(refKeyDoc)
        docMovingGoods.goods = tabDocMovingGoodses
        return docMovingGoods
    }

    fun saveToFile(appName: String) {
        isLoadingProgressBar.postValue(true)
        val docMovingGoods: DocMovingGoods = getDocMovingGoods()
        var file: File? = null
        try {
            file = docMovingGoodsRepository.saveToFile(docMovingGoods, appName)
            isLoadingProgressBar.postValue(false)
        } catch (e: Throwable) {
            isLoadingProgressBar.postValue(false)
            Log.e("ERROR", e.localizedMessage)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        if (file != null) {
            paths.postValue(arrayOf(file.absolutePath))
        }
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocMovingGoods() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocMovingGoodsRepository.delete(refKeyDoc)
            docMovingGoodsRepository.delete(refKeyDoc)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun deleteTabDocMovingGoods(id: Long) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            tabDocMovingGoodsRepository.delete(id)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun setIsEditDocMovingGoods(isEdit: Boolean) {
        GlobalScope.launch {
            docMovingGoodsRepository.setIsEditDocMovingGoods(refKeyDoc, isEdit)
        }
    }

    fun insert(docMovingGoods: DocMovingGoods) = docMovingGoodsRepository.insert(docMovingGoods)

    fun isLicenseActive(): Boolean = sharedPreferencesRepository.getIsLicenseActive()
}
