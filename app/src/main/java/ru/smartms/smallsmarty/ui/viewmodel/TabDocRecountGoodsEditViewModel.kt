package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.document.TabDocRecountGoods
import ru.smartms.smallsmarty.utils.NULL_REF
import javax.inject.Inject

class TabDocRecountGoodsEditViewModel : ViewModel() {

    private var id: Long = -1L
    lateinit var tabDocRecountGoods: TabDocRecountGoods
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var refKeyDoc: String

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var tabDocRecountGoodsRepository: TabDocRecountGoodsRepository

    @Inject
    lateinit var catalogCharacteristicRepository: CatalogCharacteristicRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogNomenclatureLiveData(query: String): List<CatalogNomenclature> {
        return catalogNomenclatureRepository.getCatalogNomenclaturesLiveData(query)
    }

    fun getCatalogNomenclatures(): List<CatalogNomenclature>? {
        return catalogNomenclatureRepository.getCatalogNomenclatures()
    }

    fun getCatalogNomenclatureAllTabDocRecountGoods(id: Long): LiveData<CatalogNomenclatureAllTabDocRecountGoods> {
        this.id = id
        GlobalScope.launch {
            tabDocRecountGoods = tabDocRecountGoodsRepository.getTabDocRecountGoods(id)
        }
        return tabDocRecountGoodsRepository.getCatalogNomenclatureAllTabDocRecountGoods(id)
    }

    fun setTabDocRecountGoods() {
        isLoadingProgressBar.postValue(true)
        if (id >= 0) {
            tabDocRecountGoodsRepository.update(tabDocRecountGoods)
        } else {
            tabDocRecountGoodsRepository.insert(tabDocRecountGoods)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun setCatalogNomenclature(catalogNomenclatureKey: String?) {
        if (catalogNomenclatureKey != null) {
            GlobalScope.launch {
                tabDocRecountGoods.catalogNomenclatureKey = catalogNomenclatureKey
                tabDocRecountGoods.catalogNomenclature = catalogNomenclatureRepository.getCatalogNomenclature(catalogNomenclatureKey)
            }
        }
    }

    fun setCatalogCharacteristic(catalogCharacteristicKey: String?) {
        if (catalogCharacteristicKey == null) {
            tabDocRecountGoods.catalogCharacteristicKey = null
            tabDocRecountGoods.catalogCharacteristic = null
        } else {
            GlobalScope.launch {
                tabDocRecountGoods.catalogCharacteristicKey = catalogCharacteristicKey
                tabDocRecountGoods.catalogCharacteristic = catalogCharacteristicRepository.getCatalogCharacteristic(catalogCharacteristicKey)
            }
        }
    }

    fun setQty(qty: Double, accQty: Double) {
        tabDocRecountGoods.qty = qty
        tabDocRecountGoods.qtyPkg = qty
        tabDocRecountGoods.accQty = accQty
        tabDocRecountGoods.actQtyPkg = accQty
    }

    fun createNewTabRecountGoods() {
        tabDocRecountGoods = TabDocRecountGoods()
        tabDocRecountGoods.refKeyDoc = refKeyDoc
        tabDocRecountGoods.lineNumber = if (tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber != null) tabDocRecountGoodsRepository.getLastTabDocRecountGoods(refKeyDoc)?.lineNumber!! + 1 else 1

    }

    fun setTabDocRecountGoods(id: Long) {
        this.id = id
        GlobalScope.launch {
            tabDocRecountGoods = tabDocRecountGoodsRepository.getTabDocRecountGoods(id)
            tabDocRecountGoods.catalogNomenclature = catalogNomenclatureRepository.getCatalogNomenclature(tabDocRecountGoods.catalogNomenclatureKey!!)
            val catalogCharacteristicKey = tabDocRecountGoods.catalogCharacteristicKey
            if (catalogCharacteristicKey != null && !catalogCharacteristicKey.isNullOrEmpty() && catalogCharacteristicKey != NULL_REF) {
                tabDocRecountGoods.catalogCharacteristic = catalogCharacteristicRepository.getCatalogCharacteristic(catalogCharacteristicKey)
            }
        }
    }

    fun getCatalogCharacteristicLiveData(query: String, typeCatalogNomenclatureKey: String?, catalogNomenclatureKey: String?): List<CatalogCharacteristic> {
        return catalogCharacteristicRepository.getCatalogCharacteristicsLiveData(query, typeCatalogNomenclatureKey, catalogNomenclatureKey)
    }
}
