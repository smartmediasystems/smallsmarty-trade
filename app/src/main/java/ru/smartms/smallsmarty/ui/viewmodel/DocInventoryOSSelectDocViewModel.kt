package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOSes
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS
import ru.smartms.smallsmarty.extensions.await
import javax.inject.Inject

class DocInventoryOSSelectDocViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var tabDocInventoryOSRepository: TabDocInventoryOSRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocInventoryOS(): LiveData<List<DocInventoryOS>>? {
        return docInventoryOSRepository.getAll()
    }

    fun loadDocInventoryOSFromRest() = GlobalScope.launch {
        var docInventoryOSes: DocInventoryOSes
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                docInventoryOSes = docInventoryOSRepository.loadFromRest(
                        BuildConfig.DOC_QTY_REQ * i,
                        BuildConfig.DOC_QTY_REQ * (i - 1),
                        null,
                        null).await()
                docInventoryOSRepository.insertAll(docInventoryOSes.value as MutableList<DocInventoryOS>)
            } while (docInventoryOSes.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllDocInventoryOS() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docInventoryOSRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOS(refKey: String) {
        GlobalScope.launch {
            docInventoryOSRepository.delete(refKey)
            tabDocInventoryOSRepository.delete(refKey)
        }
    }

    fun handleRefKey(refKeyOS: String, refKeyDoc: String) {
        val catalogOS = catalogOSRepository.getCatalogOSRefKey(refKeyOS)
        if (catalogOS != null) {
            var tabDocInventoryOS: TabDocInventoryOS? = tabDocInventoryOSRepository.getTabDocInventoryOSgetViaCatalogOSKeyRefKeyDoc(refKeyDoc, catalogOS.refKey)
            if (tabDocInventoryOS != null) {
                tabDocInventoryOS.isAvailabilityAct = true
            } else {
                tabDocInventoryOS = TabDocInventoryOS(catalogOSKey = catalogOS.refKey)
                tabDocInventoryOS.refKeyDoc = refKeyDoc
                tabDocInventoryOS.lineNumber = if (tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber != null) tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber!! + 1 else 1
            }
            tabDocInventoryOSRepository.insert(tabDocInventoryOS)
        }
    }
}
