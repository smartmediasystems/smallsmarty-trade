package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.utils.IS_FOLDER_FILTER
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclatures
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclatures
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import javax.inject.Inject

class CatalogNomenclaturesViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    val isNavigateToPref: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogNomenclatures(): LiveData<List<CatalogNomenclature>>? {
        return catalogNomenclatureRepository.getCatalogNomenclaturesLiveData()
    }

    fun loadCatalogNomenclaturesFromREST() = GlobalScope.launch {
        var catalogNomenclatures: CatalogNomenclatures
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                catalogNomenclatures = catalogNomenclatureRepository.loadFromRest(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogNomenclatureRepository.insertAll(catalogNomenclatures.value)
            } while (catalogNomenclatures.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllCatalogNomenclatures()  {
        isLoadingProgressBar.postValue(true)
        catalogNomenclatureRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun loadAllData() = GlobalScope.launch {
        if (!sharedPreferencesRepository.isAllowDataSync()) {
            isNavigateToPref.postValue(true)
            return@launch
        }
        isLoadingProgressBar.postValue(true)
        catalogNomenclatureRepository.deleteAll()
        infRegBarcodeRepository.deleteAll()
        infRegBarcodeRepository.deleteAll()
        var catalogNomenclatures: CatalogNomenclatures
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                catalogNomenclatures = catalogNomenclatureRepository.loadFromRest(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        IS_FOLDER_FILTER,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogNomenclatureRepository.insertAll(catalogNomenclatures.value)
            } while (catalogNomenclatures.value.isNotEmpty())

            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        var infRegBarcodeNomenclatures: InfRegBarcodeNomenclatures
        i = 0
        try {
            do {
                i++
                infRegBarcodeNomenclatures = infRegBarcodeRepository.getAllFromREST(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        null
                ).await()
                infRegBarcodeRepository.insertAll(infRegBarcodeNomenclatures.value)
            } while (infRegBarcodeNomenclatures.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun deleteAllData() {
        isLoadingProgressBar.postValue(true)
        catalogNomenclatureRepository.deleteAll()
        infRegBarcodeRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteCatalogNomenclature(refKey: String?) {
        catalogNomenclatureRepository.delete(refKey)
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            mDocRecountGoodsRepository.insertAll(mDocRecountGoodsRepository.loadFromFile(path) as ArrayList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOSRepository.insertAll(catalogOSRepository.loadFromFile(path))
            docInventoryOSRepository.insertAll(docInventoryOSRepository.loadFromFile(path) as MutableList<DocInventoryOS>)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun isAllowDataSync(): Boolean  = sharedPreferencesRepository.isAllowDataSync()
}
