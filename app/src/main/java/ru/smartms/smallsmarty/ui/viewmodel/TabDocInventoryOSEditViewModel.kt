package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogOSAllTabDocInventoryOS
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS
import javax.inject.Inject

class TabDocInventoryOSEditViewModel : ViewModel() {

    private var id: Long = -1L
    private lateinit var tabDocInventoryOS: TabDocInventoryOS
    lateinit var refKeyDoc: String
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var tabDocInventoryOSRepository: TabDocInventoryOSRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOSLiveData(query: String): List<CatalogOS> {
        return catalogOSRepository.getCatalogOSesLiveData(query)
    }

    fun getCatalogOSes(): List<CatalogOS>? {
        return catalogOSRepository.getCatalogOSes()
    }

    fun getCatalogOSAllTabDocInventoryOS(id: Long): LiveData<CatalogOSAllTabDocInventoryOS> {
        this.id = id
        GlobalScope.launch {
            tabDocInventoryOS = tabDocInventoryOSRepository.getTabDocInventoryOS(id)
        }
        return tabDocInventoryOSRepository.getCatalogOSAllTabDocInventoryOS(id)
    }

    fun setTabDocInventoryOS() {
        isLoadingProgressBar.postValue(true)
        if (id >= 0) {
            tabDocInventoryOSRepository.update(tabDocInventoryOS)
        } else {
            tabDocInventoryOSRepository.insert(tabDocInventoryOS)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun setCatalogOS(catalogOSKey: String?) {
        this.tabDocInventoryOS.catalogOSKey = catalogOSKey!!
    }

    fun setAct(isAct: Boolean) {
        tabDocInventoryOS.isAvailabilityAct = isAct
    }

    fun createNewTabInventoryOS() {
        tabDocInventoryOS = TabDocInventoryOS(catalogOSKey = null)
        tabDocInventoryOS.refKeyDoc = refKeyDoc
        tabDocInventoryOS.lineNumber = if (tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber != null) tabDocInventoryOSRepository.getLastTabDocInventoryOS(refKeyDoc)?.lineNumber!! + 1 else 1
    }
}
