package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ErrorMessage
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo
import ru.smartms.smallsmarty.utils.SharedPreferenceLiveData
import javax.inject.Inject

class StartActivityViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var licenseInfoRepository: LicenseInfoRepository

    @Inject
    lateinit var sharedPreferencesRepository: SharedPrefRepository

    fun isLicenseActiveLiveData(): SharedPreferenceLiveData<Boolean> {
        return sharedPreferencesRepository.getIsLicenseActiveLiveData()
    }

    fun getErrorMessage(): LiveData<List<ErrorMessage>>? {
        return errorMessageRepository.getErrorMessage()
    }

    fun deleteErrorMessage() {
        GlobalScope.launch {
            errorMessageRepository.deleteAll()
        }
    }

    fun getActivatedLicense(): LiveData<LicenseInfo> {
        return licenseInfoRepository.getActivatedLiveData()
    }
}
