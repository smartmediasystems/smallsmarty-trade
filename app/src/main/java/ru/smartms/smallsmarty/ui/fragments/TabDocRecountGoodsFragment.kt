package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.media.MediaScannerConnection
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods
import ru.smartms.smallsmarty.ui.adapters.TabDocRecountGoodsRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.TabDocRecountGoodsViewModel
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.content.Intent
import android.support.design.widget.Snackbar
import ru.smartms.smallsmarty.utils.REQUEST_WRITE_STORAGE
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.widget.SearchView
import android.text.InputType
import android.widget.EditText
import android.widget.LinearLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods


class TabDocRecountGoodsFragment : BaseFragment(), View.OnClickListener, SearchView.OnQueryTextListener {

    private var data: ArrayList<CatalogNomenclatureAllTabDocRecountGoods> = ArrayList()

    private lateinit var viewModel: TabDocRecountGoodsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TabDocRecountGoodsViewModel::class.java)
        val refKeyDoc: String = arguments?.getString("refKeyDoc", "")!!
        viewModel.refKeyDoc = refKeyDoc
        viewModel.getCatalogNomenclatureAllTabDocRecountGoods(refKeyDoc)?.observe(this, Observer { catalogNomenclatureAllTabDocRecountGoodses ->
            viewModel.setIsEditDocRecountGoods(true)
            (recyclerView.adapter as TabDocRecountGoodsRVAdapter).updateTabDocRecountGoods(catalogNomenclatureAllTabDocRecountGoodses!!)
            if (catalogNomenclatureAllTabDocRecountGoodses.isNotEmpty()) recyclerView.scrollToPosition((recyclerView.adapter as TabDocRecountGoodsRVAdapter).scrollPosition)
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.handleBarcode(scanData[0].barcode)
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.paths.observe(this, Observer { paths ->
            MediaScannerConnection.scanFile(context, paths, null) { path, _ ->
                Log.d("SCAN File", path)
                Toast.makeText(context, "${getString(R.string.document_saved)} $path", Toast.LENGTH_LONG).show()
            }
            Toast.makeText(context, "${getString(R.string.document_saved)} ${paths?.get(0)}", Toast.LENGTH_LONG).show()
        })
        viewModel.refKeyOS.observe(this, Observer { refKeyOS ->
            if (refKeyOS != null) {
                Snackbar.make(coordinatorLayout, R.string.you_scanned_os,
                        Snackbar.LENGTH_LONG)
                        .setAction(android.R.string.ok) {
                            val bundle = Bundle()
                            bundle.putString("refKeyOS", refKeyOS)
                            findNavController().navigate(R.id.action_tabDocRecountGoodsFragment_to_docRecountOSSelectDocFragment, bundle)
                        }
                        .show()
                viewModel.refKeyOS.postValue(null)
            }
        })
        viewModel.infRegBarcodeNomenclatureLiveData.observe(this, Observer { infRegBarcodeNomenclature ->
            if (infRegBarcodeNomenclature != null) {
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle(getString(R.string.enter_qty))
                val input = EditText(context)
                input.inputType = InputType.TYPE_CLASS_NUMBER
                input.setText("1")
                input.setSelectAllOnFocus(true)
                builder.setView(input)
                builder.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                    val qty = java.lang.Double.parseDouble(input.text.toString())
                    GlobalScope.launch {
                        viewModel.insertTabDocRecountGoods(infRegBarcodeNomenclature, qty)
                    }
                }
                builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, _ -> dialog.cancel() }
                builder.show()
            }
        })
        viewModel.catalogSeriesLiveData.observe(this, Observer { catalogSeriesLiveData ->
            if (catalogSeriesLiveData != null) {
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle(getString(R.string.enter_qty))
                val input = EditText(context)
                input.inputType = InputType.TYPE_CLASS_NUMBER
                input.setText("1")
                input.setSelectAllOnFocus(true)
                builder.setView(input)
                builder.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                    val qty = java.lang.Double.parseDouble(input.text.toString())
                    GlobalScope.launch {
                        viewModel.insertTabDocRecountGoods(catalogSeriesLiveData, qty)
                    }
                }
                builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, _ -> dialog.cancel() }
                builder.show()
            }
        })
        viewModel.isNavigateToPref.observe(this, Observer { isNavigateToPref ->
            if (isNavigateToPref == true) {
                showSnackbarAndNavigateToPref(R.id.action_tabDocRecountGoodsFragment_to_prefsMainFragment)
                viewModel.isNavigateToPref.postValue(false)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(TabDocRecountGoodsRVAdapter(data, this))
        recyclerView.adapter?.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                recyclerView.scrollToPosition(positionStart)
            }

        })
        fab?.setOnClickListener(this)
        fab?.show()
        iBtnSearchBarcode.setOnClickListener(this)
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_tab_doc_recount_goods_fragment, menu)
        val menuItemSearch = menu?.findItem(R.id.action_search)
        val searchView = menuItemSearch?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteTabDocRecountGoods(data[position].id)
            Toast.makeText(context, getString(R.string.item_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.item_will_be_remove), position)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigateUp()
            R.id.upload_doc_recount_goods -> {
                viewModel.uploadDocRecountGoods()
                if (viewModel.isAllowDataSync()) {
                    if (viewModel.isLicenseActive()) {
                        viewModel.uploadDocRecountGoods()
                    } else {
                        findNavController().navigate(R.id.action_tabDocRecountGoodsFragment_to_purchasesFragment)
                    }
                } else {
                    showSnackbarAndNavigateToPref(R.id.action_tabDocRecountGoodsFragment_to_prefsMainFragment)
                }
            }
            R.id.delete_doc_recount_goods -> viewModel.deleteDocRecountGoods()
            R.id.action_save_to_file -> {
                if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestWriteStoragePermission()
                } else {
                    GlobalScope.launch { viewModel.saveToFile(getString(R.string.app_name)) }
                }
            }
            R.id.action_enter_barcode -> {
                item.isChecked = !item.isChecked
                if (constraintLayout.visibility == View.GONE) constraintLayout.visibility = View.VISIBLE else constraintLayout.visibility = View.GONE
            }
            R.id.action_scan_barcode_from_camera -> initScanBarcode()
            R.id.action_add_comment -> {
                GlobalScope.launch(Dispatchers.Main) {
                    val input = EditText(context)
                    var docRecountGoods: DocRecountGoods? = null
                    withContext(Dispatchers.Default) {
                        docRecountGoods = viewModel.getDocRecountGoods()
                    }
                    input.setText(docRecountGoods?.comment)
                    val lp = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT)
                    val alertDialog = AlertDialog.Builder(context!!)
                            .setTitle(R.string.comment)
                            .setPositiveButton(android.R.string.ok) { _, _ ->
                                docRecountGoods?.comment = input.text.toString()
                                launch {
                                    docRecountGoods?.let { viewModel.insert(it) }
                                }
                            }
                            .setNegativeButton(android.R.string.cancel) { _, _ -> }
                    input.layoutParams = lp
                    alertDialog.setView(input)
                    alertDialog.show()
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tab_doc_recount_goods_item -> {
                val bundle = Bundle()
                bundle.putLong("id", data[recyclerView.getChildAdapterPosition(v)].id)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocRecountGoodsFragment_to_tabDocRecountGoodsEditFragment, bundle)
            }
            R.id.fab -> {
                val bundle = Bundle()
                bundle.putLong("id", -1L)
                bundle.putString("refKeyDoc", viewModel.refKeyDoc)
                findNavController().navigate(R.id.action_tabDocRecountGoodsFragment_to_tabDocRecountGoodsEditFragment, bundle)
            }
            R.id.ibtn_enter_barcode -> {
                val query = etSearch.text.toString()
                etSearch.setText("")
                if (query.isNotEmpty()) {
                    viewModel.handleBarcode(query)
                }
            }
        }
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        if (!query.isEmpty()) {
            viewModel.handleBarcode(query)
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText != null && newText.contains("\n")) {
            viewModel.handleBarcode(newText.replace("\n", ""))
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_WRITE_STORAGE) {
            viewModel.saveToFile(getString(R.string.app_name))
        }
    }

    override fun handleScan(query: String) {
        viewModel.handleBarcode(query)
    }
}
