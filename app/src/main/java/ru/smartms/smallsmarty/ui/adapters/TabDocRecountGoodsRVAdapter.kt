package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.tab_doc_recount_goods_item.view.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback
import ru.smartms.smallsmarty.data.TabDocRecountGoodsDiffUtil


class TabDocRecountGoodsRVAdapter(private val dataModels: ArrayList<CatalogNomenclatureAllTabDocRecountGoods>, private val listener: View.OnClickListener) : BaseRecyclerViewAdapter(), ListUpdateCallback {

    var scrollPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tab_doc_recount_goods_item, parent, false)
        view.setOnClickListener(listener)
        return TabDocRecountGoodsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as TabDocRecountGoodsViewHolder
        viewHolder.tvCatalogNomenclature.text = if (!dataModels[position].catalogNomenclatureDescription.isNullOrEmpty()) dataModels[position].catalogNomenclatureDescription else viewHolder.tvCatalogNomenclature.context.getText(R.string.no_description)
        viewHolder.tvAccQty.text = dataModels[position].accQty.toString()
        viewHolder.tvQty.text = dataModels[position].qty.toString()
        if (!dataModels[position].catalogCharacteristicDescription.isNullOrEmpty()) {
            viewHolder.tvCatalogCharacteristic.text = dataModels[position].catalogCharacteristicDescription
            viewHolder.tvCatalogCharacteristic.visibility = View.VISIBLE
        } else {
            viewHolder.tvCatalogCharacteristic.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return dataModels.size
    }

    inner class TabDocRecountGoodsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCatalogNomenclature: TextView = itemView.tv_recount_goods_catalog_nomenclature
        var tvCatalogCharacteristic: TextView = itemView.tv_recount_goods_catalog_characteristic
        var tvAccQty: TextView = itemView.tv_recount_goods_acc_qty
        var tvQty: TextView = itemView.tv_recount_goods_qty
    }

    fun updateTabDocRecountGoods(catalogNomenclatureAllTabDocRecountGoods: List<CatalogNomenclatureAllTabDocRecountGoods>) {
        val diffCallback = TabDocRecountGoodsDiffUtil(this.dataModels, catalogNomenclatureAllTabDocRecountGoods)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.dataModels.clear()
        this.dataModels.addAll(catalogNomenclatureAllTabDocRecountGoods)
        diffResult.dispatchUpdatesTo(this as ListUpdateCallback)
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        scrollPosition = position
        this.notifyItemRangeChanged(position, count, payload)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        this.notifyItemMoved(fromPosition, toPosition)
    }

    override fun onInserted(position: Int, count: Int) {
        scrollPosition = position
        this.notifyItemRangeInserted(position, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        this.notifyItemRangeRemoved(position, count)
    }
}
