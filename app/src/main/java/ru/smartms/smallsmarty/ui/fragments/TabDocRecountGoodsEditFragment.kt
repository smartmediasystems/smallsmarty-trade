package ru.smartms.smallsmarty.ui.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.tab_doc_recount_goods_item_edit.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.ui.viewmodel.TabDocRecountGoodsEditViewModel

class TabDocRecountGoodsEditFragment : BaseFragment(), View.OnClickListener {

    private var job: Job = Job()
    private lateinit var viewModel: TabDocRecountGoodsEditViewModel
    private lateinit var catalogNomenclatureAutoCompleteTextView: AutoCompleteTextView
    private lateinit var catalogCharacteristicAutoCompleteTextView: AutoCompleteTextView
    private lateinit var catalogSeriesAutoCompleteTextView: AutoCompleteTextView
    private lateinit var catalogNomenclatureQtyEditText: EditText
    private lateinit var catalogNomenclatureAccQtyEditText: EditText
    private lateinit var ivBtnClearCatalogNomenclature: ImageView
    private lateinit var ivBtnClearCatalogCharacteristic: ImageView
    private lateinit var ivBtnClearCatalogSeries: ImageView
    private lateinit var btnOk: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TabDocRecountGoodsEditViewModel::class.java)
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            if (isLoadingProgressBar!!) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
        val refKeyDocTemp = arguments?.getString("refKeyDoc", "")
        if (refKeyDocTemp != null) {
            viewModel.refKeyDoc = refKeyDocTemp
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        val view = inflater.inflate(R.layout.tab_doc_recount_goods_item_edit, container, false)
        catalogNomenclatureAutoCompleteTextView = view.autocomplete_tv_recount_goods_edit_catalog_nomenclature
        catalogCharacteristicAutoCompleteTextView = view.autocomplete_tv_recount_goods_edit_catalog_characteristic
        catalogSeriesAutoCompleteTextView = view.autocomplete_tv_recount_goods_edit_catalog_series
        catalogNomenclatureQtyEditText = view.et_recount_goods_edit_qty
        catalogNomenclatureAccQtyEditText = view.et_recount_goods_edit_acc_qty
        ivBtnClearCatalogNomenclature = view.iv_clear_btn_recount_goods_edit_catalog_nomenclature
        ivBtnClearCatalogCharacteristic = view.iv_clear_btn_recount_goods_edit_catalog_characteristic
        ivBtnClearCatalogSeries = view.iv_clear_btn_recount_goods_edit_catalog_series
        btnOk = view.btn_recount_goods_edit_ok
        return view
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        catalogNomenclatureAutoCompleteTextView.setAdapter(CatalogNomenclatureAutoCompleteAdapter(ArrayList()))
        val id = arguments?.getLong("id", -1L)
        if (id!! >= 0) {
            viewModel.setTabDocRecountGoods(id)
            setObserverViewModelItem(id)
        } else {
            GlobalScope.launch {
                viewModel.createNewTabRecountGoods()
            }
        }
        catalogNomenclatureAutoCompleteTextView.setOnTouchListener { _, _ ->
            catalogNomenclatureAutoCompleteTextView.showDropDown()
            false
        }
        catalogNomenclatureAutoCompleteTextView.setOnItemClickListener { adapterView, _, _, idAc ->
            val catalogNomenclature = adapterView.adapter.getItem(idAc.toInt()) as CatalogNomenclature
            viewModel.setCatalogNomenclature(catalogNomenclature.refKey)
            viewModel.setCatalogCharacteristic(null)
            (catalogCharacteristicAutoCompleteTextView.adapter as CatalogCharacteristicAutoCompleteAdapter).result.clear()
            catalogCharacteristicAutoCompleteTextView.setText("")
            catalogSeriesAutoCompleteTextView.setText("")
        }
        catalogCharacteristicAutoCompleteTextView.setAdapter(CatalogCharacteristicAutoCompleteAdapter(ArrayList()))
        catalogCharacteristicAutoCompleteTextView.setOnTouchListener { _, _ ->
            catalogCharacteristicAutoCompleteTextView.showDropDown()
            false
        }
        catalogCharacteristicAutoCompleteTextView.setOnItemClickListener { adapterView, _, _, idAc ->
            val catalogCharacteristic = adapterView.adapter.getItem(idAc.toInt()) as CatalogCharacteristic
            viewModel.setCatalogCharacteristic(catalogCharacteristic.refKey)
        }
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        catalogNomenclatureQtyEditText.requestFocus()
        catalogNomenclatureQtyEditText.setSelectAllOnFocus(true)
        catalogNomenclatureQtyEditText.selectAll()
        btnOk.setOnClickListener(this)
        ivBtnClearCatalogNomenclature.setOnClickListener(this)
        ivBtnClearCatalogCharacteristic.setOnClickListener(this)
        ivBtnClearCatalogSeries.setOnClickListener(this)
        fab?.hide()
        view
    }

    private fun setObserverViewModelItem(id: Long) {
        viewModel.getCatalogNomenclatureAllTabDocRecountGoods(id).observe(this, Observer { catalogNomenclatureAllTabDocRecountGoods ->
            if (catalogNomenclatureAllTabDocRecountGoods != null) {
                catalogNomenclatureAutoCompleteTextView.setText(catalogNomenclatureAllTabDocRecountGoods.catalogNomenclatureDescription)
                catalogCharacteristicAutoCompleteTextView.setText(catalogNomenclatureAllTabDocRecountGoods.catalogCharacteristicDescription)
                catalogNomenclatureQtyEditText.setText(catalogNomenclatureAllTabDocRecountGoods.qty.toString())
                catalogNomenclatureAccQtyEditText.setText(catalogNomenclatureAllTabDocRecountGoods.accQty.toString())
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_doc_moving_goods_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_recount_goods_edit_ok -> {
                if (catalogNomenclatureAutoCompleteTextView.text.isEmpty()) {
                    Toast.makeText(context, R.string.select_nomenclature, Toast.LENGTH_LONG).show()
                    return
                }
                if (catalogNomenclatureQtyEditText.text.isEmpty()) {
                    Toast.makeText(context, R.string.enter_qty, Toast.LENGTH_LONG).show()
                    return
                }
                GlobalScope.launch(Dispatchers.Main) {
                    withContext(Dispatchers.Default) {
                        viewModel.setQty(catalogNomenclatureQtyEditText.text.toString().toDouble(), catalogNomenclatureAccQtyEditText.text.toString().toDouble())
                        viewModel.setTabDocRecountGoods()
                    }
                    findNavController().popBackStack()
                }
            }
            R.id.iv_clear_btn_recount_goods_edit_catalog_nomenclature -> {
                catalogNomenclatureAutoCompleteTextView.setText("")
                viewModel.setCatalogCharacteristic(null)
                (catalogCharacteristicAutoCompleteTextView.adapter as CatalogCharacteristicAutoCompleteAdapter).result.clear()
                catalogCharacteristicAutoCompleteTextView.setText("")
            }
            R.id.iv_clear_btn_recount_goods_edit_catalog_characteristic -> {
                catalogCharacteristicAutoCompleteTextView.setText("")
                viewModel.setCatalogCharacteristic(null)
                (catalogCharacteristicAutoCompleteTextView.adapter as CatalogCharacteristicAutoCompleteAdapter).result.clear()
                catalogCharacteristicAutoCompleteTextView.setText("")
            }
            R.id.iv_clear_btn_recount_goods_edit_catalog_series -> catalogSeriesAutoCompleteTextView.setText("")
        }
    }

    inner class CatalogNomenclatureAutoCompleteAdapter(val result: ArrayList<CatalogNomenclature>) : BaseAdapter(), Filterable {

        override fun getCount(): Int {
            return result.size
        }

        override fun getItem(index: Int): CatalogNomenclature {
            return result[index]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, cV: View?, parent: ViewGroup): View {
            var convertView = cV
            if (convertView == null) {
                val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.custom_catalog_nomenclature_autocomplete_adapter_item, parent, false)
            }
            (convertView!!.findViewById(R.id.tv_item) as TextView).text = getItem(position).description
            return convertView
        }

        override fun getFilter(): Filter? {
            return object : Filter() {
                override fun convertResultToString(resultValue: Any): CharSequence? {
                    return (resultValue as CatalogNomenclature).description
                }

                override fun performFiltering(constraint: CharSequence): FilterResults? {
                    return null
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    if (constraint != null) {
                        job.cancel()
                        job = Job()
                        GlobalScope.launch(Dispatchers.Main + job) {
                            progressBar.visibility = View.VISIBLE
                            withContext(Dispatchers.Default) {
                                result.clear()
                                result.addAll(if (!constraint.isEmpty()) viewModel.getCatalogNomenclatureLiveData("%$constraint%") else viewModel.getCatalogNomenclatures()!!)
                            }
                            notifyDataSetChanged()
                            progressBar.visibility = View.GONE
                        }
                    } else {
                        notifyDataSetInvalidated()
                    }
                }
            }
        }
    }

    inner class CatalogCharacteristicAutoCompleteAdapter(val result: ArrayList<CatalogCharacteristic>) : BaseAdapter(), Filterable {

        override fun getCount(): Int {
            return result.size
        }

        override fun getItem(index: Int): CatalogCharacteristic {
            return result[index]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, cV: View?, parent: ViewGroup): View {
            var convertView = cV
            if (convertView == null) {
                val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(R.layout.custom_catalog_characteristic_autocomplete_adapter_item, parent, false)
            }
            (convertView!!.findViewById(R.id.tv_item) as TextView).text = getItem(position).description
            return convertView
        }

        override fun getFilter(): Filter? {
            return object : Filter() {
                override fun convertResultToString(resultValue: Any): CharSequence? {
                    return (resultValue as CatalogCharacteristic).description
                }

                override fun performFiltering(constraint: CharSequence): FilterResults? {
                    return null
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    if (constraint != null) {
                        job.cancel()
                        job = Job()
                        GlobalScope.launch(Dispatchers.Main + job) {
                            progressBar.visibility = View.VISIBLE
                            withContext(Dispatchers.Default) {
                                result.clear()
                                if (!viewModel.tabDocRecountGoods.catalogNomenclatureKey.isNullOrEmpty() || viewModel.tabDocRecountGoods.catalogNomenclature?.typeNomenclature != null) {
                                    result.addAll( viewModel.getCatalogCharacteristicLiveData("%$constraint%", viewModel.tabDocRecountGoods.catalogNomenclature?.typeNomenclature, viewModel.tabDocRecountGoods.catalogNomenclatureKey) )
                                }
                            }
                            notifyDataSetChanged()
                            progressBar.visibility = View.GONE
                        }
                    } else {
                        notifyDataSetInvalidated()
                    }
                }
            }
        }
    }
}
