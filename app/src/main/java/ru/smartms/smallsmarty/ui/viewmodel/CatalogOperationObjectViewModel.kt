package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObjects
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.ORDER_BY_CATALOG
import javax.inject.Inject

class CatalogOperationObjectViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository

    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var docInventoryOperationObjectRepository: DocInventoryOperationObjectRepository

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOperationObjects(): LiveData<List<CatalogOperationObject>>? {
        return catalogOperationObjectRepository.getCatalogOperationObjectsLiveData()
    }

    fun loadCatalogOperationObjectsFromREST() = GlobalScope.launch {
        var catalogOperationObjects: CatalogOperationObjects
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                catalogOperationObjects = catalogOperationObjectRepository.loadFromRest(
                        BuildConfig.QTY_REQ * i,
                        BuildConfig.QTY_REQ * (i - 1),
                        null,
                        null,
                        null,
                        ORDER_BY_CATALOG).await()
                catalogOperationObjectRepository.insertAll(catalogOperationObjects.value)
            } while (catalogOperationObjects.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllCatalogOperationObjects()  = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        catalogOperationObjectRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteCatalogOperationObject(refKey: String) {
        catalogOperationObjectRepository.delete(refKey)
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            mDocRecountGoodsRepository.insertAll(mDocRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOperationObjectRepository.insertAll(catalogOperationObjectRepository.loadFromFile(path))
            docInventoryOperationObjectRepository.insertAll(docInventoryOperationObjectRepository.loadFromFile(path) as MutableList<DocInventoryOperationObject>)
            isLoadingProgressBar.postValue(false)
        }
    }
}
