package ru.smartms.smallsmarty.ui.adapters

import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.catalog_nomenclature_item.view.*
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature


class CatalogNomenclaturesRVAdapter(private val dataModels: ArrayList<CatalogNomenclature>) : BaseRecyclerViewAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.catalog_nomenclature_item, parent, false)
        return CatalogNomenclaturesViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as CatalogNomenclaturesViewHolder
        viewHolder.description.text = if (!dataModels[position].description.isNullOrEmpty()) dataModels[position].description else viewHolder.description.context.getText(R.string.no_description)
    }

    override fun getItemCount(): Int {
        return dataModels.size
    }

    inner class CatalogNomenclaturesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var description: TextView = itemView.tv_description
    }
}
