package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.ui.adapters.DocRecountGoodsRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.DocRecountGoodsViewModel
import com.obsez.android.lib.filechooser.ChooserDialog
import android.content.pm.PackageManager
import android.os.Environment
import android.support.v4.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DocRecountGoodsFragment : BaseFragment(), View.OnClickListener {

    var data: ArrayList<DocRecountGoods> = ArrayList()

    private lateinit var viewModel: DocRecountGoodsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(activity!!).get(DocRecountGoodsViewModel::class.java)
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
        viewModel.getDocRecountGoods()?.observe(this, Observer { docRecountGoodses ->
            data.clear()
            data.addAll(docRecountGoodses!!)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(DocRecountGoodsRVAdapter(data, this))
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteDocRecountGoods(data[position].refKey)
            Toast.makeText(context, getString(R.string.document_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.doc_will_be_remove), position)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_doc_recount_goods_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.load_doc_recount_goods -> {
                GlobalScope.launch(Dispatchers.Main) {
                    if (viewModel.isLicenseActive()) {
                        withContext(Dispatchers.Default) {
                            viewModel.loadDocRecountGoodsFromRest()
                        }
                    } else {
                        findNavController().navigate(R.id.action_docRecountGoodsFragment_to_purchasesFragment)
                    }
                }

            }
            R.id.delete_all_doc_recount_goods -> viewModel.deleteAllDocRecountGoods()
            R.id.load_from_file_doc_recount_goods -> {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestReadStoragePermission()
                } else {
                    ChooserDialog(context)
                            .withFilter(false, false, "json", "txt")
                            .withStartFile("${Environment.getExternalStorageDirectory()}")
                            .withChosenListener { path, _ -> viewModel.loadFromFile(path) }
                            .build()
                            .show()
                }
            }
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.doc_recount_goods_item -> {
                val bundle = Bundle()
                bundle.putString("refKeyDoc", data[recyclerView.getChildAdapterPosition(v)].refKey)
                findNavController().navigate(R.id.action_docRecountGoodsFragment_to_tabDocRecountGoodsFragment, bundle)
            }
            R.id.fab -> viewModel.createDocRecountGoods()
        }
    }
}
