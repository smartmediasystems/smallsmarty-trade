package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObjects
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.getCurrentDateTime
import javax.inject.Inject

class DocInventoryOperationObjectViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOperationObjectRepository: DocInventoryOperationObjectRepository

    @Inject
    lateinit var tabDocInventoryOperationObjectRepository: TabDocInventoryOperationObjectRepository


    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocInventoryOperationObject(): LiveData<List<DocInventoryOperationObject>>? {
        return docInventoryOperationObjectRepository.getAll()
    }

    fun loadDocInventoryOperationObjectFromRest() = GlobalScope.launch {
        var docInventoryOperationObjects: DocInventoryOperationObjects
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                docInventoryOperationObjects = docInventoryOperationObjectRepository.loadFromRest(
//                        "Ref_Key, Date, Number, DeletionMark, Организация_Key, Комментарий",
                        BuildConfig.DOC_QTY_REQ * i,
                        BuildConfig.DOC_QTY_REQ * (i - 1),
                        null,
                        null).await()
                docInventoryOperationObjectRepository.insertAll(docInventoryOperationObjects.value as MutableList<DocInventoryOperationObject>)
            } while (docInventoryOperationObjects.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllDocInventoryOperationObject() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docInventoryOperationObjectRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOperationObject(refKey: String) {
        GlobalScope.launch {
            docInventoryOperationObjectRepository.delete(refKey)
            tabDocInventoryOperationObjectRepository.delete(refKey)
        }
    }

    fun createDocInventoryOperationObject() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val id = if (docInventoryOperationObjectRepository.getLastDocInventoryOperationObjectOrderById()?.id is Long) docInventoryOperationObjectRepository.getLastDocInventoryOperationObjectOrderById()?.id!! + 1L else 1L
            val docInventoryOperationObject = DocInventoryOperationObject()
            docInventoryOperationObject.id = id
            docInventoryOperationObject.refKey = id.toString()
            docInventoryOperationObject.number = id.toString()
            docInventoryOperationObject.date = getCurrentDateTime()
            docInventoryOperationObjectRepository.insert(docInventoryOperationObject)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            mDocRecountGoodsRepository.insertAll(mDocRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOperationObjectRepository.insertAll(catalogOperationObjectRepository.loadFromFile(path))
            docInventoryOperationObjectRepository.insertAll(docInventoryOperationObjectRepository.loadFromFile(path) as MutableList<DocInventoryOperationObject>)
            isLoadingProgressBar.postValue(false)
        }
    }

}
