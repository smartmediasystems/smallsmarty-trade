package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.CatalogOperationObjectAllTabDocInventoryOperationObject
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOperationObject
import javax.inject.Inject

class TabDocInventoryOperationObjectEditViewModel : ViewModel() {

    private var id: Long = -1L
    private lateinit var tabDocInventoryOperationObject: TabDocInventoryOperationObject
    lateinit var refKeyDoc: String
    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var catalogOperationObjectRepository: CatalogOperationObjectRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOperationObjectRepository: DocInventoryOperationObjectRepository

    @Inject
    lateinit var tabDocInventoryOperationObjectRepository: TabDocInventoryOperationObjectRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getCatalogOperationObjectLiveData(query: String): List<CatalogOperationObject> {
        return catalogOperationObjectRepository.getCatalogOperationObjectsLiveData(query)
    }

    fun getCatalogOperationObjects(): List<CatalogOperationObject>? {
        return catalogOperationObjectRepository.getCatalogOperationObjects()
    }

    fun getCatalogOperationObjectAllTabDocInventoryOperationObject(id: Long): LiveData<CatalogOperationObjectAllTabDocInventoryOperationObject> {
        this.id = id
        GlobalScope.launch {
            tabDocInventoryOperationObject = tabDocInventoryOperationObjectRepository.getTabDocInventoryOperationObject(id)
        }
        return tabDocInventoryOperationObjectRepository.getCatalogOperationObjectAllTabDocInventoryOperationObject(id)
    }

    fun setTabDocInventoryOperationObject() {
        isLoadingProgressBar.postValue(true)
        if (id >= 0) {
            tabDocInventoryOperationObjectRepository.update(tabDocInventoryOperationObject)
        } else {
            tabDocInventoryOperationObjectRepository.insert(tabDocInventoryOperationObject)
        }
        isLoadingProgressBar.postValue(false)
    }

    fun setCatalogOperationObject(catalogOperationObjectKey: String?) {
        this.tabDocInventoryOperationObject.catalogOperationObjectKey = catalogOperationObjectKey!!
    }

    fun setAct(isAct: Boolean) {
        tabDocInventoryOperationObject.isAvailabilityAct = isAct
    }

    fun createNewTabInventoryOperationObject() {
        tabDocInventoryOperationObject = TabDocInventoryOperationObject(catalogOperationObjectKey = null)
        tabDocInventoryOperationObject.refKeyDoc = refKeyDoc
        tabDocInventoryOperationObject.lineNumber = if (tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber != null) tabDocInventoryOperationObjectRepository.getLastTabDocInventoryOperationObject(refKeyDoc)?.lineNumber!! + 1 else 1
    }
}
