package ru.smartms.smallsmarty.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.BuildConfig
import ru.smartms.smallsmarty.data.source.*
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOSes
import ru.smartms.smallsmarty.extensions.await
import ru.smartms.smallsmarty.utils.getCurrentDateTime
import javax.inject.Inject

class DocInventoryOSViewModel : ViewModel() {

    init {
        App.component.inject(this)
    }

    val isLoadingProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    @Inject
    lateinit var catalogNomenclatureRepository: CatalogNomenclatureRepository

    @Inject
    lateinit var infRegBarcodeRepository: InfRegBarcodeRepository

    @Inject
    lateinit var scanDataRepository: ScanDataRepository

    @Inject
    lateinit var docInventoryOSRepository: DocInventoryOSRepository

    @Inject
    lateinit var tabDocInventoryOSRepository: TabDocInventoryOSRepository


    @Inject
    lateinit var mDocRecountGoodsRepository: DocRecountGoodsRepository

    @Inject
    lateinit var catalogOSRepository: CatalogOSRepository

    @Inject
    lateinit var errorMessageRepository: ErrorMessageRepository


    fun getScanData(): LiveData<List<ScanData>>? {
        return scanDataRepository.getScanData()
    }

    fun deleteScanData() {
        scanDataRepository.deleteAll()
    }

    fun getDocInventoryOS(): LiveData<List<DocInventoryOS>>? {
        return docInventoryOSRepository.getAll()
    }

    fun loadDocInventoryOSFromRest() = GlobalScope.launch {
        var docInventoryOSes: DocInventoryOSes
        var i = 0
        try {
            isLoadingProgressBar.postValue(true)
            do {
                i++
                docInventoryOSes = docInventoryOSRepository.loadFromRest(
//                        "Ref_Key, Date, Number, DeletionMark, Организация_Key, Комментарий",
                        BuildConfig.DOC_QTY_REQ * i,
                        BuildConfig.DOC_QTY_REQ * (i - 1),
                        null,
                        null).await()
                docInventoryOSRepository.insertAll(docInventoryOSes.value as MutableList<DocInventoryOS>)
            } while (docInventoryOSes.value.isNotEmpty())
            isLoadingProgressBar.postValue(false)
        } catch (e: Exception) {
            Log.e("ERROR", e.localizedMessage)
            isLoadingProgressBar.postValue(false)
            errorMessageRepository.insertMessage(e.localizedMessage)
        }
    }

    fun deleteAllDocInventoryOS() = GlobalScope.launch {
        isLoadingProgressBar.postValue(true)
        docInventoryOSRepository.deleteAll()
        isLoadingProgressBar.postValue(false)
    }

    fun deleteDocInventoryOS(refKey: String) {
        GlobalScope.launch {
            docInventoryOSRepository.delete(refKey)
            tabDocInventoryOSRepository.delete(refKey)
        }
    }

    fun createDocInventoryOS() {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            val id = if (docInventoryOSRepository.getLastDocInventoryOSOrderById()?.id is Long) docInventoryOSRepository.getLastDocInventoryOSOrderById()?.id!! + 1L else 1L
            val docInventoryOS = DocInventoryOS()
            docInventoryOS.id = id
            docInventoryOS.refKey = id.toString()
            docInventoryOS.number = id.toString()
            docInventoryOS.date = getCurrentDateTime()
            docInventoryOSRepository.insert(docInventoryOS)
            isLoadingProgressBar.postValue(false)
        }
    }

    fun loadFromFile(path: String) {
        GlobalScope.launch {
            isLoadingProgressBar.postValue(true)
            mDocRecountGoodsRepository.insertAll(mDocRecountGoodsRepository.loadFromFile(path) as MutableList<DocRecountGoods>)
            catalogNomenclatureRepository.insertAll(catalogNomenclatureRepository.loadFromFile(path))
            infRegBarcodeRepository.insertAll(infRegBarcodeRepository.loadFromFile(path))
            catalogOSRepository.insertAll(catalogOSRepository.loadFromFile(path))
            docInventoryOSRepository.insertAll(docInventoryOSRepository.loadFromFile(path) as MutableList<DocInventoryOS>)
            isLoadingProgressBar.postValue(false)
        }
    }

}
