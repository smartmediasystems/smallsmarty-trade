package ru.smartms.smallsmarty.ui.fragments

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.obsez.android.lib.filechooser.ChooserDialog
import ru.smartms.smallsmarty.App
import ru.smartms.smallsmarty.R
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS
import ru.smartms.smallsmarty.ui.adapters.DocInventoryOSRVAdapter
import ru.smartms.smallsmarty.ui.viewmodel.DocInventoryOSViewModel


class DocInventoryOSFragment : BaseFragment(), View.OnClickListener {

    var data: ArrayList<DocInventoryOS> = ArrayList()

    private lateinit var viewModel: DocInventoryOSViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = ViewModelProviders.of(this).get(DocInventoryOSViewModel::class.java)
        viewModel.getDocInventoryOS()?.observe(this, Observer { docInventoryOSes ->
            data.clear()
            data.addAll(docInventoryOSes!!)
            recyclerView.adapter?.notifyDataSetChanged()
        })
        viewModel.isLoadingProgressBar.observe(this, Observer { isLoadingProgressBar ->
            showHideProgressBar(isLoadingProgressBar)
        })
        viewModel.getScanData()?.observe(this, Observer { scanData ->
            if (scanData != null) {
                if (!scanData.isEmpty()) {
                    viewModel.deleteScanData()
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        App.component.inject(this)
        return inflater.inflate(R.layout.rv_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
        setupRecyclerView(DocInventoryOSRVAdapter(data, this))
        fab?.setOnClickListener(null)
        fab?.hide()
        view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_doc_inventory_os_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(Gravity.START)
            R.id.load_doc_inventory_os -> viewModel.loadDocInventoryOSFromRest()
            R.id.delete_all_doc_inventory_os -> viewModel.deleteAllDocInventoryOS()
            R.id.action_load_data_from_file -> {
                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestReadStoragePermission()
                } else {
                    ChooserDialog(context)
                            .withFilter(false, false, "json", "txt")
                            .withStartFile("${Environment.getExternalStorageDirectory()}")
                            .withChosenListener { path, _ -> viewModel.loadFromFile(path) }
                            .build()
                            .show()
                }
            }
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        progressBar.visibility = View.GONE
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.doc_inventory_os_item -> {
                val bundle = Bundle()
                bundle.putString("refKeyDoc", data[recyclerView.getChildAdapterPosition(v)].refKey)
                findNavController().navigate(R.id.action_docInventoryOSFragment_to_tabDocInventoryOSFragment, bundle)
            }
            R.id.fab -> viewModel.createDocInventoryOS()
        }
    }

    override fun onSwipe(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position: Int = recyclerView.getChildAdapterPosition(viewHolder.itemView)
        val clickListenerPositiveButton = DialogInterface.OnClickListener { _, _ ->
            viewModel.deleteDocInventoryOS(data[position].refKey)
            Toast.makeText(context, getString(R.string.document_was_deleted_from_device),
                    Toast.LENGTH_LONG).show()
        }
        val clickListenerNegativeButton = DialogInterface.OnClickListener { _, _ -> recyclerView.adapter?.notifyItemChanged(position) }
        showAlertDialog(clickListenerPositiveButton, clickListenerNegativeButton, getString(R.string.doc_will_be_remove), position)
    }
}
