package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOS


@Dao
interface DocInventoryOSDao {

    @Query("SELECT * FROM docinventoryos")
    fun getAllLiveData(): LiveData<List<DocInventoryOS>>

    @Query("SELECT * FROM docinventoryos")
    fun getAll(): List<DocInventoryOS>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(docInventoryOS: DocInventoryOS)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<DocInventoryOS>)

    @Query("DELETE FROM docinventoryos")
    fun deleteAll()

    @Query("SELECT * FROM docinventoryos WHERE refKey = :refKey LIMIT 1")
    fun get(refKey: String): DocInventoryOS

    @Query("DELETE FROM docinventoryos WHERE refKey = :refKey")
    fun delete(refKey: String)

    @Query("UPDATE docinventoryos SET isEdit = :isEdit WHERE refKey = :refKey")
    fun setIsEditDocInventoryOS(refKey: String, isEdit: Boolean)

    @Query("SELECT * FROM docinventoryos ORDER BY id DESC LIMIT 1")
    fun getLastDocInventoryOSOrderById(): DocInventoryOS?
}