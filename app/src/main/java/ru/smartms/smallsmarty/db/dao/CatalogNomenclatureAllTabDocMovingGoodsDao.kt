package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocMovingGoods


@Dao
interface CatalogNomenclatureAllTabDocMovingGoodsDao {

//    @Query("SELECT tabdocmovinggoods.id as tab_doc_moving_goods_id, tabdocmovinggoods.refKeyDoc as doc_refkey, tabdocmovinggoods.lineNumber as lineNumber, tabdocmovinggoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocmovinggoods.qty as qty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM catalognomenclature, catalogcharacteristic, tabdocmovinggoods WHERE tabdocmovinggoods.refKeyDoc = :refKeyDoc AND catalognomenclature.refKeyDoc = tabdocmovinggoods.catalogNomenclatureKey AND catalogcharacteristic.refKeyDoc = tabdocmovinggoods.catalogCharacteristicKey")
//    fun getAll(refKeyDoc: String): LiveData<List<CatalogNomenclatureAllTabDocMovingGoods>>
//
//    @Query("SELECT tabdocmovinggoods.id as tab_doc_moving_goods_id, tabdocmovinggoods.refKeyDoc as doc_refkey, tabdocmovinggoods.lineNumber as lineNumber, tabdocmovinggoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocmovinggoods.qty as qty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM catalognomenclature, catalogcharacteristic, tabdocmovinggoods WHERE tabdocmovinggoods.id = :id AND catalognomenclature.refKeyDoc = tabdocmovinggoods.catalogNomenclatureKey AND catalogcharacteristic.refKeyDoc = tabdocmovinggoods.catalogCharacteristicKey LIMIT 1")
//    fun get(id: Long): LiveData<CatalogNomenclatureAllTabDocMovingGoods>

    @Query("SELECT tabdocmovinggoods.id as tab_doc_moving_goods_id, tabdocmovinggoods.refKeyDoc as doc_refkey, tabdocmovinggoods.lineNumber as lineNumber, tabdocmovinggoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocmovinggoods.qty as qty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM tabdocmovinggoods LEFT JOIN catalognomenclature ON catalognomenclature.refKey = tabdocmovinggoods.catalogNomenclatureKey LEFT JOIN catalogcharacteristic ON catalogcharacteristic.refKey = tabdocmovinggoods.catalogCharacteristicKey WHERE tabdocmovinggoods.refKeyDoc = :refKeyDoc")
    fun getAll(refKeyDoc: String): LiveData<List<CatalogNomenclatureAllTabDocMovingGoods>>

    @Query("SELECT tabdocmovinggoods.id as tab_doc_moving_goods_id, tabdocmovinggoods.refKeyDoc as doc_refkey, tabdocmovinggoods.lineNumber as lineNumber, tabdocmovinggoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocmovinggoods.qty as qty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM tabdocmovinggoods LEFT JOIN catalognomenclature ON catalognomenclature.refKey = tabdocmovinggoods.catalogNomenclatureKey LEFT JOIN catalogcharacteristic ON catalogcharacteristic.refKey = tabdocmovinggoods.catalogCharacteristicKey WHERE tabdocmovinggoods.id = :id LIMIT 1")
    fun get(id: Long): LiveData<CatalogNomenclatureAllTabDocMovingGoods>
}