package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries


@Dao
interface CatalogSeriesDao {

    @Query("SELECT * FROM catalogseries")
    fun getAllCatalogSeries(): LiveData<List<CatalogSeries>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(catalogSeries: CatalogSeries)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<CatalogSeries>?)

    @Query("DELETE FROM catalogseries")
    fun deleteAllCatalogSeries()

    @Query("SELECT * FROM catalogseries WHERE number LIKE :query ORDER BY number ASC")
    fun getCatalogSeries(query: String): List<CatalogSeries>

    @Query("SELECT * FROM catalogseries")
    fun getCatalogSeriess(): List<CatalogSeries>

    @Query("DELETE FROM catalogseries WHERE refKey = :refKey")
    fun delete(refKey: String?)

    @Query("SELECT *  FROM catalogseries WHERE refKey = :refKey")
    fun get(refKey: String): CatalogSeries?

    @Query("SELECT * FROM catalogseries")
    fun getAll(): List<CatalogSeries>

    @Query("SELECT * FROM catalogseries WHERE barcode = :barcode LIMIT 1")
    fun getByBarcode(barcode: String): CatalogSeries?

    @Query("SELECT * FROM catalogseries WHERE number = :numberReq AND validUntil = :validUntilBegin LIMIT 1")
    fun getByBarcodeNumberValidUntil(validUntilBegin: String?, numberReq: String?): CatalogSeries?
}