package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.document.TabDocRecountGoods


@Dao
interface TabDocRecountGoodsDao {

    @Query("SELECT * FROM tabdocrecountgoods WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber ASC")
    fun getAll(refKeyDoc: String): LiveData<List<TabDocRecountGoods>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tabDocRecountGoods: TabDocRecountGoods?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<TabDocRecountGoods>)

    @Query("DELETE FROM tabdocrecountgoods")
    fun deleteAll()

    @Query("DELETE FROM tabdocrecountgoods WHERE refKeyDoc = :refKeyDoc")
    fun delete(refKeyDoc: String)

    @Query("DELETE FROM tabdocrecountgoods WHERE id = :id")
    fun delete(id: Long)

    @Query("SELECT * FROM tabdocrecountgoods WHERE id = :id LIMIT 1")
    fun getLiveData(id: Long): LiveData<TabDocRecountGoods>

    @Query("SELECT * FROM tabdocrecountgoods WHERE id = :id LIMIT 1")
    fun get(id: Long): TabDocRecountGoods

    @Query("SELECT * FROM tabdocrecountgoods WHERE refKeyDoc = :refKeyDoc")
    fun get(refKeyDoc: String): List<TabDocRecountGoods>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(tabDocRecountGoods: TabDocRecountGoods?)

    @Query("SELECT * FROM tabdocrecountgoods WHERE refKeyDoc = :refKeyDoc AND catalogNomenclatureKey = :catalogNomenclatureKey AND catalogCharacteristicKey = :catalogCharacteristicKey AND catalogSeriesKey = :catalogSeriesKey LIMIT 1")
    fun getViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc: String, catalogNomenclatureKey: String, catalogCharacteristicKey: String?, catalogSeriesKey: String?): TabDocRecountGoods

    @Query("SELECT * FROM tabdocrecountgoods WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber DESC LIMIT 1")
    fun getLastTabDocRecountGoods(refKeyDoc: String?): TabDocRecountGoods?

}