package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStore


@Dao
interface ARGoodsInStoreDao {

    @Query("SELECT * FROM argoodsinstore")
    fun getAllARGoodsInStore(): LiveData<List<ARGoodsInStore>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(arGoodsInStore: ARGoodsInStore)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(arGoodsInStoreList: List<ARGoodsInStore>?)

    @Query("DELETE FROM argoodsinstore")
    fun deleteAll()

    @Query("SELECT * FROM argoodsinstore WHERE catalogSeriesKey = :refKey ORDER BY inStockClosingBalance, inStockTurnover DESC LIMIT 1")
    fun getRecordWithQty(refKey: String) : ARGoodsInStore?
}