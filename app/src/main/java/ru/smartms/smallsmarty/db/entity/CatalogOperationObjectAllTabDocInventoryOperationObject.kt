package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.ColumnInfo

class CatalogOperationObjectAllTabDocInventoryOperationObject {
    @ColumnInfo(name = "tab_doc_inventory_operationobject_id")
    var id: Long = 0
    @ColumnInfo(name = "doc_refkey")
    lateinit var refKey: String
    lateinit var lineNumber: String
    lateinit var catalogOperationObjectKey: String
    var isAvailabilityAct: Boolean = false
    var isAvailabilityAcc: Boolean = false
    lateinit var catalogOperationObjectDescription: String
}