package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.ColumnInfo

class CatalogNomenclatureAllTabDocMovingGoods {
    @ColumnInfo(name = "tab_doc_moving_goods_id")
    var id: Long = 0
    @ColumnInfo(name = "doc_refkey")
    lateinit var refKey: String
    lateinit var lineNumber: String
    lateinit var catalogNomenclatureKey: String
    var qty: Double = 0.0
    var catalogNomenclatureDescription: String? = null
    var catalogCharacteristicDescription: String? = null
}