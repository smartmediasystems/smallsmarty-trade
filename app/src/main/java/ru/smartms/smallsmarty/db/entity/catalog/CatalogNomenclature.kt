package ru.smartms.smallsmarty.db.entity.catalog

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseCatalogEntity

@Entity
data class CatalogNomenclature(
        @SerializedName("ЕдиницаИзмерения_Key") var measureUnitKey: String?,
        @SerializedName("ВидНоменклатуры_Key") var typeNomenclature: String?
) : BaseCatalogEntity()