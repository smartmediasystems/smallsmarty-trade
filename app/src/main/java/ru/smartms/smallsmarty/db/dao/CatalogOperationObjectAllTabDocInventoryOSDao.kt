package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.CatalogOperationObjectAllTabDocInventoryOperationObject


@Dao
interface CatalogOperationObjectAllTabDocInventoryOperationObjectDao {

    @Query("SELECT tabdocinventoryoperationobject.id as tab_doc_inventory_operationobject_id, tabdocinventoryoperationobject.refKeyDoc as doc_refkey, tabdocinventoryoperationobject.lineNumber as lineNumber, tabdocinventoryoperationobject.catalogOperationObjectKey as catalogOperationObjectKey, tabdocinventoryoperationobject.isAvailabilityAct as isAvailabilityAct, tabdocinventoryoperationobject.isAvailabilityAcc as isAvailabilityAcc, catalogoperationobject.Description as catalogOperationObjectDescription   FROM catalogoperationobject, tabdocinventoryoperationobject WHERE tabdocinventoryoperationobject.refKeyDoc = :refKeyDoc AND catalogoperationobject.refKey = tabdocinventoryoperationobject.catalogOperationObjectKey")
    fun getAllLiveData(refKeyDoc: String): LiveData<List<CatalogOperationObjectAllTabDocInventoryOperationObject>>

    @Query("SELECT tabdocinventoryoperationobject.id as tab_doc_inventory_operationobject_id, tabdocinventoryoperationobject.refKeyDoc as doc_refkey, tabdocinventoryoperationobject.lineNumber as lineNumber, tabdocinventoryoperationobject.catalogOperationObjectKey as catalogOperationObjectKey, tabdocinventoryoperationobject.isAvailabilityAct as isAvailabilityAct, tabdocinventoryoperationobject.isAvailabilityAcc as isAvailabilityAcc, catalogoperationobject.Description as catalogOperationObjectDescription   FROM catalogoperationobject, tabdocinventoryoperationobject WHERE tabdocinventoryoperationobject.id = :id AND catalogoperationobject.refKey = tabdocinventoryoperationobject.catalogOperationObjectKey LIMIT 1")
    fun get(id: Long): LiveData<CatalogOperationObjectAllTabDocInventoryOperationObject>

    @Query("SELECT tabdocinventoryoperationobject.id as tab_doc_inventory_operationobject_id, tabdocinventoryoperationobject.refKeyDoc as doc_refkey, tabdocinventoryoperationobject.lineNumber as lineNumber, tabdocinventoryoperationobject.catalogOperationObjectKey as catalogOperationObjectKey, tabdocinventoryoperationobject.isAvailabilityAct as isAvailabilityAct, tabdocinventoryoperationobject.isAvailabilityAcc as isAvailabilityAcc, catalogoperationobject.Description as catalogOperationObjectDescription   FROM catalogoperationobject, tabdocinventoryoperationobject WHERE tabdocinventoryoperationobject.refKeyDoc = :refKeyDoc AND catalogoperationobject.refKey = tabdocinventoryoperationobject.catalogOperationObjectKey")
    fun getAll(refKeyDoc: String): List<CatalogOperationObjectAllTabDocInventoryOperationObject>?
}