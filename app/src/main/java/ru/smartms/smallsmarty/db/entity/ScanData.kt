package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ScanData(
        @PrimaryKey(autoGenerate = true)
        val id: Long,
        val barcode: String,
        val dateTime: String
)