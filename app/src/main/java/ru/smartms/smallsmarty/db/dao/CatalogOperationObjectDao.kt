package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOperationObject


@Dao
interface CatalogOperationObjectDao {

    @Query("SELECT * FROM catalogoperationobject")
    fun getAllCatalogOperationObject(): LiveData<List<CatalogOperationObject>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(catalogOperationObject: CatalogOperationObject)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<CatalogOperationObject>)

    @Query("DELETE FROM catalogoperationobject")
    fun deleteAllCatalogOperationObject()

    @Query("SELECT * FROM catalogoperationobject WHERE description LIKE :query ORDER BY description ASC")
    fun getCatalogOperationObject(query: String): List<CatalogOperationObject>

    @Query("SELECT * FROM catalogoperationobject")
    fun getCatalogOperationObjects(): List<CatalogOperationObject>

    @Query("SELECT * FROM catalogoperationobject WHERE code = :barcode LIMIT 1")
    fun get(barcode: String): CatalogOperationObject?

    @Query("SELECT * FROM catalogoperationobject WHERE refKey= :refKey LIMIT 1")
    fun getFromRefKey(refKey: String): CatalogOperationObject?

    @Query("DELETE FROM catalogoperationobject WHERE refKey = :refKey")
    fun delete(refKey: String)
}