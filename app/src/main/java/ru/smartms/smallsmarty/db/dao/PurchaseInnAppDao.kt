package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.PurchaseInnApp

@Dao
interface PurchaseInnAppDao {

    @Query("SELECT * FROM purchaseinnapp LIMIT 1")
    fun get(): LiveData<PurchaseInnApp>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(purchaseInnApp: PurchaseInnApp)

    @Query("DELETE FROM purchaseinnapp")
    fun deleteAll()


}