package ru.smartms.smallsmarty.db.entity.informationregister

import android.arch.persistence.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseEntity


@Entity
class InfRegBarcodeNomenclatures : BaseEntity() {
    @SerializedName("value")
    @Expose
    var value: List<InfRegBarcodeNomenclature> = ArrayList()
}