package ru.smartms.smallsmarty.db.entity.licensesystem

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity
data class LicenseInfo(
        @PrimaryKey(autoGenerate = true) @NonNull val id: Long,
        @SerializedName("success") val success: Boolean = false,
        @SerializedName("error") val error: String?,
        @SerializedName("code") val code: String?,
        @SerializedName("activated") val activated: Boolean = false,
        @SerializedName("instance") val instance: Long?,
        @SerializedName("message") val message: String?,
        @SerializedName("remaining") val remaining: Int = 0,
        @SerializedName("timestamp") val timestamp: Long?,
        @SerializedName("sig") val sig: String?,
        val licenseKey: String?,
        val email: String?,
        val productId: String?
) {
    @Ignore
    @SerializedName("activations")
    val licenseInfoActivations: List<LicenseInfoActivation> = ArrayList()
}