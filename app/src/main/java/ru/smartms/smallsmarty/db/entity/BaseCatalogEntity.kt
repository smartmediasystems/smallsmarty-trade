package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseCatalogEntity: BaseEntity() {
    @PrimaryKey
    @NonNull
    @SerializedName(value = "Ref_Key", alternate = ["Ref", "Ссылка"])
    lateinit var refKey: String

    @SerializedName("DataVersion")
    var dataVersion: String? = null

    @SerializedName(value = "DeletionMark", alternate = ["ПометкаУдаления"])
    var deletionMark: Boolean? = false

    @SerializedName("Owner")
    var owner: String? = null

    @SerializedName("Predefined")
    var predefined: Boolean? = false

    @SerializedName("Parent_Key")
    var parentKey: String? = null

    @SerializedName(value = "Code", alternate = ["Код"])
    var code: String? = null

    @SerializedName(value = "Description", alternate = ["Наименование"])
    @ColumnInfo(name = "Description")
    var description: String? = null

    @SerializedName("IsFolder")
    @Expose
    var isFolder: Boolean? = null

    @SerializedName("Комментарий")
    var comment: String? = ""
}