package ru.smartms.smallsmarty.db.entity.accumulationregister

import android.arch.persistence.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseEntity

@Entity
data class ARGoodsInStoreOData(
        @SerializedName("value")
        @Expose
        var value: List<ARGoodsInStore>
) : BaseEntity()