package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.TabDocMovingGoods


@Dao
interface TabDocMovingGoodsDao {

    @Query("SELECT * FROM tabdocmovinggoods WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber ASC")
    fun getAll(refKeyDoc: String): LiveData<List<TabDocMovingGoods>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tabDocMovingGoods: TabDocMovingGoods?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<TabDocMovingGoods>)

    @Query("DELETE FROM tabdocmovinggoods")
    fun deleteAll()

    @Query("DELETE FROM tabdocmovinggoods WHERE refKeyDoc = :refKeyDoc")
    fun delete(refKeyDoc: String?)

    @Query("DELETE FROM tabdocmovinggoods WHERE id = :id")
    fun delete(id: Long)

    @Query("SELECT * FROM tabdocmovinggoods WHERE id = :id LIMIT 1")
    fun getLiveData(id: Long): LiveData<TabDocMovingGoods>

    @Query("SELECT * FROM tabdocmovinggoods WHERE id = :id LIMIT 1")
    fun get(id: Long): TabDocMovingGoods

    @Query("SELECT * FROM tabdocmovinggoods WHERE refKeyDoc = :refKeyDoc")
    fun get(refKeyDoc: String): List<TabDocMovingGoods>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(tabDocMovingGoods: TabDocMovingGoods?)

    @Query("SELECT * FROM tabdocmovinggoods WHERE refKeyDoc = :refKeyDoc AND catalogNomenclatureKey = :catalogNomenclatureKey LIMIT 1")
    fun getViaCatalogNomenclatureKeyRefKeyDoc(refKeyDoc: String, catalogNomenclatureKey: String): TabDocMovingGoods

    @Query("SELECT * FROM tabdocmovinggoods WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber DESC LIMIT 1")
    fun getLastTabDocMovingGoods(refKeyDoc: String?): TabDocMovingGoods?

}