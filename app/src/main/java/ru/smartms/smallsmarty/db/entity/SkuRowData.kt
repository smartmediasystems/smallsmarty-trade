package ru.smartms.smallsmarty.db.entity

import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.SkuDetails

class SkuRowData(details: SkuDetails, rowType: Int, billingType: String) {

    private var sku: String = details.sku
    private var title: String? = null
    private var price: String
    private var description: String
    private var type: Int = 0
    private var billingType: String

    init {
        this.title = details.title
        this.price = details.price
        this.description = details.description
        this.type = rowType
        this.billingType = billingType
    }

    fun getSku(): String {
        return sku
    }

    fun getTitle(): String? {
        return title
    }

    fun getPrice(): String {
        return price
    }

    fun getDescription(): String {
        return description
    }

    fun getRowType(): Int {
        return type
    }

    @BillingClient.SkuType
    fun getSkuType(): String {
        return billingType
    }
}