package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.DocMovingGoods


@Dao
interface DocMovingGoodsDao {

    @Query("SELECT * FROM docmovinggoods")
    fun getAllLiveData(): LiveData<List<DocMovingGoods>>

    @Query("SELECT * FROM docmovinggoods")
    fun getAll(): List<DocMovingGoods>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(docMovingGoods: DocMovingGoods)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<DocMovingGoods>?)

    @Query("DELETE FROM docmovinggoods")
    fun deleteAll()

    @Query("SELECT * FROM docmovinggoods WHERE refKey = :refKey LIMIT 1")
    fun get(refKey: String): DocMovingGoods

    @Query("DELETE FROM docmovinggoods WHERE refKey = :refKey")
    fun delete(refKey: String)

    @Query("UPDATE docmovinggoods SET isEdit = :isEdit WHERE refKey = :refKey")
    fun setIsEditDocMovingGoods(refKey: String, isEdit: Boolean)

    @Query("SELECT * FROM docmovinggoods ORDER BY id DESC LIMIT 1")
    fun getLastDocMovingGoodsOrderById(): DocMovingGoods?
}