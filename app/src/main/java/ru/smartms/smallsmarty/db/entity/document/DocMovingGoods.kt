package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseDocEntity

@Entity
class DocMovingGoods : BaseDocEntity() {
    @SerializedName("Организация_Key", alternate = ["Организация"])
    var catalogOrganizationKey: String? = null
    @SerializedName("Склад_Key", alternate = ["Склад"])
    var storeKey: String? = null
    @SerializedName("ОтветственноеЛицо_Key", alternate = ["ОтветственноеЛицо"])
    var responsiblePersonKey: String? = null
    @SerializedName("Ответственный_Key", alternate = ["Ответственный"])
    var userKey: String? = null
    @SerializedName("Организация@navigationLinkUrl")
    var organizationNavigationLinkUrl: String? = null
    @Ignore
    @SerializedName("Товары")
    var goods: List<TabDocMovingGoods> = ArrayList()
}