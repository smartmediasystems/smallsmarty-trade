package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.ScanData


@Dao
interface ScanDataDao {

    @Query("SELECT * FROM scandata")
    fun getAll(): LiveData<List<ScanData>>

    @Query("SELECT * FROM scandata ORDER BY ID DESC LIMIT 1")
    fun getLast(): LiveData<List<ScanData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(scandata: ScanData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<ScanData>)

    @Query("DELETE FROM scandata")
    fun deleteAll()
}