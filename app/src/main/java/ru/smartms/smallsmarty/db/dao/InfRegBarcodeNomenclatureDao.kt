package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature


@Dao
interface InfRegBarcodeNomenclatureDao {

    @Query("SELECT * FROM infregbarcodenomenclature")
    fun getAll(): LiveData<List<InfRegBarcodeNomenclature>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(infRegBarcodeNomenclature: InfRegBarcodeNomenclature)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<InfRegBarcodeNomenclature>)

    @Query("DELETE FROM infregbarcodenomenclature")
    fun deleteAll()

    @Query("SELECT * FROM infregbarcodenomenclature WHERE barcode = :barcode LIMIT 1")
    fun get(barcode: String): InfRegBarcodeNomenclature?
}