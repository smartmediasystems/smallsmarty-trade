package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.DocRecountGoods


@Dao
interface DocRecountGoodsDao {

    @Query("SELECT * FROM docrecountgoods")
    fun getAllLiveData(): LiveData<List<DocRecountGoods>>

    @Query("SELECT * FROM docrecountgoods")
    fun getAll(): List<DocRecountGoods>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(docRecountGoods: DocRecountGoods)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<DocRecountGoods>?)

    @Query("DELETE FROM docrecountgoods")
    fun deleteAll()

    @Query("SELECT * FROM docrecountgoods WHERE refKey = :refKey LIMIT 1")
    fun get(refKey: String): DocRecountGoods

    @Query("DELETE FROM docrecountgoods WHERE refKey = :refKey")
    fun delete(refKey: String)

    @Query("UPDATE docrecountgoods SET isEdit = :isEdit WHERE refKey = :refKey")
    fun setIsEditDocRecountGoods(refKey: String, isEdit: Boolean)

    @Query("SELECT * FROM docrecountgoods ORDER BY id DESC LIMIT 1")
    fun getLastDocRecountGoodsOrderById(): DocRecountGoods?
}