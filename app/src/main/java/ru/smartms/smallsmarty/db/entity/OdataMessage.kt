package ru.smartms.smallsmarty.db.entity

import com.google.gson.annotations.SerializedName


class OdataMessage {
    @SerializedName("lang")
    private val lang: String? = null
    @SerializedName("value")
    val value: String? = null
}