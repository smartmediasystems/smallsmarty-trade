package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseTabDocEntity
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries

@Entity
class TabDocMovingGoods : BaseTabDocEntity() {
    @SerializedName("Номенклатура_Key", alternate = ["Номенклатура"])
    var catalogNomenclatureKey: String? = null
    @SerializedName("Характеристика_Key", alternate = ["Характеристика"])
    var catalogCharacteristicKey: String? = null
    @SerializedName("Упаковка_Key", alternate = ["Упаковка"])
    var pkgKey: String? = null
    @SerializedName("КоличествоУпаковок")
    var pkgQty: Double = 0.0
    @SerializedName("Количество")
    var qty: Double = 0.0
    @SerializedName("КодСтроки")
    var codeString: String? = null
    @SerializedName("СтатусУказанияСерий")
    var statusSeries: Int? = 0
    @SerializedName("СтатусУказанияСерийОтправитель")
    var statusSeriesFrom: Int? = 0
    @SerializedName("СтатусУказанияСерийПолучатель")
    var statusSeriesTo: Int? = 0
    @SerializedName("ЗаказНаПеремещение_Key")
    var orderMoving: String? = null
    @SerializedName("Сделка_Key")
    var dealKey: String? = null
    @SerializedName("Назначение_Key")
    var destinationKey: String? = null
    @SerializedName("НазначениеОтправителя_Key")
    var destinationFromKey: String? = null
    @SerializedName("Серия_Key")
    var seriesKey: String? = null
    @SerializedName("АналитикаУчетаНоменклатуры_Key")
    var nomenclatureAccountingAnalyticsKey: String? = null
    @Ignore
    lateinit var catalogNomenclature: CatalogNomenclature
    @Ignore
    var catalogCharacteristic: CatalogCharacteristic? = null
    @Ignore
    var catalogSeries: CatalogSeries? = null
}