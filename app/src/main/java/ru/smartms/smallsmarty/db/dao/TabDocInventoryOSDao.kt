package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOS


@Dao
interface TabDocInventoryOSDao {

    @Query("SELECT * FROM tabdocinventoryos WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber ASC")
    fun getAllLiveData(refKeyDoc: String): LiveData<List<TabDocInventoryOS>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tabDocInventoryOS: TabDocInventoryOS?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<TabDocInventoryOS>)

    @Query("DELETE FROM tabdocinventoryos")
    fun deleteAll()

    @Query("DELETE FROM tabdocinventoryos WHERE refKeyDoc = :refKeyDoc")
    fun delete(refKeyDoc: String)

    @Query("DELETE FROM tabdocinventoryos WHERE id = :id")
    fun delete(id: Long)

    @Query("SELECT * FROM tabdocinventoryos WHERE id = :id LIMIT 1")
    fun getLiveData(id: Long): LiveData<TabDocInventoryOS>

    @Query("SELECT * FROM tabdocinventoryos WHERE id = :id LIMIT 1")
    fun get(id: Long): TabDocInventoryOS

    @Query("SELECT * FROM tabdocinventoryos WHERE refKeyDoc = :refKeyDoc")
    fun get(refKeyDoc: String): List<TabDocInventoryOS>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(tabDocInventoryOS: TabDocInventoryOS?)

    @Query("SELECT * FROM tabdocinventoryos WHERE refKeyDoc = :refKeyDoc AND catalogOSKey = :catalogOSKey LIMIT 1")
    fun getViaCatalogOSKeyRefKeyDoc(refKeyDoc: String, catalogOSKey: String): TabDocInventoryOS

    @Query("SELECT * FROM tabdocinventoryos WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber DESC LIMIT 1")
    fun getLastTabDocInventoryOS(refKeyDoc: String?): TabDocInventoryOS?

    @Query("UPDATE tabdocinventoryos SET isAvailabilityAct = :checked WHERE id = :id")
    fun updateIsAct(checked: Boolean, id: Long)
}