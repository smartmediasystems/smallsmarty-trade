package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseTabDocEntity
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature
import ru.smartms.smallsmarty.db.entity.catalog.CatalogSeries

@Entity
class TabDocRecountGoods : BaseTabDocEntity() {
    @SerializedName("Количество")
    var accQty: Double = 0.0
    @SerializedName("КоличествоФакт")
    var qty: Double = 0.0
    @SerializedName("КоличествоУпаковок")
    var qtyPkg: Double = 0.0
    @SerializedName("КоличествоУпаковокФакт")
    var actQtyPkg: Double = 0.0
    @SerializedName("Номенклатура_Key", alternate = ["Номенклатура"])
    var catalogNomenclatureKey: String? = null
    @SerializedName("Упаковка_Key", alternate = ["Упаковка"])
    var pkgKey: String? = null
    @SerializedName("Характеристика_Key", alternate = ["Характеристика"])
    var catalogCharacteristicKey: String? = null
    @SerializedName("Назначение_Key", alternate = ["Назначение"])
    var assignmentKey: String? = null
    @SerializedName("Ячейка_Key", alternate = ["Ячейка"])
    var cellKey: String? = null
    @SerializedName("Серия_Key", alternate = ["Серия"])
    var catalogSeriesKey: String? = null
    @SerializedName("СтатусУказанияСерий")
    var indicationStatusSeries: Int = 0
    @Ignore
    var catalogNomenclature: CatalogNomenclature? = null
    @Ignore
    var catalogCharacteristic: CatalogCharacteristic? = null
    @Ignore
    var catalogSeries: CatalogSeries? = null
}