package ru.smartms.smallsmarty.db.entity.catalog

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseCatalogEntity

@Entity
data class CatalogOperationObject(
        @SerializedName("НаименованиеПолное") val descriptionFull: String?,
        @SerializedName("Изготовитель") val vendor: String?,
        @SerializedName("ЗаводскойНомер") val serialNumber: String?,
        @SerializedName("НомерПаспорта") val passportNumber: String?,
        @SerializedName("ДатаВыпуска") val dateRelease: String?
) : BaseCatalogEntity()