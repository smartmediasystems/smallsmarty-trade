package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.DocInventoryOperationObject


@Dao
interface DocInventoryOperationObjectDao {

    @Query("SELECT * FROM docinventoryoperationobject")
    fun getAllLiveData(): LiveData<List<DocInventoryOperationObject>>

    @Query("SELECT * FROM docinventoryoperationobject")
    fun getAll(): List<DocInventoryOperationObject>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(docInventoryOperationObject: DocInventoryOperationObject)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<DocInventoryOperationObject>)

    @Query("DELETE FROM docinventoryoperationobject")
    fun deleteAll()

    @Query("SELECT * FROM docinventoryoperationobject WHERE refKey = :refKey LIMIT 1")
    fun get(refKey: String): DocInventoryOperationObject

    @Query("DELETE FROM docinventoryoperationobject WHERE refKey = :refKey")
    fun delete(refKey: String)

    @Query("UPDATE docinventoryoperationobject SET isEdit = :isEdit WHERE refKey = :refKey")
    fun setIsEditDocInventoryOperationObject(refKey: String, isEdit: Boolean)

    @Query("SELECT * FROM docinventoryoperationobject ORDER BY id DESC LIMIT 1")
    fun getLastDocInventoryOperationObjectOrderById(): DocInventoryOperationObject?
}