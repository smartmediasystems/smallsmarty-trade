package ru.smartms.smallsmarty.db.entity.catalog

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseCatalogEntity
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStore

@Entity
class CatalogSeries : BaseCatalogEntity() {
    @SerializedName("ГоденДо") var validUntil: String? = null
    @SerializedName("ВидНоменклатуры_Key") var typeNomenclature: String? = null
    @SerializedName("НомерКиЗГИСМ") var numberKiZGISM: String? = null
    @SerializedName("RFIDTID") var rfidTID: String? = null
    @SerializedName("RFIDUser") var rfidUser: String? = null
    @SerializedName("RFIDEPC") var rfidEPC: String? = null
    @SerializedName("EPCGTIN") var epcGTIN: String? = null
    @SerializedName("RFIDМеткаНеЧитаемая") var rfidNotReadable: Boolean? = null
    @SerializedName("ДатаПроизводства") var dateProduction: String? = null
    @SerializedName("ПроизводительЕГАИС_Key") var manufacturerEGAISKey: String? = null
    @SerializedName("Справка2ЕГАИС_Key") var inquiry2EGAISKey: String? = null
    @SerializedName("ПроизводительВЕТИС_Key") var manufacturerVETISKey: String? = null
    @SerializedName("ЗаписьСкладскогоЖурналаВЕТИС_Key") var recordWETISWarehouseJournalKey: String? = null
    @SerializedName("ИдентификаторПартииВЕТИС") var uidVETIS: String? = null
    @SerializedName("PredefinedDataName") var predefinedDataName: String? = null
    @SerializedName("Number", alternate = ["Номер"]) var number: String? = null
    var barcode: String? = null
    @Ignore
    var arGoodsInStore: ARGoodsInStore? = null
    var validUntilBegin: String? = null
    var validUntilEnd: String? = null
    var numberReq: String? = null
}