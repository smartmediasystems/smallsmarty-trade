package ru.smartms.smallsmarty.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import ru.smartms.smallsmarty.db.dao.*
import ru.smartms.smallsmarty.db.entity.ErrorMessage
import ru.smartms.smallsmarty.db.entity.PurchaseInnApp
import ru.smartms.smallsmarty.db.entity.ScanData
import ru.smartms.smallsmarty.db.entity.accumulationregister.ARGoodsInStore
import ru.smartms.smallsmarty.db.entity.catalog.*
import ru.smartms.smallsmarty.db.entity.document.*
import ru.smartms.smallsmarty.db.entity.informationregister.InfRegBarcodeNomenclature
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo

@Database(entities = [
    CatalogNomenclature::class,
    InfRegBarcodeNomenclature::class,
    ScanData::class,
    DocRecountGoods::class,
    TabDocRecountGoods::class,
    TabDocInventoryOS::class,
    CatalogOperationObject::class,
    CatalogOS::class,
    DocInventoryOS::class,
    DocInventoryOperationObject::class,
    TabDocInventoryOperationObject::class,
    DocMovingGoods::class,
    TabDocMovingGoods::class,
    ErrorMessage::class,
    CatalogSeries::class,
    CatalogCharacteristic::class,
    LicenseInfo::class,
    PurchaseInnApp::class,
    ARGoodsInStore::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun catalogNomenclatureDao(): CatalogNomenclatureDao
    abstract fun infRegBarcodeNomenclatureDao(): InfRegBarcodeNomenclatureDao
    abstract fun scanDataDao(): ScanDataDao
    abstract fun docRecountGoodsDao(): DocRecountGoodsDao
    abstract fun tabDocRecountGoodsDao(): TabDocRecountGoodsDao
    abstract fun catalogNomenclatureAllTabDocRecountGoodsDao(): CatalogNomenclatureAllTabDocRecountGoodsDao
    abstract fun catalogOSDao(): CatalogOSDao
    abstract fun catalogSeriesDao(): CatalogSeriesDao
    abstract fun catalogCharacteristicDao(): CatalogCharacteristicDao
    abstract fun docInventoryOSDao(): DocInventoryOSDao
    abstract fun tabDocInventoryOSDao(): TabDocInventoryOSDao
    abstract fun catalogOSAllTabDocInventoryOSDao(): CatalogOSAllTabDocInventoryOSDao
    abstract fun docMovingGoodsDao(): DocMovingGoodsDao
    abstract fun tabDocMovingGoodsDao(): TabDocMovingGoodsDao
    abstract fun catalogNomenclatureAllTabDocMovingGoodsDao(): CatalogNomenclatureAllTabDocMovingGoodsDao
    abstract fun errorMessageDao(): ErrorMessageDao
    abstract fun licenseInfoDao(): LicenseInfoDao
    abstract fun aRGoodsInStoreDao(): ARGoodsInStoreDao
    abstract fun catalogOperationObjectDao(): CatalogOperationObjectDao
    abstract fun catalogOperationObjectAllTabDocInventoryOperationObjectDao(): CatalogOperationObjectAllTabDocInventoryOperationObjectDao
    abstract fun docInventoryOperationObjectDao(): DocInventoryOperationObjectDao
    abstract fun tabDocInventoryOperationObjectDao(): TabDocInventoryOperationObjectDao
}