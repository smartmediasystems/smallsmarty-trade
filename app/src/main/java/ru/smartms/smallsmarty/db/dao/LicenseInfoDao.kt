package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.licensesystem.LicenseInfo

@Dao
interface LicenseInfoDao {

    @Query("SELECT * FROM licenseinfo")
    fun getAll(): LiveData<List<LicenseInfo>>

    @Query("SELECT * FROM licenseinfo ORDER BY ID DESC LIMIT 1")
    fun getLast(): LiveData<List<LicenseInfo>>

    @Query("SELECT * FROM licenseinfo WHERE activated = 1 ORDER BY timestamp DESC LIMIT 1")
    fun getActivatedLiveData(): LiveData<LicenseInfo>

    @Query("SELECT * FROM licenseinfo WHERE activated = 1 ORDER BY timestamp DESC LIMIT 1")
    fun getActivated(): LicenseInfo?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(licenseInfo: LicenseInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(licenseInfos: List<LicenseInfo>)

    @Query("DELETE FROM licenseinfo")
    fun deleteAll()
}