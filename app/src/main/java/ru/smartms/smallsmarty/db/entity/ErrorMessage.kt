package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ErrorMessage(
        @PrimaryKey(autoGenerate = true)
        val id: Long,
        val message: String,
        val dateTime: String
)