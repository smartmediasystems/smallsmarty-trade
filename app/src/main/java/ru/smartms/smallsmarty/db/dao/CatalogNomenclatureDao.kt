package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature


@Dao
interface CatalogNomenclatureDao {

    @Query("SELECT * FROM catalognomenclature")
    fun getAllCatalogNomenclature(): LiveData<List<CatalogNomenclature>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(catalogNomenclature: CatalogNomenclature)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<CatalogNomenclature>?)

    @Query("DELETE FROM catalognomenclature")
    fun deleteAllCatalogNomenclature()

    @Query("SELECT * FROM catalognomenclature WHERE description LIKE :query ORDER BY description ASC")
    fun getCatalogNomenclature(query: String): List<CatalogNomenclature>

    @Query("SELECT * FROM catalognomenclature")
    fun getCatalogNomenclatures(): List<CatalogNomenclature>

    @Query("DELETE FROM catalognomenclature WHERE refKey = :refKey")
    fun delete(refKey: String?)

    @Query("SELECT *  FROM catalognomenclature WHERE refKey = :refKey")
    fun get(refKey: String): CatalogNomenclature?
}