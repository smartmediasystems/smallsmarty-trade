package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.android.billingclient.api.Purchase
import java.io.Serializable

@Entity
class PurchaseInnApp: Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Long = 0
    lateinit var orderId: String
    lateinit var sku: String
    lateinit var packageName: String
    var purchaseTime: Long = -1L
    lateinit var purchaseToken: String
    lateinit var originalJson: String
}

fun getPurchaseInnApp(purchase: Purchase): PurchaseInnApp {
    val purchaseInnApp = PurchaseInnApp()
    purchaseInnApp.orderId = purchase.orderId
    purchaseInnApp.sku = purchase.sku
    purchaseInnApp.packageName = purchase.packageName
    purchaseInnApp.purchaseTime = purchase.purchaseTime
    purchaseInnApp.purchaseToken = purchase.purchaseToken
    purchaseInnApp.originalJson = purchase.originalJson
    return purchaseInnApp
}