package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic


@Dao
interface CatalogCharacteristicDao {

    @Query("SELECT * FROM catalogcharacteristic")
    fun getAllCatalogCharacteristic(): LiveData<List<CatalogCharacteristic>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(catalogCharacteristic: CatalogCharacteristic)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<CatalogCharacteristic>?)

    @Query("DELETE FROM catalogcharacteristic")
    fun deleteAllCatalogCharacteristic()

    @Query("SELECT * FROM catalogcharacteristic WHERE (owner = :typeCatalogNomenclatureKey OR owner = :catalogNomenclatureKey) AND description LIKE :query ORDER BY description ASC")
    fun getCatalogCharacteristic(query: String, typeCatalogNomenclatureKey: String?, catalogNomenclatureKey: String?): List<CatalogCharacteristic>

    @Query("SELECT * FROM catalogcharacteristic")
    fun getCatalogCharacteristics(): List<CatalogCharacteristic>

    @Query("DELETE FROM catalogcharacteristic WHERE refKey = :refKey")
    fun delete(refKey: String?)

    @Query("SELECT *  FROM catalogcharacteristic WHERE refKey = :refKey")
    fun get(refKey: String): CatalogCharacteristic?
}