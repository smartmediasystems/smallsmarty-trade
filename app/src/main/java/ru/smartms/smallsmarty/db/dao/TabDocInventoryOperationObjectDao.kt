package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.document.TabDocInventoryOperationObject


@Dao
interface TabDocInventoryOperationObjectDao {

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber ASC")
    fun getAllLiveData(refKeyDoc: String): LiveData<List<TabDocInventoryOperationObject>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tabDocInventoryOperationObject: TabDocInventoryOperationObject?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<TabDocInventoryOperationObject>)

    @Query("DELETE FROM tabdocinventoryoperationobject")
    fun deleteAll()

    @Query("DELETE FROM tabdocinventoryoperationobject WHERE refKeyDoc = :refKeyDoc")
    fun delete(refKeyDoc: String)

    @Query("DELETE FROM tabdocinventoryoperationobject WHERE id = :id")
    fun delete(id: Long)

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE id = :id LIMIT 1")
    fun getLiveData(id: Long): LiveData<TabDocInventoryOperationObject>

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE id = :id LIMIT 1")
    fun get(id: Long): TabDocInventoryOperationObject

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE refKeyDoc = :refKeyDoc")
    fun get(refKeyDoc: String): List<TabDocInventoryOperationObject>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(tabDocInventoryOperationObject: TabDocInventoryOperationObject?)

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE refKeyDoc = :refKeyDoc AND catalogOperationObjectKey = :catalogOperationObjectKey LIMIT 1")
    fun getViaCatalogOperationObjectKeyRefKeyDoc(refKeyDoc: String, catalogOperationObjectKey: String): TabDocInventoryOperationObject

    @Query("SELECT * FROM tabdocinventoryoperationobject WHERE refKeyDoc = :refKeyDoc ORDER BY lineNumber DESC LIMIT 1")
    fun getLastTabDocInventoryOperationObject(refKeyDoc: String?): TabDocInventoryOperationObject?

    @Query("UPDATE tabdocinventoryoperationobject SET isAvailabilityAct = :checked WHERE id = :id")
    fun updateIsAct(checked: Boolean, id: Long)
}