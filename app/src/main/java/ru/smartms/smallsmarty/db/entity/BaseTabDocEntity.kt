package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

open class BaseTabDocEntity : BaseEntity() {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Long = 0

    @SerializedName("Ref_Key", alternate = ["Ссылка"])
    lateinit var refKeyDoc: String

    @SerializedName("LineNumber", alternate = ["НомерСтроки"])
    var lineNumber: Int = 0
}