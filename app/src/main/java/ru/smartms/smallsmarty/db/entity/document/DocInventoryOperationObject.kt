package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseDocEntity

@Entity
class DocInventoryOperationObject : BaseDocEntity() {
    @SerializedName("Организация_Key", alternate = ["Организация"])
    var organizationKey: String? = null
    @SerializedName("ПодразделениеОрганизации_Key", alternate = ["ПодразделениеОрганизации"])
    var departmentOrganizationKey: String? = null
    @SerializedName("ДокументОснованиеВид")
    var docBaseType: String? = null
    @SerializedName("ДокументОснованиеНомер")
    var docBaseNumber: String? = null
    @SerializedName("ДокументОснованиеДата")
    var docBaseDate: String? = null
    @SerializedName("ДатаНачалаИнвентаризации")
    var dateBeginInventory: String? = null
    @SerializedName("ДатаОкончанияИнвентаризации")
    var dateEndInventory: String? = null
    @SerializedName("ПричинаПроведенияИнвентаризации")
    var reasonInventory: String? = null
    @SerializedName("Ответственный_Key", alternate = ["Ответственный"])
    var user1C: String? = null
    @SerializedName("ОтветственноеЛицо_Key", alternate = ["ОтветственноеЛицо"])
    var responsiblePersonKey: String? = null
    @SerializedName("Организация@navigationLinkUrl")
    var organizationNavigationLinkUrl: String? = null
    @Ignore
    @SerializedName("ОС")
    var os: List<TabDocInventoryOperationObject> = ArrayList()
    @Ignore
    @SerializedName("ИнвентаризационнаяКомиссия")
    lateinit var inventoryCommission: List<Any>
}