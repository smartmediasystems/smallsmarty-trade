package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.ColumnInfo

class CatalogNomenclatureAllTabDocRecountGoods {
    @ColumnInfo(name = "tab_doc_recount_goods_id")
    var id: Long = 0
    @ColumnInfo(name = "doc_refkey")
    lateinit var refKey: String
    lateinit var lineNumber: String
    lateinit var catalogNomenclatureKey: String
    var qty: Double = 0.0
    var accQty: Double = 0.0
    var catalogNomenclatureDescription: String? = null
    var catalogCharacteristicDescription: String? = null

    fun equals(other: CatalogNomenclatureAllTabDocRecountGoods?): Boolean {
        return (this.id == other?.id
                && this.refKey == other.refKey
                && this.lineNumber == other.lineNumber
                && this.catalogNomenclatureKey == other.catalogNomenclatureKey
                && this.qty == other.qty
                && this.accQty == other.accQty
                && this.catalogNomenclatureDescription == other.catalogNomenclatureDescription
                && this.catalogCharacteristicDescription == other.catalogCharacteristicDescription)
    }
}