package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.ErrorMessage

@Dao
interface ErrorMessageDao {

    @Query("SELECT * FROM errormessage")
    fun getAll(): LiveData<List<ErrorMessage>>

    @Query("SELECT * FROM errormessage ORDER BY ID DESC LIMIT 1")
    fun getLast(): LiveData<List<ErrorMessage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(errorMessage: ErrorMessage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(errorMessages: List<ErrorMessage>)

    @Query("DELETE FROM errormessage")
    fun deleteAll()
}