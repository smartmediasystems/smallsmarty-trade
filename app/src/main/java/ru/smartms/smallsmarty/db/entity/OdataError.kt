package ru.smartms.smallsmarty.db.entity

import com.google.gson.annotations.SerializedName


class OdataError {
    @SerializedName("code")
    var code: Int = 0
    @SerializedName("message")
    var message: OdataMessage? = null
}