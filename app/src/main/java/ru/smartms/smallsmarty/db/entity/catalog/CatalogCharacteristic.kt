package ru.smartms.smallsmarty.db.entity.catalog

import android.arch.persistence.room.Entity
import ru.smartms.smallsmarty.db.entity.BaseCatalogEntity

@Entity
class CatalogCharacteristic : BaseCatalogEntity()
