package ru.smartms.smallsmarty.db.entity.catalog

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseEntity


data class CatalogCharacteristics(
        @SerializedName("value")
        @Expose
        var value: List<CatalogCharacteristic>
) : BaseEntity()