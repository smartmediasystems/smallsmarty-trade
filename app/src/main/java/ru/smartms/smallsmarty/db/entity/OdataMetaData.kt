package ru.smartms.smallsmarty.db.entity

import com.google.gson.annotations.SerializedName


class OdataMetaData {
    @SerializedName("odata.metadata")
    var odataMetadata: String? = null
    @SerializedName("value")
    var value: ArrayList<HashMap<String, Any>>? = null
    @SerializedName("odata.error")
    var odataError: OdataError? = null
}
