package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseTabDocEntity

@Entity
data class TabDocInventoryOS(
        @SerializedName("ОсновноеСредство_Key", alternate = ["ОсновноеСредство"]) var catalogOSKey: String?,
        @SerializedName("СтоимостьПоДаннымУчета") val costAcc: Double = 0.0,
        @SerializedName("НаличиеПоДаннымУчета") val isAvailabilityAcc: Boolean = false,
        @SerializedName("СтоимостьФактическая") val costAct: Double = 0.0,
        @SerializedName("НаличиеФактическое") var isAvailabilityAct: Boolean? = true,
        @SerializedName("СтоимостьПоРезультатамИнвентаризации") val costBasedOnInventory: Double = 0.0
) : BaseTabDocEntity()