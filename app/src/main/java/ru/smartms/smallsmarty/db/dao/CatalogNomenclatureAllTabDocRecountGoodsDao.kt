package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.CatalogNomenclatureAllTabDocRecountGoods


@Dao
interface CatalogNomenclatureAllTabDocRecountGoodsDao {

//    @Query("SELECT tabdocrecountgoods.id as tab_doc_recount_goods_id, tabdocrecountgoods.refKeyDoc as doc_refkey, tabdocrecountgoods.lineNumber as lineNumber, tabdocrecountgoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocrecountgoods.qty as qty, tabdocrecountgoods.accQty as accQty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription   FROM catalognomenclature, catalogcharacteristic, tabdocrecountgoods WHERE tabdocrecountgoods.refKeyDoc = :refKeyDoc AND catalognomenclature.refKeyDoc = tabdocrecountgoods.catalogNomenclatureKey AND catalogcharacteristic.refKeyDoc = tabdocrecountgoods.catalogCharacteristicKey")
//    fun getAll(refKeyDoc: String): LiveData<List<CatalogNomenclatureAllTabDocRecountGoods>>
//
//    @Query("SELECT tabdocrecountgoods.id as tab_doc_recount_goods_id, tabdocrecountgoods.refKeyDoc as doc_refkey, tabdocrecountgoods.lineNumber as lineNumber, tabdocrecountgoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocrecountgoods.qty as qty, tabdocrecountgoods.accQty as accQty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription   FROM catalognomenclature, catalogcharacteristic, tabdocrecountgoods WHERE tabdocrecountgoods.id = :id AND catalognomenclature.refKeyDoc = tabdocrecountgoods.catalogNomenclatureKey AND catalogcharacteristic.refKeyDoc = tabdocrecountgoods.catalogCharacteristicKey LIMIT 1")
//    fun get(id: Long): LiveData<CatalogNomenclatureAllTabDocRecountGoods>

    @Query("SELECT tabdocrecountgoods.id as tab_doc_recount_goods_id, tabdocrecountgoods.refKeyDoc as doc_refkey, tabdocrecountgoods.lineNumber as lineNumber, tabdocrecountgoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocrecountgoods.qty as qty, tabdocrecountgoods.accQty as accQty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM tabdocrecountgoods LEFT JOIN catalognomenclature ON catalognomenclature.refKey = tabdocrecountgoods.catalogNomenclatureKey LEFT JOIN catalogcharacteristic ON catalogcharacteristic.refKey = tabdocrecountgoods.catalogCharacteristicKey WHERE tabdocrecountgoods.refKeyDoc = :refKeyDoc")
    fun getAll(refKeyDoc: String): LiveData<List<CatalogNomenclatureAllTabDocRecountGoods>>

    @Query("SELECT tabdocrecountgoods.id as tab_doc_recount_goods_id, tabdocrecountgoods.refKeyDoc as doc_refkey, tabdocrecountgoods.lineNumber as lineNumber, tabdocrecountgoods.catalogNomenclatureKey as catalogNomenclatureKey, tabdocrecountgoods.qty as qty, tabdocrecountgoods.accQty as accQty, catalognomenclature.Description as catalogNomenclatureDescription, catalogcharacteristic.Description as catalogCharacteristicDescription FROM tabdocrecountgoods LEFT JOIN catalognomenclature ON catalognomenclature.refKey = tabdocrecountgoods.catalogNomenclatureKey LEFT JOIN catalogcharacteristic ON catalogcharacteristic.refKey = tabdocrecountgoods.catalogCharacteristicKey WHERE tabdocrecountgoods.id = :id LIMIT 1")
    fun get(id: Long): LiveData<CatalogNomenclatureAllTabDocRecountGoods>
}