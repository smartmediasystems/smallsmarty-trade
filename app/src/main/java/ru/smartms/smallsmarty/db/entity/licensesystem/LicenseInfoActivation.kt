package ru.smartms.smallsmarty.db.entity.licensesystem

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity
data class LicenseInfoActivation(
        @PrimaryKey(autoGenerate = true) @NonNull val id: Long,
        @SerializedName("activation_id") val activationId: Long?,
        @SerializedName("instance") val instance: Long?,
        @SerializedName("activation_platform") val code: String?,
        @SerializedName("activation_time") val activationTime: String?
)