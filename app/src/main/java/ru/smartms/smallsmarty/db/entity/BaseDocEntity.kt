package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

open class BaseDocEntity: BaseEntity() {
    var id: Long = 0

    @PrimaryKey
    @NonNull
    @SerializedName(value = "Ref_Key", alternate = ["Ref", "Ссылка"])
    lateinit var refKey: String

    @SerializedName("DataVersion")
    var dataVersion: String? = null

    @SerializedName(value = "DeletionMark", alternate = ["ПометкаУдаления"])
    var deletionMark: Boolean? = false

    @SerializedName("Комментарий")
    var comment: String? = ""

    //Documents
    @SerializedName("Number", alternate = ["Номер"])
    lateinit var number: String

    @SerializedName("Date", alternate = ["Дата"])
    lateinit var date: String

    @SerializedName("Posted", alternate = ["Проведен"])
    var posted: Boolean = false

    var isEdit: Boolean = false
    var isDownloaded: Boolean = false
}