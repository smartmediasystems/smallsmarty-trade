package ru.smartms.smallsmarty.db.entity

import android.arch.persistence.room.Ignore
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseEntity {
    @SerializedName("odata.error")
    @Expose
    @Ignore
    var odataError: OdataError? = null

    @SerializedName("odata.metadata")
    @Expose
    var odataMetadata: String? = null
}