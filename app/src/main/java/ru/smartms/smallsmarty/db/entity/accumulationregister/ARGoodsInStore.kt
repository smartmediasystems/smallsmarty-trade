package ru.smartms.smallsmarty.db.entity.accumulationregister

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ARGoodsInStore(
        @PrimaryKey(autoGenerate = true) val id: Long,
        @SerializedName("Period") val period: String?,
        @SerializedName("LineNumber") val lineNumber: Int?,
        @SerializedName("Active") val active: Boolean?,
        @SerializedName("RecordType") val recordType: String?,
        @SerializedName("Номенклатура_Key") val catalogNomenclatureKey: String,
        @SerializedName("Характеристика_Key") val catalogCharacteristicKey: String,
        @SerializedName("Назначение_Key") val assignmentKey: String,
        @SerializedName("Склад_Key") val storeKey: String,
        @SerializedName("Помещение_Key") val roomKey: String,
        @SerializedName("Серия_Key") val catalogSeriesKey: String,
        @SerializedName("ВНаличии") val inStock: Double,
        @SerializedName("ВНаличииOpeningBalance") val inStockOpeningBalance: Double,
        @SerializedName("ВНаличииTurnover") val inStockTurnover: Double,
        @SerializedName("ВНаличииReceipt") val inStockReceipt: Double,
        @SerializedName("ВНаличииExpense") val inStockExpense: Double,
        @SerializedName("ВНаличииClosingBalance") val inStockClosingBalance: Double,
        @SerializedName("КОтгрузке") val toShipment: Int,
        @SerializedName("КОтгрузкеOpeningBalance") val toShipmentOpeningBalance: Double,
        @SerializedName("КОтгрузкеTurnover") val toShipmentTurnover: Double,
        @SerializedName("КОтгрузкеReceipt") val toShipmentReceipt: Double,
        @SerializedName("КОтгрузкеExpense") val toShipmentExpense: Double,
        @SerializedName("КОтгрузкеClosingBalance") val toShipmentClosingBalance: Double,
        @SerializedName("КонтролироватьОстатки") val monitorStock: Boolean?
)
