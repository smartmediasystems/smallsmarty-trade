package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseDocEntity
import kotlin.collections.ArrayList

@Entity
class DocRecountGoods : BaseDocEntity() {
    @SerializedName("Организация_Key", alternate = ["Организация"])
    var organizationKey: String? = null
    @SerializedName("Склад_Key", alternate = ["Склад"])
    var storeKey: String? = null
    @SerializedName("ОтветственноеЛицо_Key", alternate = ["ОтветственноеЛицо"])
    var responsiblePersonKey: String? = null
    @SerializedName("Ответственный_Key", alternate = ["Ответственный"])
    var userKey: String? = null
    @Ignore
    @SerializedName("Товары")
    var goods: List<TabDocRecountGoods> = ArrayList()
    @Ignore
    @SerializedName("ИнвентаризационнаяКомиссия")
    lateinit var inventoryCommission: List<Any>
}



