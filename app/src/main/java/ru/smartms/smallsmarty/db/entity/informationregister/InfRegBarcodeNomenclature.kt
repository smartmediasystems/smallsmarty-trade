package ru.smartms.smallsmarty.db.entity.informationregister

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseEntity
import ru.smartms.smallsmarty.db.entity.catalog.CatalogCharacteristic
import ru.smartms.smallsmarty.db.entity.catalog.CatalogNomenclature


@Entity
data class InfRegBarcodeNomenclature(
        @PrimaryKey @SerializedName("Штрихкод") var barcode: String,
        @SerializedName("Номенклатура_Key") var catalogNomenclatureKey: String,
        @SerializedName("Характеристика_Key") var catalogCharacteristicKey: String,
        @SerializedName("Упаковка_Key") var pkgKey: String
) : BaseEntity() {
    @Ignore
    @SerializedName("Номенклатура")
    lateinit var catalogNomenclature: CatalogNomenclature
    @Ignore
    @SerializedName("Характеристика")
    var catalogCharacteristic: CatalogCharacteristic? = null
}