package ru.smartms.smallsmarty.db.entity

import com.google.gson.annotations.SerializedName

data class ProductPurchase(
        @SerializedName("consumptionState")
        val consumptionState: Int,
        @SerializedName("developerPayload")
        val developerPayload: String,
        @SerializedName("kind")
        val kind: String,
        @SerializedName("orderId")
        val orderId: String,
        @SerializedName("purchaseState")
        val purchaseState: Int,
        @SerializedName("purchaseTimeMillis")
        val purchaseTimeMillis: Long,
        @SerializedName("purchaseType")
        val purchaseType: Int
)