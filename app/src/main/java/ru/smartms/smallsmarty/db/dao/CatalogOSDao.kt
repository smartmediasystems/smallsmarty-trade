package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS


@Dao
interface CatalogOSDao {

    @Query("SELECT * FROM catalogos")
    fun getAllCatalogOS(): LiveData<List<CatalogOS>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(catalogOS: CatalogOS)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(products: List<CatalogOS>)

    @Query("DELETE FROM catalogos")
    fun deleteAllCatalogOS()

    @Query("SELECT * FROM catalogos WHERE description LIKE :query ORDER BY description ASC")
    fun getCatalogOS(query: String): List<CatalogOS>

    @Query("SELECT * FROM catalogos")
    fun getCatalogOSes(): List<CatalogOS>

    @Query("SELECT * FROM catalogos WHERE code = :barcode LIMIT 1")
    fun get(barcode: String): CatalogOS?

    @Query("SELECT * FROM catalogos WHERE refKey= :refKey LIMIT 1")
    fun getFromRefKey(refKey: String): CatalogOS?

    @Query("DELETE FROM catalogos WHERE refKey = :refKey")
    fun delete(refKey: String)
}