package ru.smartms.smallsmarty.db.entity.document

import android.arch.persistence.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ru.smartms.smallsmarty.db.entity.BaseEntity

@Entity
data class DocRecountGoodses(
        @SerializedName("value")
        @Expose
        var value: List<DocRecountGoods> = ArrayList()
) : BaseEntity()