package ru.smartms.smallsmarty.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import ru.smartms.smallsmarty.db.entity.CatalogOSAllTabDocInventoryOS


@Dao
interface CatalogOSAllTabDocInventoryOSDao {

    @Query("SELECT tabdocinventoryos.id as tab_doc_inventory_os_id, tabdocinventoryos.refKeyDoc as doc_refkey, tabdocinventoryos.lineNumber as lineNumber, tabdocinventoryos.catalogOSKey as catalogOSKey, tabdocinventoryos.isAvailabilityAct as isAvailabilityAct, tabdocinventoryos.isAvailabilityAcc as isAvailabilityAcc, catalogos.Description as catalogOSDescription   FROM catalogos, tabdocinventoryos WHERE tabdocinventoryos.refKeyDoc = :refKeyDoc AND catalogos.refKey = tabdocinventoryos.catalogOSKey")
    fun getAllLiveData(refKeyDoc: String): LiveData<List<CatalogOSAllTabDocInventoryOS>>

    @Query("SELECT tabdocinventoryos.id as tab_doc_inventory_os_id, tabdocinventoryos.refKeyDoc as doc_refkey, tabdocinventoryos.lineNumber as lineNumber, tabdocinventoryos.catalogOSKey as catalogOSKey, tabdocinventoryos.isAvailabilityAct as isAvailabilityAct, tabdocinventoryos.isAvailabilityAcc as isAvailabilityAcc, catalogos.Description as catalogOSDescription   FROM catalogos, tabdocinventoryos WHERE tabdocinventoryos.id = :id AND catalogos.refKey = tabdocinventoryos.catalogOSKey LIMIT 1")
    fun get(id: Long): LiveData<CatalogOSAllTabDocInventoryOS>

    @Query("SELECT tabdocinventoryos.id as tab_doc_inventory_os_id, tabdocinventoryos.refKeyDoc as doc_refkey, tabdocinventoryos.lineNumber as lineNumber, tabdocinventoryos.catalogOSKey as catalogOSKey, tabdocinventoryos.isAvailabilityAct as isAvailabilityAct, tabdocinventoryos.isAvailabilityAcc as isAvailabilityAcc, catalogos.Description as catalogOSDescription   FROM catalogos, tabdocinventoryos WHERE tabdocinventoryos.refKeyDoc = :refKeyDoc AND catalogos.refKey = tabdocinventoryos.catalogOSKey")
    fun getAll(refKeyDoc: String): List<CatalogOSAllTabDocInventoryOS>?
}