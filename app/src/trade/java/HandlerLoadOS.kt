@file:Suppress("UNUSED_PARAMETER")

import android.arch.lifecycle.MutableLiveData
import ru.smartms.smallsmarty.data.source.CatalogOperationObjectRepository
import ru.smartms.smallsmarty.data.source.ErrorMessageRepository
import ru.smartms.smallsmarty.db.entity.catalog.CatalogOS

fun loadOSFromREST(catalogOperationObjectRepository: CatalogOperationObjectRepository, isLoadingProgressBar: MutableLiveData<Boolean>, errorMessageRepository: ErrorMessageRepository) {

}

fun handleOS(barcode: String, catalogOperationObjectRepository: CatalogOperationObjectRepository): CatalogOS? = null

fun sendOSKey(refKey: String, refKeyOS: MutableLiveData<String>, isLoadingProgressBar: MutableLiveData<Boolean>) {

}